import 'package:logger/src/log_level.dart';
import 'package:core/core.dart';

class ApplicationSettings implements IApplicationSettings {
  @override
  List<Level> get logLevels => [
        Level.debug,
        Level.error,
        Level.fatal,
        Level.info,
        Level.trace,
        Level.warning
      ];

  @override
  String get logsDirectoryName => 'logs';

  @override
  List<String> get supportedLanguages => ['en', 'ru'];

  @override
  String get baseUrl =>
      'http://keycloak.default.k8s-dev.graham.bell-main.bellintegrator.ru/realms/master/protocol/openid-connect';
}
