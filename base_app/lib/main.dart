import 'dart:convert';

import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:keycloak_auth/keycloak_auth.dart';
import 'package:localization/localization.dart';
import 'package:theme/theme.dart';
import 'package:theme/theme_loader.dart';
import 'package:todo/todo.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'application_settings.dart';

late final AppThemesHolderData themesHolderDataModel;
const String authUrl =
    'http://keycloak.default.k8s-dev.graham.bell-main.bellintegrator.ru/realms/master/protocol/openid-connect/auth?client_id=test-resource-server&redirect_uri=http://keycloak.default.k8s-dev.graham.bell-main.bellintegrator.ru/test-resource-server/redirect/test-resource-server/auth';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt.instance.registerSingleton<IApplicationSettings>(ApplicationSettings());
  final themeContent = await rootBundle.loadString('assets/theme/theme.json');
  final themesHolderDataJson = jsonDecode(themeContent);
  themesHolderDataModel = AppThemesHolderDataDto.fromJson(themesHolderDataJson)
      .toAppThemesHolderData();
  final mainApp = MainApp();
  await mainApp.init();
  runApp(AppThemesHolder(
    data: themesHolderDataModel,
    child: MainAppWidget(
      router: mainApp.router,
    ),
  ));
}

class MainApp with BaseApp {
  @override
  List<MicroApp> get microApps =>
      [KeycloakAuth(authUri: Uri.parse(authUrl)), Todo()];

  // @override
  // List<MicroApp> get microApps => [microApp.Login(), Todo()];
}

class MainAppWidget extends StatelessWidget {
  final GoRouter router;
  const MainAppWidget({super.key, required this.router});

  @override
  Widget build(BuildContext context) {
    return AnimatedAppTheme.wrapViewWithEffectiveTheme(
      child: MaterialApp.router(
        routerConfig: router,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: GetIt.instance
            .get<IApplicationSettings>()
            .supportedLanguages
            .map((e) => Locale(e, '')),
      ),
      viewKey: key,
      context: context,
    );
  }
}
