import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:core/src/application_settings.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:package_info_plus/package_info_plus.dart';

Logger? _logger;

Logger get logger {
  _logger ??= Logger(
    filter: ProjectLogFilter._internal(),
    printer: PrettyPrinter(
        printTime: false,
        noBoxingByDefault: true,
        methodCount: 0,
        errorMethodCount: 0),
  );
  return _logger!;
}

Future<void> initLogToFile(String path) async {
  if (Platform.isAndroid || Platform.isIOS) return;
  final currentIsolate = Isolate.current;
  final dir = Directory('$path${Platform.pathSeparator}logs');
  if (!dir.existsSync()) {
    dir.createSync(recursive: true);
  }
  PackageInfo packageInfo = await PackageInfo.fromPlatform();

  final logFile = File(
      '${dir.path}${Platform.pathSeparator}${packageInfo.appName}_${currentIsolate.debugName}_${DateTime.now().toFileNameString}.log');
  if (!logFile.existsSync()) {
    logFile.createSync();
  }

  _logger = Logger(
      filter: ProjectLogFilter._internal(),
      printer: PrettyPrinter(
          printTime: true,
          colors: false,
          methodCount: 0,
          noBoxingByDefault: true,
          printEmojis: true),
      output: CustomOutput(file: logFile));

  _logger?.i('New log file created in $logFile');
}

class CustomOutput extends LogOutput {
  final File file;
  final bool overrideExisting;
  final Encoding encoding;
  IOSink? _sink;

  CustomOutput({
    required this.file,
    this.overrideExisting = false,
    this.encoding = utf8,
  });

  @override
  Future<void> destroy() async {
    await _sink?.flush();
    await _sink?.close();
  }

  @override
  Future<void> init() async {
    _sink = file.openWrite(
      mode: overrideExisting ? FileMode.writeOnly : FileMode.writeOnlyAppend,
      encoding: encoding,
    );
  }

  @override
  void output(OutputEvent event) {
    // ignore: avoid_print
    event.lines.forEach(print);
    _sink?.writeAll(event.lines, '\n');
    _sink?.writeln();
  }
}

class ProjectLogFilter extends LogFilter {
  ProjectLogFilter._internal();

  @override
  bool shouldLog(LogEvent event) {
    IApplicationSettings? applicationSettings;
    if (GetIt.instance.isRegistered<IApplicationSettings>()) {
      applicationSettings = GetIt.instance.get<IApplicationSettings>();
    }

    if (kDebugMode) {
      if (applicationSettings != null) {
        return applicationSettings.logLevels.contains(event.level);
      } else {
        return ((event.level == Level.error) ||
            event.level == Level.warning ||
            event.level == Level.info ||
            event.level == Level.trace ||
            event.level == Level.fatal ||
            event.level == Level.debug);
      }
    } else {
      return ((event.level == Level.error) || event.level == Level.warning);
    }
  }
}

extension DateTimeHelper on DateTime {
  String get toFileNameString {
    var date = toLocal();
    var formatter = DateFormat('dd_MMM_yyyy_HH_mm_ss');
    return formatter.format(date);
  }
}
