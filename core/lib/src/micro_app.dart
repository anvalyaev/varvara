import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';

abstract mixin class MicroApp {
  String get microAppName;

  List<RouteBase> get routes;

  Widget? microAppWidget();

  Future<void> init();
}
