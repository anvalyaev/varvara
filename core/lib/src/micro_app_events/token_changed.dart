import 'package:core/src/event_bus.dart';

class TokenChanged extends MicroAppEvent {
  final String baseUrl;
  final String? token;
  bool get authorized => token != null;
  TokenChanged({
    required this.baseUrl,
    required this.token,
  });
}
