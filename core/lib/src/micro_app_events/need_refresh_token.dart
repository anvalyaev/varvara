import 'package:core/src/event_bus.dart';

class NeedRefreshToken extends MicroAppEvent {
  NeedRefreshToken();
}
