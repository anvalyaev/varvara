import 'package:core/core.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';

abstract mixin class BaseApp {
  List<MicroApp> get microApps;

  final List<RouteBase> _routes = [];

  late final GoRouter router;

  Future<void> init() async {
    router = GoRouter(routes: _routes);
    GetIt.instance.registerSingleton<GoRouter>(router, signalsReady: true);
    if (microApps.isNotEmpty && _routes.isEmpty) {
      for (MicroApp microapp in microApps) {
        _routes.addAll(microapp.routes);
        await microapp.init();
        if (microapp.microAppWidget() != null) {
          WidgetsRegistry[microapp.microAppName] = microapp.microAppWidget();
        }
      }
    }
  }
}
