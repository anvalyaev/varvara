import 'package:logger/logger.dart';

abstract class IApplicationSettings {
  String get logsDirectoryName;
  String get baseUrl;
  List<Level> get logLevels;
  List<String> get supportedLanguages;
}
