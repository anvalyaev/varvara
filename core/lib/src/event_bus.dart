// ignore_for_file: constant_identifier_names, prefer_final_fields

import 'dart:async';

typedef FeaturesEventHandler<T extends MicroAppEvent> = void Function(T event);

abstract class MicroAppEvent {}

class MicroAppsEventBus {
  MicroAppsEventBus._internal();
  static final MicroAppsEventBus _instance = MicroAppsEventBus._internal();
  factory MicroAppsEventBus() => _instance;

  final _MicroAppsEventBus _bus = _MicroAppsEventBus();

  void emit(MicroAppEvent event) {
    _instance._bus.emit(event);
  }

  StreamSubscription on<T extends MicroAppEvent>(
      FeaturesEventHandler<T> handler) {
    return _instance._bus.on<T>(handler);
  }
}

class _MicroAppsEventBus {
  StreamController _streamController;

  StreamController get streamController => _streamController;

  _MicroAppsEventBus({bool sync = false})
      : _streamController = StreamController.broadcast(sync: sync);

  StreamSubscription on<T extends MicroAppEvent>(
      FeaturesEventHandler<T> handler) {
    return streamController.stream
        .where((event) => event is T)
        .cast<T>()
        .listen((event) {
      handler(event);
    });
  }

  void emit(event) {
    streamController.add(event);
  }

  void dispose() {
    _streamController.close();
  }
}
