library data;

import 'dart:io';

import 'package:isar/isar.dart';
import 'package:isolated_clean_architecture/isolated_clean_architecture.dart';

abstract class IsarRepositoryBase extends RepositoryBase {
  final Isar _isar;

  IsarRepositoryBase(
      {required String name,
      required Directory dir,
      required List<IsarGeneratedSchema> schemas})
      : _isar = Isar.open(schemas: schemas, directory: dir.path, name: name);

  Isar get isar => _isar;

  @override
  Future<void> clear() async {
    isar.clear();
  }
}
