import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'app_color_scheme_override.dart';

Color? _getColorFromCode(String? colorCode) {
  if (colorCode == null) {
    return null;
  }
  return Color(int.parse(colorCode, radix: 16));
}

class AppColorSchemeOverrideDto extends Equatable {
  final CFPrimaryColorSchemeNodeOverrideDto? cfPrimary;
  final CFAccentColorSchemeNodeOverrideDto? cfAccent;
  final CFSuccessColorSchemeNodeOverrideDto? cfSuccess;
  final CFWarningColorSchemeNodeOverrideDto? cfWarning;
  final BPrimaryColorSchemeNodeOverrideDto? bPrimary;
  final BSecondary100ColorSchemeNodeOverrideDto? bSecondary100;
  final BSecondary200ColorSchemeNodeOverrideDto? bSecondary200;
  final BSecondary300ColorSchemeNodeOverrideDto? bSecondary300;
  final BTransparent100ColorSchemeNodeOverrideDto? bTranparent100;
  final OtherColorSchemeNodeOverrideDto other;
  final OtherTransparentColorSchemeNodeOverrideDto otherTransparent;

  const AppColorSchemeOverrideDto({
    required this.cfSuccess,
    required this.cfWarning,
    required this.cfPrimary,
    required this.cfAccent,
    required this.bPrimary,
    required this.bSecondary100,
    required this.bSecondary200,
    required this.bSecondary300,
    required this.bTranparent100,
    required this.other,
    required this.otherTransparent,
  });

  factory AppColorSchemeOverrideDto.fromJson(Map<String, dynamic> json) {
    return AppColorSchemeOverrideDto(
      cfSuccess:
          CFSuccessColorSchemeNodeOverrideDto.fromJson(json['cfSuccess']),
      cfWarning:
          CFWarningColorSchemeNodeOverrideDto.fromJson(json['cfWarning']),
      cfPrimary:
          CFPrimaryColorSchemeNodeOverrideDto.fromJson(json['cfPrimary']),
      cfAccent: CFAccentColorSchemeNodeOverrideDto.fromJson(json['cfAccent']),
      bPrimary: BPrimaryColorSchemeNodeOverrideDto.fromJson(json['bPrimary']),
      bSecondary100: BSecondary100ColorSchemeNodeOverrideDto.fromJson(
          json['bSecondary100']),
      bSecondary200: BSecondary200ColorSchemeNodeOverrideDto.fromJson(
          json['bSecondary200']),
      bSecondary300: BSecondary300ColorSchemeNodeOverrideDto.fromJson(
          json['bSecondary300']),
      bTranparent100: BTransparent100ColorSchemeNodeOverrideDto.fromJson(
          json['bTransparent100']),
      other: OtherColorSchemeNodeOverrideDto.fromJson(json['other']),
      otherTransparent: OtherTransparentColorSchemeNodeOverrideDto.fromJson(
          json['otherTransparent']),
    );
  }

  @override
  List<Object?> get props {
    return [
      cfSuccess,
      cfWarning,
      cfPrimary,
      cfAccent,
      bPrimary,
      bSecondary100,
      bSecondary200,
      bSecondary300,
      other,
      otherTransparent,
    ];
  }

  AppColorSchemeOverride toAppColorSchemeOverride() {
    return AppColorSchemeOverride(
      cfSuccess: cfSuccess?.toEntity(),
      cfWarning: cfWarning?.toEntity(),
      cfPrimary: cfPrimary?.toEntity(),
      cfAccent: cfAccent?.toEntity(),
      bPrimary: bPrimary?.toEntity(),
      bSecondary100: bSecondary100?.toEntity(),
      bSecondary200: bSecondary200?.toEntity(),
      bSecondary300: bSecondary300?.toEntity(),
      bTranparent100: bTranparent100?.toEntity(),
      other: other.toEntity(),
      otherTransparent: otherTransparent.toEntity(),
    );
  }
}

class BPrimaryColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final NPrimaryColorSchemeNodeOverrideDto? nPrimary;
  final NSecondary100ColorSchemeNodeOverrideDto? nSecondary100;
  final NSecondary200ColorSchemeNodeOverrideDto? nSecondary200;
  final NSecondary300ColorSchemeNodeOverrideDto? nSecondary300;
  final NSecondary400ColorSchemeNodeOverrideDto? nSecondary400;
  final NSecondary500ColorSchemeNodeOverrideDto? nSecondary500;

  const BPrimaryColorSchemeNodeOverrideDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BPrimaryColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return BPrimaryColorSchemeNodeOverrideDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeOverrideDto.fromJson(json['nPrimary']),
      nSecondary100: NSecondary100ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary100']),
      nSecondary200: NSecondary200ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary200']),
      nSecondary300: NSecondary300ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary300']),
      nSecondary400: NSecondary400ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary400']),
      nSecondary500: NSecondary500ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BPrimaryColorSchemeNodeOverride toEntity() {
    return BPrimaryColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      nPrimary: nPrimary?.toEntity(),
      nSecondary100: nSecondary100?.toEntity(),
      nSecondary200: nSecondary200?.toEntity(),
      nSecondary300: nSecondary300?.toEntity(),
      nSecondary400: nSecondary400?.toEntity(),
      nSecondary500: nSecondary500?.toEntity(),
    );
  }
}

class BSecondary100ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final NPrimaryColorSchemeNodeOverrideDto? nPrimary;
  final NSecondary100ColorSchemeNodeOverrideDto? nSecondary100;
  final NSecondary200ColorSchemeNodeOverrideDto? nSecondary200;
  final NSecondary300ColorSchemeNodeOverrideDto? nSecondary300;
  final NSecondary400ColorSchemeNodeOverrideDto? nSecondary400;
  final NSecondary500ColorSchemeNodeOverrideDto? nSecondary500;

  const BSecondary100ColorSchemeNodeOverrideDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BSecondary100ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return BSecondary100ColorSchemeNodeOverrideDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeOverrideDto.fromJson(json['nPrimary']),
      nSecondary100: NSecondary100ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary100']),
      nSecondary200: NSecondary200ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary200']),
      nSecondary300: NSecondary300ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary300']),
      nSecondary400: NSecondary400ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary400']),
      nSecondary500: NSecondary500ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary100ColorSchemeNodeOverride toEntity() {
    return BSecondary100ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      nPrimary: nPrimary?.toEntity(),
      nSecondary100: nSecondary100?.toEntity(),
      nSecondary200: nSecondary200?.toEntity(),
      nSecondary300: nSecondary300?.toEntity(),
      nSecondary400: nSecondary400?.toEntity(),
      nSecondary500: nSecondary500?.toEntity(),
    );
  }
}

class BSecondary200ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final NPrimaryColorSchemeNodeOverrideDto? nPrimary;
  final NSecondary100ColorSchemeNodeOverrideDto? nSecondary100;
  final NSecondary200ColorSchemeNodeOverrideDto? nSecondary200;
  final NSecondary300ColorSchemeNodeOverrideDto? nSecondary300;
  final NSecondary400ColorSchemeNodeOverrideDto? nSecondary400;
  final NSecondary500ColorSchemeNodeOverrideDto? nSecondary500;

  const BSecondary200ColorSchemeNodeOverrideDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BSecondary200ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return BSecondary200ColorSchemeNodeOverrideDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeOverrideDto.fromJson(json['nPrimary']),
      nSecondary100: NSecondary100ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary100']),
      nSecondary200: NSecondary200ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary200']),
      nSecondary300: NSecondary300ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary300']),
      nSecondary400: NSecondary400ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary400']),
      nSecondary500: NSecondary500ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary200ColorSchemeNodeOverride toEntity() {
    return BSecondary200ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      nPrimary: nPrimary?.toEntity(),
      nSecondary100: nSecondary100?.toEntity(),
      nSecondary200: nSecondary200?.toEntity(),
      nSecondary300: nSecondary300?.toEntity(),
      nSecondary400: nSecondary400?.toEntity(),
      nSecondary500: nSecondary500?.toEntity(),
    );
  }
}

class BSecondary300ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final NPrimaryColorSchemeNodeOverrideDto? nPrimary;
  final NSecondary100ColorSchemeNodeOverrideDto? nSecondary100;
  final NSecondary200ColorSchemeNodeOverrideDto? nSecondary200;
  final NSecondary300ColorSchemeNodeOverrideDto? nSecondary300;
  final NSecondary400ColorSchemeNodeOverrideDto? nSecondary400;
  final NSecondary500ColorSchemeNodeOverrideDto? nSecondary500;

  const BSecondary300ColorSchemeNodeOverrideDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BSecondary300ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return BSecondary300ColorSchemeNodeOverrideDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeOverrideDto.fromJson(json['nPrimary']),
      nSecondary100: NSecondary100ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary100']),
      nSecondary200: NSecondary200ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary200']),
      nSecondary300: NSecondary300ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary300']),
      nSecondary400: NSecondary400ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary400']),
      nSecondary500: NSecondary500ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary300ColorSchemeNodeOverride toEntity() {
    return BSecondary300ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      nPrimary: nPrimary?.toEntity(),
      nSecondary100: nSecondary100?.toEntity(),
      nSecondary200: nSecondary200?.toEntity(),
      nSecondary300: nSecondary300?.toEntity(),
      nSecondary400: nSecondary400?.toEntity(),
      nSecondary500: nSecondary500?.toEntity(),
    );
  }
}

class BTransparent100ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final NPrimaryColorSchemeNodeOverrideDto? nPrimary;
  final NSecondary100ColorSchemeNodeOverrideDto? nSecondary100;
  final NSecondary200ColorSchemeNodeOverrideDto? nSecondary200;
  final NSecondary300ColorSchemeNodeOverrideDto? nSecondary300;
  final NSecondary400ColorSchemeNodeOverrideDto? nSecondary400;
  final NSecondary500ColorSchemeNodeOverrideDto? nSecondary500;

  const BTransparent100ColorSchemeNodeOverrideDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BTransparent100ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return BTransparent100ColorSchemeNodeOverrideDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeOverrideDto.fromJson(json['nPrimary']),
      nSecondary100: NSecondary100ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary100']),
      nSecondary200: NSecondary200ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary200']),
      nSecondary300: NSecondary300ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary300']),
      nSecondary400: NSecondary400ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary400']),
      nSecondary500: NSecondary500ColorSchemeNodeOverrideDto.fromJson(
          json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BTransparent100ColorSchemeNodeOverride toEntity() {
    return BTransparent100ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      nPrimary: nPrimary?.toEntity(),
      nSecondary100: nSecondary100?.toEntity(),
      nSecondary200: nSecondary200?.toEntity(),
      nSecondary300: nSecondary300?.toEntity(),
      nSecondary400: nSecondary400?.toEntity(),
      nSecondary500: nSecondary500?.toEntity(),
    );
  }
}

class CFAccentColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final CFOnAccentColorSchemeNodeOverrideDto? cfOnAccent;

  const CFAccentColorSchemeNodeOverrideDto({
    required this.color,
    required this.cfOnAccent,
  });

  factory CFAccentColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFAccentColorSchemeNodeOverrideDto(
      color: json['color'],
      cfOnAccent:
          CFOnAccentColorSchemeNodeOverrideDto.fromJson(json['cfOnAccent']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      cfOnAccent,
    ];
  }

  CFAccentColorSchemeNodeOverride toEntity() {
    return CFAccentColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      cfOnAccent: cfOnAccent?.toEntity(),
    );
  }
}

class CFOnAccentColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const CFOnAccentColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory CFOnAccentColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFOnAccentColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnAccentColorSchemeNodeOverride toEntity() {
    return CFOnAccentColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class CFOnPrimaryColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const CFOnPrimaryColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory CFOnPrimaryColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFOnPrimaryColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnPrimaryColorSchemeNodeOverride toEntity() {
    return CFOnPrimaryColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class CFOnSuccessColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const CFOnSuccessColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory CFOnSuccessColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFOnSuccessColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnSuccessColorSchemeNodeOverride toEntity() {
    return CFOnSuccessColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class CFOnWarningColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const CFOnWarningColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory CFOnWarningColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFOnWarningColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnWarningColorSchemeNodeOverride toEntity() {
    return CFOnWarningColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class CFPrimaryColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final CFOnPrimaryColorSchemeNodeOverrideDto? cfOnPrimary;

  const CFPrimaryColorSchemeNodeOverrideDto({
    required this.color,
    required this.cfOnPrimary,
  });

  factory CFPrimaryColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFPrimaryColorSchemeNodeOverrideDto(
      color: json['color'],
      cfOnPrimary:
          CFOnPrimaryColorSchemeNodeOverrideDto.fromJson(json['cfOnPrimary']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      cfOnPrimary,
    ];
  }

  CFPrimaryColorSchemeNodeOverride toEntity() {
    return CFPrimaryColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      cfOnPrimary: cfOnPrimary?.toEntity(),
    );
  }
}

class CFSuccessColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final CFOnSuccessColorSchemeNodeOverrideDto cfOnSuccess;

  const CFSuccessColorSchemeNodeOverrideDto(
      {required this.color, required this.cfOnSuccess});

  factory CFSuccessColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFSuccessColorSchemeNodeOverrideDto(
        color: json['color'],
        cfOnSuccess: CFOnSuccessColorSchemeNodeOverrideDto.fromJson(
            json['cfOnSuccess']));
  }

  @override
  List<Object?> get props {
    return [color, cfOnSuccess];
  }

  CFSuccessColorSchemeNodeOverride toEntity() {
    return CFSuccessColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      cfOnSuccess: cfOnSuccess.toEntity(),
    );
  }
}

class CFWarningColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final CFOnWarningColorSchemeNodeOverrideDto? cfOnWarning;

  const CFWarningColorSchemeNodeOverrideDto(
      {required this.color, required this.cfOnWarning});

  factory CFWarningColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return CFWarningColorSchemeNodeOverrideDto(
        color: json['color'],
        cfOnWarning: CFOnWarningColorSchemeNodeOverrideDto.fromJson(
            json['cfOnWarning']));
  }

  @override
  List<Object?> get props {
    return [color, cfOnWarning];
  }

  CFWarningColorSchemeNodeOverride toEntity() {
    return CFWarningColorSchemeNodeOverride(
      color: _getColorFromCode(color),
      cfOnWarning: cfOnWarning?.toEntity(),
    );
  }
}

class NPrimaryColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const NPrimaryColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory NPrimaryColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return NPrimaryColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NPrimaryColorSchemeNodeOverride toEntity() {
    return NPrimaryColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary100ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const NSecondary100ColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory NSecondary100ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return NSecondary100ColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary100ColorSchemeNodeOverride toEntity() {
    return NSecondary100ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary200ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const NSecondary200ColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory NSecondary200ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return NSecondary200ColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary200ColorSchemeNodeOverride toEntity() {
    return NSecondary200ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary300ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const NSecondary300ColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory NSecondary300ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return NSecondary300ColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary300ColorSchemeNodeOverride toEntity() {
    return NSecondary300ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary400ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const NSecondary400ColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory NSecondary400ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return NSecondary400ColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary400ColorSchemeNodeOverride toEntity() {
    return NSecondary400ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary500ColorSchemeNodeOverrideDto extends Equatable {
  final String? color;

  const NSecondary500ColorSchemeNodeOverrideDto({
    required this.color,
  });

  factory NSecondary500ColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return NSecondary500ColorSchemeNodeOverrideDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary500ColorSchemeNodeOverride toEntity() {
    return NSecondary500ColorSchemeNodeOverride(
      color: _getColorFromCode(color),
    );
  }
}

class OtherColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final Map<String, dynamic>? colors;

  const OtherColorSchemeNodeOverrideDto({
    required this.color,
    required this.colors,
  });

  factory OtherColorSchemeNodeOverrideDto.fromJson(Map<String, dynamic> json) {
    return OtherColorSchemeNodeOverrideDto(
      color: json['color'],
      colors: json['colors'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }

  OtherColorSchemeNodeOverride toEntity() {
    return OtherColorSchemeNodeOverride(
        color: _getColorFromCode(color),
        colors: colors?.map((key, value) =>
                MapEntry(int.parse(key), _getColorFromCode(value))) ??
            {});
  }
}

class OtherTransparentColorSchemeNodeOverrideDto extends Equatable {
  final String? color;
  final Map<String, dynamic>? colors;

  const OtherTransparentColorSchemeNodeOverrideDto({
    required this.color,
    required this.colors,
  });

  factory OtherTransparentColorSchemeNodeOverrideDto.fromJson(
      Map<String, dynamic> json) {
    return OtherTransparentColorSchemeNodeOverrideDto(
      color: json['color'],
      colors: json['colors'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }

  OtherTransparentColorSchemeNodeOverride toEntity() {
    return OtherTransparentColorSchemeNodeOverride(
        color: _getColorFromCode(color),
        colors: colors?.map((key, value) =>
                MapEntry(int.parse(key), _getColorFromCode(value))) ??
            {});
  }
}
