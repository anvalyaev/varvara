import 'package:equatable/equatable.dart';

import 'app_text_style_dto.dart';
import 'app_text_style_override_dto.dart';
import 'app_text_styles_holder_data.dart';

class AppTextStylesHolderDataDto extends Equatable {
  final AppTextStyleDto appTextStyleDto;

  final Map<String, AppTextStyleOverrideDto> appTextStyleOverrideDtos;

  const AppTextStylesHolderDataDto({
    required this.appTextStyleDto,
    this.appTextStyleOverrideDtos = const {},
  });

  factory AppTextStylesHolderDataDto.fromJson(Map<String, dynamic> json) {
    final baseTextStyleSchemeDto = AppTextStyleDto.fromJson(json);

    return AppTextStylesHolderDataDto(
        appTextStyleDto: baseTextStyleSchemeDto,
        appTextStyleOverrideDtos: const {});
  }

  @override
  List<Object?> get props {
    return [appTextStyleDto, appTextStyleOverrideDtos];
  }

  AppTextStylesHolderData toAppTextStylesHolderData() {
    final textStyleOverrideDtos = appTextStyleOverrideDtos.map((key, value) {
      return MapEntry(key, value.toAppTextStyleOverride());
    });

    return AppTextStylesHolderData(
        appTextStyle: appTextStyleDto.toAppTextStyle(),
        appTextStyleOverrides: textStyleOverrideDtos);
  }
}
