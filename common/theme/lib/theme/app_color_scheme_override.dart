import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class AppColorSchemeNodeOverride {
  Color? get color;
}

class AppColorSchemeOverride extends Equatable {
  final CFPrimaryColorSchemeNodeOverride? cfPrimary;
  final CFAccentColorSchemeNodeOverride? cfAccent;
  final CFSuccessColorSchemeNodeOverride? cfSuccess;
  final CFWarningColorSchemeNodeOverride? cfWarning;
  final BPrimaryColorSchemeNodeOverride? bPrimary;
  final BSecondary100ColorSchemeNodeOverride? bSecondary100;
  final BSecondary200ColorSchemeNodeOverride? bSecondary200;
  final BSecondary300ColorSchemeNodeOverride? bSecondary300;
  final BTransparent100ColorSchemeNodeOverride? bTranparent100;
  final OtherColorSchemeNodeOverride other;
  final OtherTransparentColorSchemeNodeOverride otherTransparent;

  const AppColorSchemeOverride({
    required this.cfSuccess,
    required this.cfWarning,
    required this.cfPrimary,
    required this.cfAccent,
    required this.bPrimary,
    required this.bSecondary100,
    required this.bSecondary200,
    required this.bSecondary300,
    required this.bTranparent100,
    required this.other,
    required this.otherTransparent,
  });

  @override
  List<Object?> get props {
    return [
      cfSuccess,
      cfWarning,
      cfPrimary,
      cfAccent,
      bPrimary,
      bSecondary100,
      bSecondary200,
      bSecondary300,
      other,
      otherTransparent,
    ];
  }
}

class BackgroundSimilarContrastingColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const BackgroundSimilarContrastingColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class BPrimaryColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final NPrimaryColorSchemeNodeOverride? nPrimary;
  final NSecondary100ColorSchemeNodeOverride? nSecondary100;
  final NSecondary200ColorSchemeNodeOverride? nSecondary200;
  final NSecondary300ColorSchemeNodeOverride? nSecondary300;
  final NSecondary400ColorSchemeNodeOverride? nSecondary400;
  final NSecondary500ColorSchemeNodeOverride? nSecondary500;

  const BPrimaryColorSchemeNodeOverride({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }
}

class BSecondary100ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final NPrimaryColorSchemeNodeOverride? nPrimary;
  final NSecondary100ColorSchemeNodeOverride? nSecondary100;
  final NSecondary200ColorSchemeNodeOverride? nSecondary200;
  final NSecondary300ColorSchemeNodeOverride? nSecondary300;
  final NSecondary400ColorSchemeNodeOverride? nSecondary400;
  final NSecondary500ColorSchemeNodeOverride? nSecondary500;

  const BSecondary100ColorSchemeNodeOverride({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }
}

class BSecondary200ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final NPrimaryColorSchemeNodeOverride? nPrimary;
  final NSecondary100ColorSchemeNodeOverride? nSecondary100;
  final NSecondary200ColorSchemeNodeOverride? nSecondary200;
  final NSecondary300ColorSchemeNodeOverride? nSecondary300;
  final NSecondary400ColorSchemeNodeOverride? nSecondary400;
  final NSecondary500ColorSchemeNodeOverride? nSecondary500;

  const BSecondary200ColorSchemeNodeOverride({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }
}

class BSecondary300ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final NPrimaryColorSchemeNodeOverride? nPrimary;
  final NSecondary100ColorSchemeNodeOverride? nSecondary100;
  final NSecondary200ColorSchemeNodeOverride? nSecondary200;
  final NSecondary300ColorSchemeNodeOverride? nSecondary300;
  final NSecondary400ColorSchemeNodeOverride? nSecondary400;
  final NSecondary500ColorSchemeNodeOverride? nSecondary500;

  const BSecondary300ColorSchemeNodeOverride({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }
}

class BTransparent100ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final NPrimaryColorSchemeNodeOverride? nPrimary;
  final NSecondary100ColorSchemeNodeOverride? nSecondary100;
  final NSecondary200ColorSchemeNodeOverride? nSecondary200;
  final NSecondary300ColorSchemeNodeOverride? nSecondary300;
  final NSecondary400ColorSchemeNodeOverride? nSecondary400;
  final NSecondary500ColorSchemeNodeOverride? nSecondary500;

  const BTransparent100ColorSchemeNodeOverride({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }
}

class CFAccentColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final CFOnAccentColorSchemeNodeOverride? cfOnAccent;

  const CFAccentColorSchemeNodeOverride({
    required this.color,
    required this.cfOnAccent,
  });

  @override
  List<Object?> get props {
    return [
      color,
      cfOnAccent,
    ];
  }
}

class CFOnAccentColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const CFOnAccentColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class CFOnPrimaryColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const CFOnPrimaryColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class CFOnSuccessColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const CFOnSuccessColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

/*class SecondaryXContrastingColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const SecondaryXContrastingColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}*/

class CFOnWarningColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const CFOnWarningColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class CFPrimaryColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;
  final CFOnPrimaryColorSchemeNodeOverride? cfOnPrimary;

  const CFPrimaryColorSchemeNodeOverride({
    required this.color,
    required this.cfOnPrimary,
  });

  @override
  List<Object?> get props {
    return [
      color,
      cfOnPrimary,
    ];
  }
}

class CFSuccessColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  final CFOnSuccessColorSchemeNodeOverride cfOnSuccess;

  const CFSuccessColorSchemeNodeOverride(
      {required this.color, required this.cfOnSuccess});

  @override
  List<Object?> get props {
    return [
      color,
      cfOnSuccess,
    ];
  }
}

class CFWarningColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;
  final CFOnWarningColorSchemeNodeOverride? cfOnWarning;

  const CFWarningColorSchemeNodeOverride({
    required this.color,
    required this.cfOnWarning,
  });

  @override
  List<Object?> get props {
    return [
      color,
      cfOnWarning,
    ];
  }
}

class NPrimaryColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const NPrimaryColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class NSecondary100ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const NSecondary100ColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class NSecondary200ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const NSecondary200ColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class NSecondary300ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const NSecondary300ColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class NSecondary400ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const NSecondary400ColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class NSecondary500ColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;

  const NSecondary500ColorSchemeNodeOverride({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }
}

class OtherColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;
  final Map<int, Color?>? colors;

  const OtherColorSchemeNodeOverride({
    required this.color,
    required this.colors,
  });

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }
}

class OtherTransparentColorSchemeNodeOverride extends Equatable
    implements AppColorSchemeNodeOverride {
  @override
  final Color? color;
  final Map<int, Color?>? colors;

  const OtherTransparentColorSchemeNodeOverride({
    required this.color,
    required this.colors,
  });

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }
}
