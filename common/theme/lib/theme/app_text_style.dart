import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class AppTextStyle extends Equatable {
  final TextStyle heading1;
  final TextStyle heading2;
  final TextStyle heading3;
  final TextStyle heading4;
  final TextStyle largeBold;
  final TextStyle largeSemibold;
  final TextStyle largeRegular;
  final TextStyle baseBold;
  final TextStyle baseSemibold;
  final TextStyle baseRegular;
  final TextStyle smallBold;
  final TextStyle smallSemibold;
  final TextStyle smallRegular;
  final TextStyle smallCaption;
  final TextStyle tapBar;
  final TextStyle button;
  final TextStyle date;
  final TextStyle mono;

  const AppTextStyle(
      {required this.heading1,
      required this.heading2,
      required this.heading3,
      required this.heading4,
      required this.largeBold,
      required this.largeSemibold,
      required this.largeRegular,
      required this.baseBold,
      required this.baseSemibold,
      required this.baseRegular,
      required this.smallBold,
      required this.smallSemibold,
      required this.smallRegular,
      required this.smallCaption,
      required this.tapBar,
      required this.button,
      required this.date,
      required this.mono});

  factory AppTextStyle.empty() => const AppTextStyle(
        heading1: TextStyle(),
        heading2: TextStyle(),
        heading3: TextStyle(),
        heading4: TextStyle(),
        largeBold: TextStyle(),
        largeSemibold: TextStyle(),
        largeRegular: TextStyle(),
        baseBold: TextStyle(),
        baseSemibold: TextStyle(),
        baseRegular: TextStyle(),
        smallBold: TextStyle(),
        smallSemibold: TextStyle(),
        smallRegular: TextStyle(),
        smallCaption: TextStyle(),
        tapBar: TextStyle(),
        button: TextStyle(),
        date: TextStyle(),
        mono: TextStyle(),
      );

  @override
  List<Object?> get props => [
        heading1,
        heading2,
        heading3,
        heading4,
        largeBold,
        largeSemibold,
        largeRegular,
        baseBold,
        baseSemibold,
        baseRegular,
        smallBold,
        smallSemibold,
        smallRegular,
        smallCaption,
        tapBar,
        button,
        date,
      ];

  AppTextStyle copyWith({
    TextStyle? heading1,
    TextStyle? heading2,
    TextStyle? heading3,
    TextStyle? heading4,
    TextStyle? largeBold,
    TextStyle? largeSemibold,
    TextStyle? largeRegular,
    TextStyle? baseBold,
    TextStyle? baseSemibold,
    TextStyle? baseRegular,
    TextStyle? smallBold,
    TextStyle? smallSemibold,
    TextStyle? smallRegular,
    TextStyle? smallCaption,
    TextStyle? tapBar,
    TextStyle? button,
    TextStyle? date,
    TextStyle? mono,
  }) =>
      AppTextStyle(
          heading1: heading1 ?? this.heading1,
          heading2: heading2 ?? this.heading2,
          heading3: heading3 ?? this.heading3,
          heading4: heading4 ?? this.heading4,
          largeBold: largeBold ?? this.largeBold,
          largeSemibold: largeSemibold ?? this.largeSemibold,
          largeRegular: largeRegular ?? this.largeRegular,
          baseBold: baseBold ?? this.baseBold,
          baseSemibold: baseSemibold ?? this.baseSemibold,
          baseRegular: baseRegular ?? this.baseRegular,
          smallBold: smallBold ?? this.smallBold,
          smallSemibold: smallSemibold ?? this.smallSemibold,
          smallRegular: smallRegular ?? this.smallRegular,
          smallCaption: smallCaption ?? this.smallCaption,
          tapBar: tapBar ?? this.tapBar,
          button: button ?? this.button,
          date: date ?? this.date,
          mono: mono ?? this.mono);
}
