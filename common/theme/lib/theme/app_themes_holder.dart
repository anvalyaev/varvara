import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'app_color_scheme.dart';
import 'app_color_scheme_override.dart';
import 'app_color_schemes_holder_data.dart';
import 'app_text_style.dart';
import 'app_text_style_override.dart';
import 'app_text_styles_holder_data.dart';
import 'app_theme.dart';

class AppThemesHolder extends InheritedWidget {
  final AppThemesHolderData data;

  const AppThemesHolder({
    required this.data,
    required Widget child,
    Key? key,
  }) : super(
          key: key,
          child: child,
        );

  @override
  bool updateShouldNotify(AppThemesHolder oldWidget) {
    return data != oldWidget.data;
  }

  static AppThemesHolder? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppThemesHolder>();
  }
}

class AppThemesHolderData extends Equatable {
  final AppColorSchemesHolderData colorSchemesHolderData;
  final AppTextStylesHolderData textStylesHolderData;

  const AppThemesHolderData({
    required this.colorSchemesHolderData,
    required this.textStylesHolderData,
  });

  factory AppThemesHolderData.empty() {
    return AppThemesHolderData(
      colorSchemesHolderData: AppColorSchemesHolderData.empty(),
      textStylesHolderData: AppTextStylesHolderData.empty(),
    );
  }

  @override
  List<Object?> get props {
    return [
      colorSchemesHolderData,
      textStylesHolderData,
    ];
  }

  AppThemeData effectiveThemeForView({
    required String? viewId,
    required Brightness brightness,
  }) {
    late AppColorScheme baseColorScheme;
    AppColorSchemeOverride? overridedColorScheme;
    late AppTextStyle baseAppTextStyle;
    AppTextStyleOverride? textStyleOverride;

    if (brightness == Brightness.light) {
      baseColorScheme = colorSchemesHolderData.baseLightColorScheme;
      if (viewId != null) {
        overridedColorScheme =
            colorSchemesHolderData.lightColorSchemeOverrides[viewId];
      }
    } else if (brightness == Brightness.dark) {
      baseColorScheme = colorSchemesHolderData.baseDarkColorScheme;
      if (viewId != null) {
        overridedColorScheme =
            colorSchemesHolderData.darkColorSchemeOverrides[viewId];
      }
    }
    baseAppTextStyle = textStylesHolderData.appTextStyle;
    if (viewId != null) {
      textStyleOverride = textStylesHolderData.appTextStyleOverrides[viewId];
    }
    final heading1 = baseAppTextStyle.heading1.copyWith(
        fontFamily: textStyleOverride?.heading1.fontFamily,
        fontSize: textStyleOverride?.heading1.fontSize,
        fontWeight: textStyleOverride?.heading1.fontWeight,
        fontStyle: textStyleOverride?.heading1.fontStyle,
        letterSpacing: textStyleOverride?.heading1.letterSpacing);

    final heading2 = baseAppTextStyle.heading2.copyWith(
        fontFamily: textStyleOverride?.heading2.fontFamily,
        fontSize: textStyleOverride?.heading2.fontSize,
        fontWeight: textStyleOverride?.heading2.fontWeight,
        fontStyle: textStyleOverride?.heading2.fontStyle,
        letterSpacing: textStyleOverride?.heading2.letterSpacing);

    final heading3 = baseAppTextStyle.heading3.copyWith(
        fontFamily: textStyleOverride?.heading3.fontFamily,
        fontSize: textStyleOverride?.heading3.fontSize,
        fontWeight: textStyleOverride?.heading3.fontWeight,
        fontStyle: textStyleOverride?.heading3.fontStyle,
        letterSpacing: textStyleOverride?.heading3.letterSpacing);

    final heading4 = baseAppTextStyle.heading4.copyWith(
        fontFamily: textStyleOverride?.heading4.fontFamily,
        fontSize: textStyleOverride?.heading4.fontSize,
        fontWeight: textStyleOverride?.heading4.fontWeight,
        fontStyle: textStyleOverride?.heading4.fontStyle,
        letterSpacing: textStyleOverride?.heading4.letterSpacing);

    final largeBold = baseAppTextStyle.largeBold.copyWith(
        fontFamily: textStyleOverride?.largeBold.fontFamily,
        fontSize: textStyleOverride?.largeBold.fontSize,
        fontWeight: textStyleOverride?.largeBold.fontWeight,
        fontStyle: textStyleOverride?.largeBold.fontStyle,
        letterSpacing: textStyleOverride?.largeBold.letterSpacing);

    final largeSemibold = baseAppTextStyle.largeSemibold.copyWith(
        fontFamily: textStyleOverride?.largeSemibold.fontFamily,
        fontSize: textStyleOverride?.largeSemibold.fontSize,
        fontWeight: textStyleOverride?.largeSemibold.fontWeight,
        fontStyle: textStyleOverride?.largeSemibold.fontStyle,
        letterSpacing: textStyleOverride?.largeSemibold.letterSpacing);

    final largeRegular = baseAppTextStyle.largeRegular.copyWith(
        fontFamily: textStyleOverride?.largeRegular.fontFamily,
        fontSize: textStyleOverride?.largeRegular.fontSize,
        fontWeight: textStyleOverride?.largeRegular.fontWeight,
        fontStyle: textStyleOverride?.largeRegular.fontStyle,
        letterSpacing: textStyleOverride?.largeRegular.letterSpacing);

    final baseBold = baseAppTextStyle.baseBold.copyWith(
        fontFamily: textStyleOverride?.baseBold.fontFamily,
        fontSize: textStyleOverride?.baseBold.fontSize,
        fontWeight: textStyleOverride?.baseBold.fontWeight,
        fontStyle: textStyleOverride?.baseBold.fontStyle,
        letterSpacing: textStyleOverride?.baseBold.letterSpacing);

    final baseSemibold = baseAppTextStyle.baseSemibold.copyWith(
        fontFamily: textStyleOverride?.baseSemibold.fontFamily,
        fontSize: textStyleOverride?.baseSemibold.fontSize,
        fontWeight: textStyleOverride?.baseSemibold.fontWeight,
        fontStyle: textStyleOverride?.baseSemibold.fontStyle,
        letterSpacing: textStyleOverride?.baseSemibold.letterSpacing);

    final baseRegular = baseAppTextStyle.baseRegular.copyWith(
        fontFamily: textStyleOverride?.baseRegular.fontFamily,
        fontSize: textStyleOverride?.baseRegular.fontSize,
        fontWeight: textStyleOverride?.baseRegular.fontWeight,
        fontStyle: textStyleOverride?.baseRegular.fontStyle,
        letterSpacing: textStyleOverride?.baseRegular.letterSpacing);

    final smallBold = baseAppTextStyle.smallBold.copyWith(
        fontFamily: textStyleOverride?.smallBold.fontFamily,
        fontSize: textStyleOverride?.smallBold.fontSize,
        fontWeight: textStyleOverride?.smallBold.fontWeight,
        fontStyle: textStyleOverride?.smallBold.fontStyle,
        letterSpacing: textStyleOverride?.smallBold.letterSpacing);

    final smallSemibold = baseAppTextStyle.smallSemibold.copyWith(
        fontFamily: textStyleOverride?.smallSemibold.fontFamily,
        fontSize: textStyleOverride?.smallSemibold.fontSize,
        fontWeight: textStyleOverride?.smallSemibold.fontWeight,
        fontStyle: textStyleOverride?.smallSemibold.fontStyle,
        letterSpacing: textStyleOverride?.smallSemibold.letterSpacing);

    final smallRegular = baseAppTextStyle.smallRegular.copyWith(
        fontFamily: textStyleOverride?.smallRegular.fontFamily,
        fontSize: textStyleOverride?.smallRegular.fontSize,
        fontWeight: textStyleOverride?.smallRegular.fontWeight,
        fontStyle: textStyleOverride?.smallRegular.fontStyle,
        letterSpacing: textStyleOverride?.smallRegular.letterSpacing);

    final tapBar = baseAppTextStyle.tapBar.copyWith(
        fontFamily: textStyleOverride?.tapBar.fontFamily,
        fontSize: textStyleOverride?.tapBar.fontSize,
        fontWeight: textStyleOverride?.tapBar.fontWeight,
        fontStyle: textStyleOverride?.tapBar.fontStyle,
        letterSpacing: textStyleOverride?.tapBar.letterSpacing);

    final button = baseAppTextStyle.button.copyWith(
        fontFamily: textStyleOverride?.button.fontFamily,
        fontSize: textStyleOverride?.button.fontSize,
        fontWeight: textStyleOverride?.button.fontWeight,
        fontStyle: textStyleOverride?.button.fontStyle,
        letterSpacing: textStyleOverride?.button.letterSpacing);

    final date = baseAppTextStyle.date.copyWith(
        fontFamily: textStyleOverride?.date.fontFamily,
        fontSize: textStyleOverride?.date.fontSize,
        fontWeight: textStyleOverride?.date.fontWeight,
        fontStyle: textStyleOverride?.date.fontStyle,
        letterSpacing: textStyleOverride?.date.letterSpacing);

    final smallCaption = baseAppTextStyle.smallCaption.copyWith(
        fontFamily: textStyleOverride?.smallCaption.fontFamily,
        fontSize: textStyleOverride?.smallCaption.fontSize,
        fontWeight: textStyleOverride?.smallCaption.fontWeight,
        fontStyle: textStyleOverride?.smallCaption.fontStyle,
        letterSpacing: textStyleOverride?.smallCaption.letterSpacing);

    final _effectiveCFOnSuccessColorSchemeNode = baseColorScheme
        .cfSuccess.cfOnSuccess
        .copyWith(color: overridedColorScheme?.cfSuccess?.cfOnSuccess.color);

    final effectiveCFSuccessColorSchemeNode =
        baseColorScheme.cfSuccess.copyWith(
      color: overridedColorScheme?.cfSuccess?.color,
      cfOnSuccess: _effectiveCFOnSuccessColorSchemeNode,
    );

    final _effectiveCFOnWarningColorSchemeNode =
        baseColorScheme.cfWarning.cfOnWarning.copyWith(
      color: overridedColorScheme?.cfWarning?.cfOnWarning?.color,
    );

    final effectiveCFWarningColorSchemeNode = baseColorScheme.cfWarning
        .copyWith(
            color: overridedColorScheme?.cfWarning?.color,
            cfOnWarning: _effectiveCFOnWarningColorSchemeNode);

    final _effectiveCFOnPrimaryColorSchemeNode =
        baseColorScheme.cfPrimary.cfOnPrimary.copyWith(
      color: overridedColorScheme?.cfPrimary?.cfOnPrimary?.color,
    );

    final effectiveCFPrimaryColorSchemeNode =
        baseColorScheme.cfPrimary.copyWith(
      color: overridedColorScheme?.cfPrimary?.color,
      cfOnPrimary: _effectiveCFOnPrimaryColorSchemeNode,
    );

    final _effectiveCFOnAccentColorSchemeNode =
        baseColorScheme.cfAccent.cfOnAccent.copyWith(
      color: overridedColorScheme?.cfAccent?.cfOnAccent?.color,
    );

    final effectiveCFAccentColorSchemeNode = baseColorScheme.cfAccent.copyWith(
      color: overridedColorScheme?.cfAccent?.color,
      cfOnAccent: _effectiveCFOnAccentColorSchemeNode,
    );

    // background

    final _effectiveNPrimaryColorSchemeNode =
        baseColorScheme.bPrimary.nPrimary.copyWith(
      color: overridedColorScheme?.bPrimary?.nPrimary?.color,
    );

    final _effectiveNSecondary100ColorSchemeNode =
        baseColorScheme.bPrimary.nSecondary100.copyWith(
      color: overridedColorScheme?.bPrimary?.nSecondary100?.color,
    );

    final _effectiveNSecondary200ColorSchemeNode =
        baseColorScheme.bPrimary.nSecondary200.copyWith(
      color: overridedColorScheme?.bPrimary?.nSecondary200?.color,
    );

    final _effectiveNSecondary300ColorSchemeNode =
        baseColorScheme.bPrimary.nSecondary300.copyWith(
      color: overridedColorScheme?.bPrimary?.nSecondary300?.color,
    );

    final _effectiveNSecondary400ColorSchemeNode =
        baseColorScheme.bPrimary.nSecondary400.copyWith(
      color: overridedColorScheme?.bPrimary?.nSecondary400?.color,
    );

    final _effectiveNSecondary500ColorSchemeNode =
        baseColorScheme.bPrimary.nSecondary500.copyWith(
      color: overridedColorScheme?.bPrimary?.nSecondary500?.color,
    );

    final effectiveBPrimaryColorSchemeNode = baseColorScheme.bPrimary.copyWith(
      color: overridedColorScheme?.bPrimary?.color,
      nPrimary: _effectiveNPrimaryColorSchemeNode,
      nSecondary100: _effectiveNSecondary100ColorSchemeNode,
      nSecondary200: _effectiveNSecondary200ColorSchemeNode,
      nSecondary300: _effectiveNSecondary300ColorSchemeNode,
      nSecondary400: _effectiveNSecondary400ColorSchemeNode,
      nSecondary500: _effectiveNSecondary500ColorSchemeNode,
    );

    final effectiveBSecondary100ColorSchemeNode =
        baseColorScheme.bSecondary100.copyWith(
      color: overridedColorScheme?.bSecondary100?.color,
      nPrimary: _effectiveNPrimaryColorSchemeNode,
      nSecondary100: _effectiveNSecondary100ColorSchemeNode,
      nSecondary200: _effectiveNSecondary200ColorSchemeNode,
      nSecondary300: _effectiveNSecondary300ColorSchemeNode,
      nSecondary400: _effectiveNSecondary400ColorSchemeNode,
      nSecondary500: _effectiveNSecondary500ColorSchemeNode,
    );
    final effectiveBSecondary200ColorSchemeNode =
        baseColorScheme.bSecondary200.copyWith(
      color: overridedColorScheme?.bSecondary200?.color,
      nPrimary: _effectiveNPrimaryColorSchemeNode,
      nSecondary100: _effectiveNSecondary100ColorSchemeNode,
      nSecondary200: _effectiveNSecondary200ColorSchemeNode,
      nSecondary300: _effectiveNSecondary300ColorSchemeNode,
      nSecondary400: _effectiveNSecondary400ColorSchemeNode,
      nSecondary500: _effectiveNSecondary500ColorSchemeNode,
    );
    final effectiveBSecondary300ColorSchemeNode =
        baseColorScheme.bSecondary300.copyWith(
      color: overridedColorScheme?.bSecondary300?.color,
      nPrimary: _effectiveNPrimaryColorSchemeNode,
      nSecondary100: _effectiveNSecondary100ColorSchemeNode,
      nSecondary200: _effectiveNSecondary200ColorSchemeNode,
      nSecondary300: _effectiveNSecondary300ColorSchemeNode,
      nSecondary400: _effectiveNSecondary400ColorSchemeNode,
      nSecondary500: _effectiveNSecondary500ColorSchemeNode,
    );
    final effectiveBTransparent100ColorSchemeNode =
        baseColorScheme.bTransparent100.copyWith(
      color: overridedColorScheme?.bTranparent100?.color,
      nPrimary: _effectiveNPrimaryColorSchemeNode,
      nSecondary100: _effectiveNSecondary100ColorSchemeNode,
      nSecondary200: _effectiveNSecondary200ColorSchemeNode,
      nSecondary300: _effectiveNSecondary300ColorSchemeNode,
      nSecondary400: _effectiveNSecondary400ColorSchemeNode,
      nSecondary500: _effectiveNSecondary500ColorSchemeNode,
    );
    final effectiveOtherColors = baseColorScheme.other.copyWith(
        color: overridedColorScheme?.other.color,
        colors: overridedColorScheme?.other.colors);

    final effectiveOtherTransparentColors = baseColorScheme.otherTransparent
        .copyWith(
            color: overridedColorScheme?.otherTransparent.color,
            colors: overridedColorScheme?.otherTransparent.colors);

    final effectiveColorScheme = baseColorScheme.copyWith(
      cfSuccess: effectiveCFSuccessColorSchemeNode,
      cfWarning: effectiveCFWarningColorSchemeNode,
      cfPrimary: effectiveCFPrimaryColorSchemeNode,
      cfAccent: effectiveCFAccentColorSchemeNode,
      bPrimary: effectiveBPrimaryColorSchemeNode,
      bSecondary100: effectiveBSecondary100ColorSchemeNode,
      bSecondary200: effectiveBSecondary200ColorSchemeNode,
      bSecondary300: effectiveBSecondary300ColorSchemeNode,
      bTransparent100: effectiveBTransparent100ColorSchemeNode,
      other: effectiveOtherColors,
      otherTransparent: effectiveOtherTransparentColors,
    );
    final effectiveTextStyle = baseAppTextStyle.copyWith(
      heading1: heading1,
      heading2: heading2,
      heading3: heading3,
      heading4: heading4,
      largeBold: largeBold,
      largeSemibold: largeSemibold,
      largeRegular: largeRegular,
      baseBold: baseBold,
      baseSemibold: baseSemibold,
      baseRegular: baseRegular,
      smallBold: smallBold,
      smallSemibold: smallSemibold,
      smallRegular: smallRegular,
      smallCaption: smallCaption,
      tapBar: tapBar,
      button: button,
      date: date,
    );

    final effectiveThemeData = AppThemeData(
      colorScheme: effectiveColorScheme,
      textStyle: effectiveTextStyle,
    );

    return effectiveThemeData;
  }
}
