import 'package:flutter/material.dart';
import 'package:theme/theme.dart' as theme;

extension Theme on BuildContext {
  TextStyle get baseBold => theme.AppTheme.of(this).textStyle.baseBold;

  TextStyle get baseRegular => theme.AppTheme.of(this).textStyle.baseRegular;

  TextStyle get baseSemibold => theme.AppTheme.of(this).textStyle.baseSemibold;

  theme.BPrimaryColorSchemeNode get bPrimary =>
      theme.AppTheme.of(this).colorScheme.bPrimary;

  theme.BSecondary100ColorSchemeNode get bSecondary100 =>
      theme.AppTheme.of(this).colorScheme.bSecondary100;

  theme.BSecondary200ColorSchemeNode get bSecondary200 =>
      theme.AppTheme.of(this).colorScheme.bSecondary200;

  theme.BSecondary300ColorSchemeNode get bSecondary300 =>
      theme.AppTheme.of(this).colorScheme.bSecondary300;

  theme.BTransparent100ColorSchemeNode get bTransparent100 =>
      theme.AppTheme.of(this).colorScheme.bTransparent100;

  TextStyle get button => theme.AppTheme.of(this).textStyle.button;

  theme.CFAccentColorSchemeNode get cfAccent =>
      theme.AppTheme.of(this).colorScheme.cfAccent;

  theme.CFPrimaryColorSchemeNode get cfPrimary =>
      theme.AppTheme.of(this).colorScheme.cfPrimary;

  theme.CFSuccessColorSchemeNode get cfSuccess =>
      theme.AppTheme.of(this).colorScheme.cfSuccess;

  theme.CFWarningColorSchemeNode get cfWarning =>
      theme.AppTheme.of(this).colorScheme.cfWarning;

  theme.AppColorScheme get colorScheme => theme.AppTheme.of(this).colorScheme;

  TextStyle get date => theme.AppTheme.of(this).textStyle.date;

  TextStyle get heading1 => theme.AppTheme.of(this).textStyle.heading1;

  TextStyle get heading2 => theme.AppTheme.of(this).textStyle.heading2;

  TextStyle get heading3 => theme.AppTheme.of(this).textStyle.heading3;

  TextStyle get heading4 => theme.AppTheme.of(this).textStyle.heading4;

  TextStyle get largeBold => theme.AppTheme.of(this).textStyle.largeBold;

  TextStyle get largeRegular => theme.AppTheme.of(this).textStyle.largeRegular;

  TextStyle get largeSemibold =>
      theme.AppTheme.of(this).textStyle.largeSemibold;

  theme.OtherColorSchemeNode get other =>
      theme.AppTheme.of(this).colorScheme.other;

  theme.OtherTransparentColorSchemeNode get otherTransparent =>
      theme.AppTheme.of(this).colorScheme.otherTransparent;

  TextStyle get smallBold => theme.AppTheme.of(this).textStyle.smallBold;

  TextStyle get smallCaption => theme.AppTheme.of(this).textStyle.smallCaption;

  TextStyle get smallRegular => theme.AppTheme.of(this).textStyle.smallRegular;

  TextStyle get smallSemibold =>
      theme.AppTheme.of(this).textStyle.smallSemibold;

  TextStyle get tapBar => theme.AppTheme.of(this).textStyle.tapBar;

  theme.AppTextStyle get textStyle => theme.AppTheme.of(this).textStyle;
}
