import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'app_color_scheme.dart';

Color _getColorFromCode(String colorCode) {
  return Color(int.parse(colorCode, radix: 16));
}

class AppColorSchemeDto extends Equatable {
  final CFPrimaryColorSchemeNodeDto cfPrimary;
  final CFAccentColorSchemeNodeDto cfAccent;
  final CFSuccessColorSchemeNodeDto cfSuccess;
  final CFWarningColorSchemeNodeDto cfWarning;
  final BPrimaryColorSchemeNodeDto bPrimary;
  final BSecondary100ColorSchemeNodeDto bSecondary100;
  final BSecondary200ColorSchemeNodeDto bSecondary200;
  final BSecondary300ColorSchemeNodeDto bSecondary300;
  final BTransparent100ColorSchemeNodeDto bTransparent100;
  final OtherColorSchemeNodeDto other;
  final OtherTransparentColorSchemeNodeDto otherTransparent;

  const AppColorSchemeDto({
    required this.cfSuccess,
    required this.cfWarning,
    required this.cfPrimary,
    required this.cfAccent,
    required this.bPrimary,
    required this.bSecondary100,
    required this.bSecondary200,
    required this.bSecondary300,
    required this.bTransparent100,
    required this.other,
    required this.otherTransparent,
  });

  factory AppColorSchemeDto.fromJson(Map<String, dynamic> json) {
    return AppColorSchemeDto(
      cfSuccess: CFSuccessColorSchemeNodeDto.fromJson(json['cfSuccess']),
      cfWarning: CFWarningColorSchemeNodeDto.fromJson(json['cfWarning']),
      cfPrimary: CFPrimaryColorSchemeNodeDto.fromJson(json['cfPrimary']),
      cfAccent: CFAccentColorSchemeNodeDto.fromJson(json['cfAccent']),
      bPrimary: BPrimaryColorSchemeNodeDto.fromJson(json['bPrimary']),
      bSecondary100:
          BSecondary100ColorSchemeNodeDto.fromJson(json['bSecondary100']),
      bSecondary200:
          BSecondary200ColorSchemeNodeDto.fromJson(json['bSecondary200']),
      bSecondary300:
          BSecondary300ColorSchemeNodeDto.fromJson(json['bSecondary300']),
      bTransparent100:
          BTransparent100ColorSchemeNodeDto.fromJson(json['bTransparent100']),
      other: OtherColorSchemeNodeDto.fromJson(json['other']),
      otherTransparent:
          OtherTransparentColorSchemeNodeDto.fromJson(json['otherTransparent']),
    );
  }

  @override
  List<Object?> get props {
    return [
      cfSuccess,
      cfWarning,
      cfPrimary,
      cfAccent,
      bPrimary,
      bSecondary100,
      bSecondary200,
      bSecondary300,
      other,
      otherTransparent,
    ];
  }

  AppColorScheme toAppColorScheme() {
    return AppColorScheme(
        cfSuccess: cfSuccess.toEntity(),
        cfWarning: cfWarning.toEntity(),
        cfPrimary: cfPrimary.toEntity(),
        cfAccent: cfAccent.toEntity(),
        bPrimary: bPrimary.toEntity(),
        bSecondary100: bSecondary100.toEntity(),
        bSecondary200: bSecondary200.toEntity(),
        bSecondary300: bSecondary300.toEntity(),
        bTransparent100: bTransparent100.toEntity(),
        other: other.toEntity(),
        otherTransparent: otherTransparent.toEntity());
  }
}

class BPrimaryColorSchemeNodeDto extends Equatable {
  final String color;
  final NPrimaryColorSchemeNodeDto nPrimary;
  final NSecondary100ColorSchemeNodeDto nSecondary100;
  final NSecondary200ColorSchemeNodeDto nSecondary200;
  final NSecondary300ColorSchemeNodeDto nSecondary300;
  final NSecondary400ColorSchemeNodeDto nSecondary400;
  final NSecondary500ColorSchemeNodeDto nSecondary500;

  const BPrimaryColorSchemeNodeDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BPrimaryColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return BPrimaryColorSchemeNodeDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeDto.fromJson(json['nPrimary']),
      nSecondary100:
          NSecondary100ColorSchemeNodeDto.fromJson(json['nSecondary100']),
      nSecondary200:
          NSecondary200ColorSchemeNodeDto.fromJson(json['nSecondary200']),
      nSecondary300:
          NSecondary300ColorSchemeNodeDto.fromJson(json['nSecondary300']),
      nSecondary400:
          NSecondary400ColorSchemeNodeDto.fromJson(json['nSecondary400']),
      nSecondary500:
          NSecondary500ColorSchemeNodeDto.fromJson(json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BPrimaryColorSchemeNode toEntity() {
    return BPrimaryColorSchemeNode(
      color: _getColorFromCode(color),
      nPrimary: nPrimary.toEntity(),
      nSecondary100: nSecondary100.toEntity(),
      nSecondary200: nSecondary200.toEntity(),
      nSecondary300: nSecondary300.toEntity(),
      nSecondary400: nSecondary400.toEntity(),
      nSecondary500: nSecondary500.toEntity(),
    );
  }
}

class BSecondary100ColorSchemeNodeDto extends Equatable {
  final String color;
  final NPrimaryColorSchemeNodeDto nPrimary;
  final NSecondary100ColorSchemeNodeDto nSecondary100;
  final NSecondary200ColorSchemeNodeDto nSecondary200;
  final NSecondary300ColorSchemeNodeDto nSecondary300;
  final NSecondary400ColorSchemeNodeDto nSecondary400;
  final NSecondary500ColorSchemeNodeDto nSecondary500;

  const BSecondary100ColorSchemeNodeDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BSecondary100ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return BSecondary100ColorSchemeNodeDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeDto.fromJson(json['nPrimary']),
      nSecondary100:
          NSecondary100ColorSchemeNodeDto.fromJson(json['nSecondary100']),
      nSecondary200:
          NSecondary200ColorSchemeNodeDto.fromJson(json['nSecondary200']),
      nSecondary300:
          NSecondary300ColorSchemeNodeDto.fromJson(json['nSecondary300']),
      nSecondary400:
          NSecondary400ColorSchemeNodeDto.fromJson(json['nSecondary400']),
      nSecondary500:
          NSecondary500ColorSchemeNodeDto.fromJson(json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary100ColorSchemeNode toEntity() {
    return BSecondary100ColorSchemeNode(
      color: _getColorFromCode(color),
      nPrimary: nPrimary.toEntity(),
      nSecondary100: nSecondary100.toEntity(),
      nSecondary200: nSecondary200.toEntity(),
      nSecondary300: nSecondary300.toEntity(),
      nSecondary400: nSecondary400.toEntity(),
      nSecondary500: nSecondary500.toEntity(),
    );
  }
}

class BSecondary200ColorSchemeNodeDto extends Equatable {
  final String color;
  final NPrimaryColorSchemeNodeDto nPrimary;
  final NSecondary100ColorSchemeNodeDto nSecondary100;
  final NSecondary200ColorSchemeNodeDto nSecondary200;
  final NSecondary300ColorSchemeNodeDto nSecondary300;
  final NSecondary400ColorSchemeNodeDto nSecondary400;
  final NSecondary500ColorSchemeNodeDto nSecondary500;

  const BSecondary200ColorSchemeNodeDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BSecondary200ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return BSecondary200ColorSchemeNodeDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeDto.fromJson(json['nPrimary']),
      nSecondary100:
          NSecondary100ColorSchemeNodeDto.fromJson(json['nSecondary100']),
      nSecondary200:
          NSecondary200ColorSchemeNodeDto.fromJson(json['nSecondary200']),
      nSecondary300:
          NSecondary300ColorSchemeNodeDto.fromJson(json['nSecondary300']),
      nSecondary400:
          NSecondary400ColorSchemeNodeDto.fromJson(json['nSecondary400']),
      nSecondary500:
          NSecondary500ColorSchemeNodeDto.fromJson(json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary200ColorSchemeNode toEntity() {
    return BSecondary200ColorSchemeNode(
      color: _getColorFromCode(color),
      nPrimary: nPrimary.toEntity(),
      nSecondary100: nSecondary100.toEntity(),
      nSecondary200: nSecondary200.toEntity(),
      nSecondary300: nSecondary300.toEntity(),
      nSecondary400: nSecondary400.toEntity(),
      nSecondary500: nSecondary500.toEntity(),
    );
  }
}

class BSecondary300ColorSchemeNodeDto extends Equatable {
  final String color;
  final NPrimaryColorSchemeNodeDto nPrimary;
  final NSecondary100ColorSchemeNodeDto nSecondary100;
  final NSecondary200ColorSchemeNodeDto nSecondary200;
  final NSecondary300ColorSchemeNodeDto nSecondary300;
  final NSecondary400ColorSchemeNodeDto nSecondary400;
  final NSecondary500ColorSchemeNodeDto nSecondary500;

  const BSecondary300ColorSchemeNodeDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BSecondary300ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return BSecondary300ColorSchemeNodeDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeDto.fromJson(json['nPrimary']),
      nSecondary100:
          NSecondary100ColorSchemeNodeDto.fromJson(json['nSecondary100']),
      nSecondary200:
          NSecondary200ColorSchemeNodeDto.fromJson(json['nSecondary200']),
      nSecondary300:
          NSecondary300ColorSchemeNodeDto.fromJson(json['nSecondary300']),
      nSecondary400:
          NSecondary400ColorSchemeNodeDto.fromJson(json['nSecondary400']),
      nSecondary500:
          NSecondary500ColorSchemeNodeDto.fromJson(json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary300ColorSchemeNode toEntity() {
    return BSecondary300ColorSchemeNode(
      color: _getColorFromCode(color),
      nPrimary: nPrimary.toEntity(),
      nSecondary100: nSecondary100.toEntity(),
      nSecondary200: nSecondary200.toEntity(),
      nSecondary300: nSecondary300.toEntity(),
      nSecondary400: nSecondary400.toEntity(),
      nSecondary500: nSecondary500.toEntity(),
    );
  }
}

class BTransparent100ColorSchemeNodeDto extends Equatable {
  final String color;
  final NPrimaryColorSchemeNodeDto nPrimary;
  final NSecondary100ColorSchemeNodeDto nSecondary100;
  final NSecondary200ColorSchemeNodeDto nSecondary200;
  final NSecondary300ColorSchemeNodeDto nSecondary300;
  final NSecondary400ColorSchemeNodeDto nSecondary400;
  final NSecondary500ColorSchemeNodeDto nSecondary500;

  const BTransparent100ColorSchemeNodeDto({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  factory BTransparent100ColorSchemeNodeDto.fromJson(
      Map<String, dynamic> json) {
    return BTransparent100ColorSchemeNodeDto(
      color: json['color'],
      nPrimary: NPrimaryColorSchemeNodeDto.fromJson(json['nPrimary']),
      nSecondary100:
          NSecondary100ColorSchemeNodeDto.fromJson(json['nSecondary100']),
      nSecondary200:
          NSecondary200ColorSchemeNodeDto.fromJson(json['nSecondary200']),
      nSecondary300:
          NSecondary300ColorSchemeNodeDto.fromJson(json['nSecondary300']),
      nSecondary400:
          NSecondary400ColorSchemeNodeDto.fromJson(json['nSecondary400']),
      nSecondary500:
          NSecondary500ColorSchemeNodeDto.fromJson(json['nSecondary500']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BTransparent100ColorSchemeNode toEntity() {
    return BTransparent100ColorSchemeNode(
      color: _getColorFromCode(color),
      nPrimary: nPrimary.toEntity(),
      nSecondary100: nSecondary100.toEntity(),
      nSecondary200: nSecondary200.toEntity(),
      nSecondary300: nSecondary300.toEntity(),
      nSecondary400: nSecondary400.toEntity(),
      nSecondary500: nSecondary500.toEntity(),
    );
  }
}

class CFAccentColorSchemeNodeDto extends Equatable {
  final String color;
  final CFOnAccentColorSchemeNodeDto cfOnAccent;

  const CFAccentColorSchemeNodeDto({
    required this.color,
    required this.cfOnAccent,
  });

  factory CFAccentColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFAccentColorSchemeNodeDto(
      color: json['color'],
      cfOnAccent: CFOnAccentColorSchemeNodeDto.fromJson(json['cfOnAccent']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      cfOnAccent,
    ];
  }

  CFAccentColorSchemeNode toEntity() {
    return CFAccentColorSchemeNode(
      color: _getColorFromCode(color),
      cfOnAccent: cfOnAccent.toEntity(),
    );
  }
}

class CFOnAccentColorSchemeNodeDto extends Equatable {
  final String color;

  const CFOnAccentColorSchemeNodeDto({
    required this.color,
  });

  factory CFOnAccentColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFOnAccentColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnAccentColorSchemeNode toEntity() {
    return CFOnAccentColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class CFOnPrimaryColorSchemeNodeDto extends Equatable {
  final String color;

  const CFOnPrimaryColorSchemeNodeDto({
    required this.color,
  });

  factory CFOnPrimaryColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFOnPrimaryColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnPrimaryColorSchemeNode toEntity() {
    return CFOnPrimaryColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class CFOnSuccessColorSchemeNodeDto extends Equatable {
  final String color;

  const CFOnSuccessColorSchemeNodeDto({
    required this.color,
  });

  factory CFOnSuccessColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFOnSuccessColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnSuccessColorSchemeNode toEntity() {
    return CFOnSuccessColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class CFOnWarningColorSchemeNodeDto extends Equatable {
  final String color;

  const CFOnWarningColorSchemeNodeDto({
    required this.color,
  });

  factory CFOnWarningColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFOnWarningColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnWarningColorSchemeNode toEntity() {
    return CFOnWarningColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class CFPrimaryColorSchemeNodeDto extends Equatable {
  final String color;
  final CFOnPrimaryColorSchemeNodeDto cfOnPrimary;

  const CFPrimaryColorSchemeNodeDto({
    required this.color,
    required this.cfOnPrimary,
  });

  factory CFPrimaryColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFPrimaryColorSchemeNodeDto(
      color: json['color'],
      cfOnPrimary: CFOnPrimaryColorSchemeNodeDto.fromJson(json['cfOnPrimary']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      cfOnPrimary,
    ];
  }

  CFPrimaryColorSchemeNode toEntity() {
    return CFPrimaryColorSchemeNode(
      color: _getColorFromCode(color),
      cfOnPrimary: cfOnPrimary.toEntity(),
    );
  }
}

class CFSuccessColorSchemeNodeDto extends Equatable {
  final String color;
  final CFOnSuccessColorSchemeNodeDto cfOnSuccess;

  const CFSuccessColorSchemeNodeDto(
      {required this.color, required this.cfOnSuccess});

  factory CFSuccessColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFSuccessColorSchemeNodeDto(
      color: json['color'],
      cfOnSuccess: CFOnSuccessColorSchemeNodeDto.fromJson(json['cfOnSuccess']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      cfOnSuccess,
    ];
  }

  CFSuccessColorSchemeNode toEntity() {
    return CFSuccessColorSchemeNode(
      color: _getColorFromCode(color),
      cfOnSuccess: cfOnSuccess.toEntity(),
    );
  }
}

class CFWarningColorSchemeNodeDto extends Equatable {
  final String color;
  final CFOnWarningColorSchemeNodeDto cfOnWarning;

  const CFWarningColorSchemeNodeDto({
    required this.color,
    required this.cfOnWarning,
  });

  factory CFWarningColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return CFWarningColorSchemeNodeDto(
      color: json['color'],
      cfOnWarning: CFOnWarningColorSchemeNodeDto.fromJson(json['cfOnWarning']),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      cfOnWarning,
    ];
  }

  CFWarningColorSchemeNode toEntity() {
    return CFWarningColorSchemeNode(
        color: _getColorFromCode(color), cfOnWarning: cfOnWarning.toEntity());
  }
}

class NPrimaryColorSchemeNodeDto extends Equatable {
  final String color;

  const NPrimaryColorSchemeNodeDto({
    required this.color,
  });

  factory NPrimaryColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return NPrimaryColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NPrimaryColorSchemeNode toEntity() {
    return NPrimaryColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary100ColorSchemeNodeDto extends Equatable {
  final String color;

  const NSecondary100ColorSchemeNodeDto({
    required this.color,
  });

  factory NSecondary100ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return NSecondary100ColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary100ColorSchemeNode toEntity() {
    return NSecondary100ColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary200ColorSchemeNodeDto extends Equatable {
  final String color;

  const NSecondary200ColorSchemeNodeDto({
    required this.color,
  });

  factory NSecondary200ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return NSecondary200ColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary200ColorSchemeNode toEntity() {
    return NSecondary200ColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary300ColorSchemeNodeDto extends Equatable {
  final String color;

  const NSecondary300ColorSchemeNodeDto({
    required this.color,
  });

  factory NSecondary300ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return NSecondary300ColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary300ColorSchemeNode toEntity() {
    return NSecondary300ColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary400ColorSchemeNodeDto extends Equatable {
  final String color;

  const NSecondary400ColorSchemeNodeDto({
    required this.color,
  });

  factory NSecondary400ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return NSecondary400ColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary400ColorSchemeNode toEntity() {
    return NSecondary400ColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class NSecondary500ColorSchemeNodeDto extends Equatable {
  final String color;

  const NSecondary500ColorSchemeNodeDto({
    required this.color,
  });

  factory NSecondary500ColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return NSecondary500ColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary500ColorSchemeNode toEntity() {
    return NSecondary500ColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}

class OtherColorSchemeNodeDto extends Equatable {
  final String color;
  final Map<int, Color> colors;

  const OtherColorSchemeNodeDto({
    required this.color,
    required this.colors,
  });

  factory OtherColorSchemeNodeDto.fromJson(Map<String, dynamic> json) {
    return OtherColorSchemeNodeDto(
      color: json['color'] ?? '',
      colors: Map.castFrom(json['colors']?.map((key, value) =>
              MapEntry(int.parse(key), _getColorFromCode(value))) ??
          {}),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }

  OtherColorSchemeNode toEntity() {
    return OtherColorSchemeNode(
      color: _getColorFromCode(color),
      colors: colors,
    );
  }
}

class OtherTransparentColorSchemeNodeDto extends Equatable {
  final String color;
  final Map<int, Color> colors;

  const OtherTransparentColorSchemeNodeDto({
    required this.color,
    required this.colors,
  });

  factory OtherTransparentColorSchemeNodeDto.fromJson(
      Map<String, dynamic> json) {
    return OtherTransparentColorSchemeNodeDto(
      color: json['color'],
      colors: Map.castFrom(json['colors']?.map((key, value) =>
              MapEntry(int.parse(key), _getColorFromCode(value))) ??
          {}),
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }

  OtherTransparentColorSchemeNode toEntity() {
    return OtherTransparentColorSchemeNode(
      color: _getColorFromCode(color),
      colors: colors,
    );
  }
}

class PrimaryXContrastingColorSchemeNodeDto extends Equatable {
  final String color;

  const PrimaryXContrastingColorSchemeNodeDto({
    required this.color,
  });

  factory PrimaryXContrastingColorSchemeNodeDto.fromJson(
      Map<String, dynamic> json) {
    return PrimaryXContrastingColorSchemeNodeDto(
      color: json['color'],
    );
  }

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnPrimaryColorSchemeNode toEntity() {
    return CFOnPrimaryColorSchemeNode(
      color: _getColorFromCode(color),
    );
  }
}
