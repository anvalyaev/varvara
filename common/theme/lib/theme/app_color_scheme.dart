import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

/// Color Full
/// cf.primary - [CFPrimaryColorSchemeNode] cfPrimary – основной цвет в приложении
/// cf.accent - [CFAccentColorSchemeNode] cfAccent – второстепенный цвет в приложении
/// cf.success - [CFSuccessColorSchemeNode] cfSuccess – цвет, показывающий положительный статус
/// cf.warning - [CFWarningColorSchemeNode] cfWarning – цвет для отображения ситуации, когда что-то идёт не так
///
///
/// Background
/// b.primary - [BPrimaryColorSchemeNode] bPrimary – основной цвет фона.
/// b.secondary100 - [BSecondary100ColorSchemeNode] bSecondary100 – второстепенный цвет фона 100
/// b.secondary200 - [BSecondary200ColorSchemeNode] bSecondary200 – второстепенный цвет фона 200
/// b.secondary300 - [BSecondary300ColorSchemeNode] bSecondary300 – второстепенный цвет фона 300
///
/// Neutral используются как он цвета для Background
/// n.primary - [NPrimaryColorSchemeNode] nPrimary;
/// n.secondary100 - [NSecondary100ColorSchemeNode] nSecondary100;
/// n.secondary200 - [NSecondary200ColorSchemeNode] nSecondary200;
/// n.secondary300 - [NSecondary300ColorSchemeNode] nSecondary300;
/// n.secondary400 - [NSecondary400ColorSchemeNode] nSecondary400;
/// n.secondary500 - [NSecondary500ColorSchemeNode] nSecondary500;

/// TODO поправить описание
class AppColorScheme extends Equatable {
  final CFPrimaryColorSchemeNode cfPrimary;
  final CFAccentColorSchemeNode cfAccent;
  final CFSuccessColorSchemeNode cfSuccess;
  final CFWarningColorSchemeNode cfWarning;
  final BPrimaryColorSchemeNode bPrimary;
  final BSecondary100ColorSchemeNode bSecondary100;
  final BSecondary200ColorSchemeNode bSecondary200;
  final BSecondary300ColorSchemeNode bSecondary300;
  final BTransparent100ColorSchemeNode bTransparent100;
  final OtherColorSchemeNode other;
  final OtherTransparentColorSchemeNode otherTransparent;

  const AppColorScheme({
    required this.cfSuccess,
    required this.cfWarning,
    required this.cfPrimary,
    required this.cfAccent,
    required this.bPrimary,
    required this.bSecondary100,
    required this.bSecondary200,
    required this.bSecondary300,
    required this.bTransparent100,
    required this.other,
    required this.otherTransparent,
  });

  factory AppColorScheme.empty() {
    return const AppColorScheme(
        cfPrimary: CFPrimaryColorSchemeNode(
          color: Colors.transparent,
          cfOnPrimary: CFOnPrimaryColorSchemeNode(
            color: Colors.transparent,
          ),
        ),
        cfAccent: CFAccentColorSchemeNode(
          color: Colors.transparent,
          cfOnAccent: CFOnAccentColorSchemeNode(
            color: Colors.transparent,
          ),
        ),
        cfSuccess: CFSuccessColorSchemeNode(
            color: Colors.transparent,
            cfOnSuccess: CFOnSuccessColorSchemeNode(color: Colors.transparent)),
        cfWarning: CFWarningColorSchemeNode(
          color: Colors.transparent,
          cfOnWarning: CFOnWarningColorSchemeNode(color: Colors.transparent),
        ),
        bPrimary: BPrimaryColorSchemeNode(
          color: Colors.transparent,
          nPrimary: NPrimaryColorSchemeNode(color: Colors.transparent),
          nSecondary100:
              NSecondary100ColorSchemeNode(color: Colors.transparent),
          nSecondary200:
              NSecondary200ColorSchemeNode(color: Colors.transparent),
          nSecondary300:
              NSecondary300ColorSchemeNode(color: Colors.transparent),
          nSecondary400:
              NSecondary400ColorSchemeNode(color: Colors.transparent),
          nSecondary500:
              NSecondary500ColorSchemeNode(color: Colors.transparent),
        ),
        bSecondary100: BSecondary100ColorSchemeNode(
          color: Colors.transparent,
          nPrimary: NPrimaryColorSchemeNode(color: Colors.transparent),
          nSecondary100:
              NSecondary100ColorSchemeNode(color: Colors.transparent),
          nSecondary200:
              NSecondary200ColorSchemeNode(color: Colors.transparent),
          nSecondary300:
              NSecondary300ColorSchemeNode(color: Colors.transparent),
          nSecondary400:
              NSecondary400ColorSchemeNode(color: Colors.transparent),
          nSecondary500:
              NSecondary500ColorSchemeNode(color: Colors.transparent),
        ),
        bSecondary200: BSecondary200ColorSchemeNode(
          color: Colors.transparent,
          nPrimary: NPrimaryColorSchemeNode(color: Colors.transparent),
          nSecondary100:
              NSecondary100ColorSchemeNode(color: Colors.transparent),
          nSecondary200:
              NSecondary200ColorSchemeNode(color: Colors.transparent),
          nSecondary300:
              NSecondary300ColorSchemeNode(color: Colors.transparent),
          nSecondary400:
              NSecondary400ColorSchemeNode(color: Colors.transparent),
          nSecondary500:
              NSecondary500ColorSchemeNode(color: Colors.transparent),
        ),
        bSecondary300: BSecondary300ColorSchemeNode(
          color: Colors.transparent,
          nPrimary: NPrimaryColorSchemeNode(color: Colors.transparent),
          nSecondary100:
              NSecondary100ColorSchemeNode(color: Colors.transparent),
          nSecondary200:
              NSecondary200ColorSchemeNode(color: Colors.transparent),
          nSecondary300:
              NSecondary300ColorSchemeNode(color: Colors.transparent),
          nSecondary400:
              NSecondary400ColorSchemeNode(color: Colors.transparent),
          nSecondary500:
              NSecondary500ColorSchemeNode(color: Colors.transparent),
        ),
        bTransparent100: BTransparent100ColorSchemeNode(
          color: Colors.transparent,
          nPrimary: NPrimaryColorSchemeNode(color: Colors.transparent),
          nSecondary100:
              NSecondary100ColorSchemeNode(color: Colors.transparent),
          nSecondary200:
              NSecondary200ColorSchemeNode(color: Colors.transparent),
          nSecondary300:
              NSecondary300ColorSchemeNode(color: Colors.transparent),
          nSecondary400:
              NSecondary400ColorSchemeNode(color: Colors.transparent),
          nSecondary500:
              NSecondary500ColorSchemeNode(color: Colors.transparent),
        ),
        other: OtherColorSchemeNode(color: Colors.transparent, colors: {}),
        otherTransparent: OtherTransparentColorSchemeNode(
            color: Colors.transparent, colors: {}));
  }

  @override
  List<Object?> get props {
    return [
      cfSuccess,
      cfWarning,
      cfPrimary,
      cfAccent,
      bPrimary,
      bSecondary100,
      bSecondary200,
      bSecondary300,
    ];
  }

  AppColorScheme copyWith({
    required CFSuccessColorSchemeNode? cfSuccess,
    required CFWarningColorSchemeNode? cfWarning,
    required CFPrimaryColorSchemeNode? cfPrimary,
    required CFAccentColorSchemeNode? cfAccent,
    required BPrimaryColorSchemeNode? bPrimary,
    required BSecondary100ColorSchemeNode? bSecondary100,
    required BSecondary200ColorSchemeNode? bSecondary200,
    required BSecondary300ColorSchemeNode? bSecondary300,
    required BTransparent100ColorSchemeNode? bTransparent100,
    required OtherColorSchemeNode? other,
    required OtherTransparentColorSchemeNode? otherTransparent,
  }) {
    return AppColorScheme(
      cfSuccess: cfSuccess ?? this.cfSuccess,
      cfWarning: cfWarning ?? this.cfWarning,
      cfPrimary: cfPrimary ?? this.cfPrimary,
      cfAccent: cfAccent ?? this.cfAccent,
      bPrimary: bPrimary ?? this.bPrimary,
      bSecondary100: bSecondary100 ?? this.bSecondary100,
      bSecondary200: bSecondary200 ?? this.bSecondary200,
      bSecondary300: bSecondary300 ?? this.bSecondary300,
      bTransparent100: bTransparent100 ?? this.bTransparent100,
      other: other ?? this.other,
      otherTransparent: otherTransparent ?? this.otherTransparent,
    );
  }
}

abstract class AppColorSchemeNode {
  Color get color;
}

class BPrimaryColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;

  final NPrimaryColorSchemeNode nPrimary;
  final NSecondary100ColorSchemeNode nSecondary100;
  final NSecondary200ColorSchemeNode nSecondary200;
  final NSecondary300ColorSchemeNode nSecondary300;
  final NSecondary400ColorSchemeNode nSecondary400;
  final NSecondary500ColorSchemeNode nSecondary500;

  const BPrimaryColorSchemeNode({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BPrimaryColorSchemeNode copyWith({
    required Color? color,
    required NPrimaryColorSchemeNode? nPrimary,
    required NSecondary100ColorSchemeNode? nSecondary100,
    required NSecondary200ColorSchemeNode? nSecondary200,
    required NSecondary300ColorSchemeNode? nSecondary300,
    required NSecondary400ColorSchemeNode? nSecondary400,
    required NSecondary500ColorSchemeNode? nSecondary500,
  }) {
    return BPrimaryColorSchemeNode(
      color: color ?? this.color,
      nPrimary: nPrimary ?? this.nPrimary,
      nSecondary100: nSecondary100 ?? this.nSecondary100,
      nSecondary200: nSecondary200 ?? this.nSecondary200,
      nSecondary300: nSecondary300 ?? this.nSecondary300,
      nSecondary400: nSecondary400 ?? this.nSecondary400,
      nSecondary500: nSecondary500 ?? this.nSecondary500,
    );
  }
}

class BSecondary100ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  final NPrimaryColorSchemeNode nPrimary;
  final NSecondary100ColorSchemeNode nSecondary100;
  final NSecondary200ColorSchemeNode nSecondary200;
  final NSecondary300ColorSchemeNode nSecondary300;
  final NSecondary400ColorSchemeNode nSecondary400;
  final NSecondary500ColorSchemeNode nSecondary500;

  const BSecondary100ColorSchemeNode({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary100ColorSchemeNode copyWith({
    required Color? color,
    required NPrimaryColorSchemeNode? nPrimary,
    required NSecondary100ColorSchemeNode? nSecondary100,
    required NSecondary200ColorSchemeNode? nSecondary200,
    required NSecondary300ColorSchemeNode? nSecondary300,
    required NSecondary400ColorSchemeNode? nSecondary400,
    required NSecondary500ColorSchemeNode? nSecondary500,
  }) {
    return BSecondary100ColorSchemeNode(
      color: color ?? this.color,
      nPrimary: nPrimary ?? this.nPrimary,
      nSecondary100: nSecondary100 ?? this.nSecondary100,
      nSecondary200: nSecondary200 ?? this.nSecondary200,
      nSecondary300: nSecondary300 ?? this.nSecondary300,
      nSecondary400: nSecondary400 ?? this.nSecondary400,
      nSecondary500: nSecondary500 ?? this.nSecondary500,
    );
  }
}

class BSecondary200ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  final NPrimaryColorSchemeNode nPrimary;
  final NSecondary100ColorSchemeNode nSecondary100;
  final NSecondary200ColorSchemeNode nSecondary200;
  final NSecondary300ColorSchemeNode nSecondary300;
  final NSecondary400ColorSchemeNode nSecondary400;
  final NSecondary500ColorSchemeNode nSecondary500;

  const BSecondary200ColorSchemeNode({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary200ColorSchemeNode copyWith({
    required Color? color,
    required NPrimaryColorSchemeNode? nPrimary,
    required NSecondary100ColorSchemeNode? nSecondary100,
    required NSecondary200ColorSchemeNode? nSecondary200,
    required NSecondary300ColorSchemeNode? nSecondary300,
    required NSecondary400ColorSchemeNode? nSecondary400,
    required NSecondary500ColorSchemeNode? nSecondary500,
  }) {
    return BSecondary200ColorSchemeNode(
      color: color ?? this.color,
      nPrimary: nPrimary ?? this.nPrimary,
      nSecondary100: nSecondary100 ?? this.nSecondary100,
      nSecondary200: nSecondary200 ?? this.nSecondary200,
      nSecondary300: nSecondary300 ?? this.nSecondary300,
      nSecondary400: nSecondary400 ?? this.nSecondary400,
      nSecondary500: nSecondary500 ?? this.nSecondary500,
    );
  }
}

class BSecondary300ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  final NPrimaryColorSchemeNode nPrimary;
  final NSecondary100ColorSchemeNode nSecondary100;
  final NSecondary200ColorSchemeNode nSecondary200;
  final NSecondary300ColorSchemeNode nSecondary300;
  final NSecondary400ColorSchemeNode nSecondary400;
  final NSecondary500ColorSchemeNode nSecondary500;

  const BSecondary300ColorSchemeNode({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BSecondary300ColorSchemeNode copyWith({
    required Color? color,
    required NPrimaryColorSchemeNode? nPrimary,
    required NSecondary100ColorSchemeNode? nSecondary100,
    required NSecondary200ColorSchemeNode? nSecondary200,
    required NSecondary300ColorSchemeNode? nSecondary300,
    required NSecondary400ColorSchemeNode? nSecondary400,
    required NSecondary500ColorSchemeNode? nSecondary500,
  }) {
    return BSecondary300ColorSchemeNode(
      color: color ?? this.color,
      nPrimary: nPrimary ?? this.nPrimary,
      nSecondary100: nSecondary100 ?? this.nSecondary100,
      nSecondary200: nSecondary200 ?? this.nSecondary200,
      nSecondary300: nSecondary300 ?? this.nSecondary300,
      nSecondary400: nSecondary400 ?? this.nSecondary400,
      nSecondary500: nSecondary500 ?? this.nSecondary500,
    );
  }
}

class BTransparent100ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  final NPrimaryColorSchemeNode nPrimary;
  final NSecondary100ColorSchemeNode nSecondary100;
  final NSecondary200ColorSchemeNode nSecondary200;
  final NSecondary300ColorSchemeNode nSecondary300;
  final NSecondary400ColorSchemeNode nSecondary400;
  final NSecondary500ColorSchemeNode nSecondary500;

  const BTransparent100ColorSchemeNode({
    required this.color,
    required this.nPrimary,
    required this.nSecondary100,
    required this.nSecondary200,
    required this.nSecondary300,
    required this.nSecondary400,
    required this.nSecondary500,
  });

  @override
  List<Object?> get props {
    return [
      color,
      nPrimary,
      nSecondary100,
      nSecondary200,
      nSecondary300,
      nSecondary400,
      nSecondary500,
    ];
  }

  BTransparent100ColorSchemeNode copyWith({
    required Color? color,
    required NPrimaryColorSchemeNode? nPrimary,
    required NSecondary100ColorSchemeNode? nSecondary100,
    required NSecondary200ColorSchemeNode? nSecondary200,
    required NSecondary300ColorSchemeNode? nSecondary300,
    required NSecondary400ColorSchemeNode? nSecondary400,
    required NSecondary500ColorSchemeNode? nSecondary500,
  }) {
    return BTransparent100ColorSchemeNode(
      color: color ?? this.color,
      nPrimary: nPrimary ?? this.nPrimary,
      nSecondary100: nSecondary100 ?? this.nSecondary100,
      nSecondary200: nSecondary200 ?? this.nSecondary200,
      nSecondary300: nSecondary300 ?? this.nSecondary300,
      nSecondary400: nSecondary400 ?? this.nSecondary400,
      nSecondary500: nSecondary500 ?? this.nSecondary500,
    );
  }
}

class CFAccentColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;

  final CFOnAccentColorSchemeNode cfOnAccent;

  const CFAccentColorSchemeNode({
    required this.color,
    required this.cfOnAccent,
  });

  @override
  List<Object?> get props {
    return [
      color,
      cfOnAccent,
    ];
  }

  CFAccentColorSchemeNode copyWith({
    required Color? color,
    required CFOnAccentColorSchemeNode? cfOnAccent,
  }) {
    return CFAccentColorSchemeNode(
      color: color ?? this.color,
      cfOnAccent: cfOnAccent ?? this.cfOnAccent,
    );
  }
}

class CFOnAccentColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const CFOnAccentColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnAccentColorSchemeNode copyWith({
    required Color? color,
  }) {
    return CFOnAccentColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class CFOnPrimaryColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const CFOnPrimaryColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnPrimaryColorSchemeNode copyWith({
    required Color? color,
  }) {
    return CFOnPrimaryColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class CFOnSuccessColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const CFOnSuccessColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnSuccessColorSchemeNode copyWith({
    required Color? color,
  }) {
    return CFOnSuccessColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class CFOnWarningColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const CFOnWarningColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  CFOnWarningColorSchemeNode copyWith({
    required Color? color,
  }) {
    return CFOnWarningColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class CFPrimaryColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;

  final CFOnPrimaryColorSchemeNode cfOnPrimary;

  const CFPrimaryColorSchemeNode({
    required this.color,
    required this.cfOnPrimary,
  });

  @override
  List<Object?> get props {
    return [
      color,
      cfOnPrimary,
    ];
  }

  CFPrimaryColorSchemeNode copyWith({
    required Color? color,
    required CFOnPrimaryColorSchemeNode? cfOnPrimary,
  }) {
    return CFPrimaryColorSchemeNode(
      color: color ?? this.color,
      cfOnPrimary: cfOnPrimary ?? this.cfOnPrimary,
    );
  }
}

class CFSuccessColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;

  final CFOnSuccessColorSchemeNode cfOnSuccess;

  const CFSuccessColorSchemeNode(
      {required this.color, required this.cfOnSuccess});

  @override
  List<Object?> get props {
    return [
      color,
      cfOnSuccess,
    ];
  }

  CFSuccessColorSchemeNode copyWith({
    required Color? color,
    required CFOnSuccessColorSchemeNode? cfOnSuccess,
  }) {
    return CFSuccessColorSchemeNode(
      color: color ?? this.color,
      cfOnSuccess: cfOnSuccess ?? this.cfOnSuccess,
    );
  }
}

class CFWarningColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;
  final CFOnWarningColorSchemeNode cfOnWarning;

  const CFWarningColorSchemeNode({
    required this.color,
    required this.cfOnWarning,
  });

  @override
  List<Object?> get props {
    return [
      color,
      cfOnWarning,
    ];
  }

  CFWarningColorSchemeNode copyWith({
    required Color? color,
    required CFOnWarningColorSchemeNode? cfOnWarning,
  }) {
    return CFWarningColorSchemeNode(
      color: color ?? this.color,
      cfOnWarning: cfOnWarning ?? this.cfOnWarning,
    );
  }
}

class NPrimaryColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;

  const NPrimaryColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NPrimaryColorSchemeNode copyWith({
    required Color? color,
  }) {
    return NPrimaryColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class NSecondary100ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const NSecondary100ColorSchemeNode({required this.color});

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary100ColorSchemeNode copyWith({required Color? color}) {
    return NSecondary100ColorSchemeNode(color: color ?? this.color);
  }
}

class NSecondary200ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const NSecondary200ColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary200ColorSchemeNode copyWith({
    required Color? color,
  }) {
    return NSecondary200ColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class NSecondary300ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const NSecondary300ColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary300ColorSchemeNode copyWith({
    required Color? color,
  }) {
    return NSecondary300ColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class NSecondary400ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const NSecondary400ColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary400ColorSchemeNode copyWith({
    required Color? color,
  }) {
    return NSecondary400ColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class NSecondary500ColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  const NSecondary500ColorSchemeNode({
    required this.color,
  });

  @override
  List<Object?> get props {
    return [
      color,
    ];
  }

  NSecondary500ColorSchemeNode copyWith({
    required Color? color,
  }) {
    return NSecondary500ColorSchemeNode(
      color: color ?? this.color,
    );
  }
}

class OtherColorSchemeNode extends Equatable implements AppColorSchemeNode {
  @override
  final Color color;

  final Map<int, Color?> colors;

  const OtherColorSchemeNode({required this.color, required this.colors});

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }

  OtherColorSchemeNode copyWith({
    required Color? color,
    required Map<int, Color?>? colors,
  }) {
    return OtherColorSchemeNode(
      color: color ?? this.color,
      colors: colors ?? this.colors,
    );
  }

  Color getColor(int value) => colors[value] ?? color;
}

class OtherTransparentColorSchemeNode extends Equatable
    implements AppColorSchemeNode {
  @override
  final Color color;

  final Map<int, Color?> colors;

  const OtherTransparentColorSchemeNode(
      {required this.color, required this.colors});

  @override
  List<Object?> get props {
    return [
      color,
      colors,
    ];
  }

  OtherTransparentColorSchemeNode copyWith({
    required Color? color,
    required Map<int, Color?>? colors,
  }) {
    return OtherTransparentColorSchemeNode(
      color: color ?? this.color,
      colors: colors ?? this.colors,
    );
  }

  Color getColor(int value) => colors[value] ?? color;
}
