import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'app_text_style.dart';

FontStyle getFontStyle(String? value) {
  switch (value) {
    case 'italic':
      return FontStyle.italic;

    case 'normal':
    default:
      return FontStyle.normal;
  }
}

FontWeight getFontWeight(String? value) {
  int? weight = int.tryParse(value ?? '');

  switch (weight) {
    case 100:
      return FontWeight.w100;
    case 200:
      return FontWeight.w200;
    case 300:
      return FontWeight.w300;
    case 400:
      return FontWeight.w400;
    case 500:
      return FontWeight.w500;
    case 600:
      return FontWeight.w600;
    case 700:
      return FontWeight.w700;
    case 800:
      return FontWeight.w800;
    case 900:
      return FontWeight.w900;
    default:
      return FontWeight.w400;
  }
}

double? parseDouble(String? value) {
  return double.tryParse(value ?? '');
}

class AppTextStyleDto extends Equatable {
  final TextStyleDto heading1;
  final TextStyleDto heading2;
  final TextStyleDto heading3;
  final TextStyleDto heading4;
  final TextStyleDto largeBold;
  final TextStyleDto largeSemibold;
  final TextStyleDto largeRegular;
  final TextStyleDto baseBold;
  final TextStyleDto baseSemibold;
  final TextStyleDto baseRegular;
  final TextStyleDto smallBold;
  final TextStyleDto smallSemibold;
  final TextStyleDto smallRegular;
  final TextStyleDto smallCaption;
  final TextStyleDto tapBar;
  final TextStyleDto button;
  final TextStyleDto date;
  final TextStyleDto mono;

  const AppTextStyleDto(
      {required this.heading1,
      required this.heading2,
      required this.heading3,
      required this.heading4,
      required this.largeBold,
      required this.largeSemibold,
      required this.largeRegular,
      required this.baseBold,
      required this.baseSemibold,
      required this.baseRegular,
      required this.smallBold,
      required this.smallSemibold,
      required this.smallRegular,
      required this.smallCaption,
      required this.tapBar,
      required this.button,
      required this.date,
      required this.mono});

  factory AppTextStyleDto.fromJson(Map<String, dynamic> json) =>
      AppTextStyleDto(
        heading1: TextStyleDto.fromJson(json['heading1']),
        heading2: TextStyleDto.fromJson(json['heading2']),
        heading3: TextStyleDto.fromJson(json['heading3']),
        heading4: TextStyleDto.fromJson(json['heading4']),
        largeBold: TextStyleDto.fromJson(json['largeBold']),
        largeSemibold: TextStyleDto.fromJson(json['largeSemibold']),
        largeRegular: TextStyleDto.fromJson(json['largeRegular']),
        baseBold: TextStyleDto.fromJson(json['baseBold']),
        baseSemibold: TextStyleDto.fromJson(json['baseSemibold']),
        baseRegular: TextStyleDto.fromJson(json['baseRegular']),
        smallBold: TextStyleDto.fromJson(json['smallBold']),
        smallSemibold: TextStyleDto.fromJson(json['smallSemibold']),
        smallRegular: TextStyleDto.fromJson(json['smallRegular']),
        smallCaption: TextStyleDto.fromJson(json['smallCaption']),
        tapBar: TextStyleDto.fromJson(json['tapBar']),
        button: TextStyleDto.fromJson(json['button']),
        date: TextStyleDto.fromJson(json['date']),
        mono: TextStyleDto.fromJson(json['mono']),
      );

  @override
  List<Object?> get props => [heading1];

  AppTextStyle toAppTextStyle() {
    return AppTextStyle(
        heading1: heading1.toEntity(),
        heading2: heading2.toEntity(),
        heading3: heading3.toEntity(),
        heading4: heading4.toEntity(),
        largeBold: largeBold.toEntity(),
        largeSemibold: largeSemibold.toEntity(),
        largeRegular: largeRegular.toEntity(),
        baseBold: baseBold.toEntity(),
        baseSemibold: baseSemibold.toEntity(),
        baseRegular: baseRegular.toEntity(),
        smallBold: smallBold.toEntity(),
        smallSemibold: smallSemibold.toEntity(),
        smallRegular: smallRegular.toEntity(),
        smallCaption: smallCaption.toEntity(),
        tapBar: tapBar.toEntity(),
        button: button.toEntity(),
        date: date.toEntity(),
        mono: mono.toEntity());
  }
}

class TextStyleDto extends Equatable {
  final String? fontFamily;
  final String? fontSize;
  final String? fontWeight;
  final String? fontStyle;
  final String? letterSpacing;

  const TextStyleDto(
      {required this.fontFamily,
      required this.fontSize,
      required this.fontWeight,
      required this.fontStyle,
      required this.letterSpacing});

  factory TextStyleDto.fromJson(Map<String, dynamic> json) => TextStyleDto(
      fontFamily: json['fontFamily'],
      fontSize: json['fontSize'],
      fontWeight: json['fontWeight'],
      fontStyle: json['fontStyle'],
      letterSpacing: json['letterSpacing']);

  @override
  List<Object?> get props =>
      [fontFamily, fontSize, fontWeight, fontStyle, letterSpacing];

  TextStyle toEntity() => TextStyle(
      fontFamily: fontFamily,
      fontSize: parseDouble(fontSize),
      fontWeight: getFontWeight(fontWeight),
      fontStyle: getFontStyle(fontStyle),
      letterSpacing: parseDouble(letterSpacing));
}
