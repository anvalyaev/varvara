import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class AppTextStyleOverride extends Equatable {
  final TextStyleOverride heading1;
  final TextStyleOverride heading2;
  final TextStyleOverride heading3;
  final TextStyleOverride heading4;
  final TextStyleOverride largeBold;
  final TextStyleOverride largeSemibold;
  final TextStyleOverride largeRegular;
  final TextStyleOverride baseBold;
  final TextStyleOverride baseSemibold;
  final TextStyleOverride baseRegular;
  final TextStyleOverride smallBold;
  final TextStyleOverride smallSemibold;
  final TextStyleOverride smallRegular;
  final TextStyleOverride smallCaption;
  final TextStyleOverride tapBar;
  final TextStyleOverride button;
  final TextStyleOverride date;

  const AppTextStyleOverride({
    required this.heading1,
    required this.heading2,
    required this.heading3,
    required this.heading4,
    required this.largeBold,
    required this.largeSemibold,
    required this.largeRegular,
    required this.baseBold,
    required this.baseSemibold,
    required this.baseRegular,
    required this.smallBold,
    required this.smallSemibold,
    required this.smallRegular,
    required this.smallCaption,
    required this.tapBar,
    required this.button,
    required this.date,
  });

  @override
  List<Object?> get props => [
        heading1,
        heading2,
        heading3,
        heading4,
        largeBold,
        largeSemibold,
        largeRegular,
        baseBold,
        baseSemibold,
        baseRegular,
        smallBold,
        smallSemibold,
        smallRegular,
        smallCaption,
        tapBar,
        button,
        date,
      ];
}

class TextStyleOverride extends Equatable {
  final String? fontFamily;
  final double? fontSize;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final double? letterSpacing;

  const TextStyleOverride(
      {required this.fontFamily,
      required this.fontSize,
      required this.fontWeight,
      required this.fontStyle,
      required this.letterSpacing});

  @override
  List<Object?> get props =>
      [fontFamily, fontSize, fontWeight, fontStyle, letterSpacing];
}
