import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'app_color_scheme.dart';
import 'app_text_style.dart';
import 'app_themes_holder.dart';

class AnimatedAppTheme extends ImplicitlyAnimatedWidget {
  final AppThemeData data;
  final Widget child;

  const AnimatedAppTheme({
    required this.data,
    required this.child,
    Curve curve = Curves.easeInOut,
    Duration duration = kThemeAnimationDuration,
    Key? key,
  }) : super(
          key: key,
          curve: curve,
          duration: duration,
        );

  @override
  _AnimatedAppThemeDataState createState() => _AnimatedAppThemeDataState();

  static Widget wrapViewWithEffectiveTheme({
    required Key? viewKey,
    required BuildContext context,
    required Widget child,
  }) {
    String? viewId;
    if (viewKey != null && viewKey is ValueKey<String>) {
      viewId = viewKey.value;
    }

    final appThemesHolder = AppThemesHolder.of(context);
    final brightness = Theme.of(context).brightness;

    final appThemesHolderData =
        appThemesHolder?.data ?? AppThemesHolderData.empty();

    final effectiveAppTheme = appThemesHolderData.effectiveThemeForView(
      viewId: viewId,
      brightness: brightness,
    );

    return AnimatedAppTheme(
      data: effectiveAppTheme,
      child: child,
    );
  }
}

class AppTheme extends StatelessWidget {
  final Widget child;
  final AppThemeData data;

  const AppTheme({
    required this.child,
    required this.data,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _InheritedStateContainer(
      data: this,
      child: child,
    );
  }

  static AppThemeData of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<_InheritedStateContainer>()!
        .data
        .data;
  }
}

class AppThemeData extends Equatable {
  final AppColorScheme colorScheme;
  final AppTextStyle textStyle;

  const AppThemeData({
    required this.colorScheme,
    required this.textStyle,
  });

  factory AppThemeData.empty() {
    return AppThemeData(
      colorScheme: AppColorScheme.empty(),
      textStyle: AppTextStyle.empty(),
    );
  }

  @override
  List<Object?> get props {
    return [colorScheme, textStyle];
  }

  AppThemeData copyWith({
    AppColorScheme? colorScheme,
    AppTextStyle? textStyle,
  }) {
    return AppThemeData(
      colorScheme: colorScheme ?? this.colorScheme,
      textStyle: textStyle ?? this.textStyle,
    );
  }

  static AppThemeData lerp(AppThemeData a, AppThemeData b, double t) {
    // todo: implement
    if (t < 0.5) {
      return a;
    }
    return b;
  }
}

class AppThemeDataTween extends Tween<AppThemeData> {
  AppThemeDataTween({
    AppThemeData? begin,
    AppThemeData? end,
  }) : super(
          begin: begin,
          end: end,
        );

  @override
  AppThemeData lerp(double t) => AppThemeData.lerp(begin!, end!, t);
}

class _AnimatedAppThemeDataState
    extends AnimatedWidgetBaseState<AnimatedAppTheme> {
  AppThemeDataTween? _data;

  @override
  Widget build(BuildContext context) {
    return AppTheme(
      data: _data!.evaluate(animation),
      child: widget.child,
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder description) {
    super.debugFillProperties(description);
    description.add(DiagnosticsProperty<AppThemeDataTween>('data', _data,
        showName: false, defaultValue: null));
  }

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _data = visitor(
      _data,
      widget.data,
      (dynamic value) {
        return AppThemeDataTween(begin: value as AppThemeData);
      },
    )! as AppThemeDataTween;
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final AppTheme data;

  const _InheritedStateContainer({
    required this.data,
    required Widget child,
    Key? key,
  }) : super(
          key: key,
          child: child,
        );

  @override
  bool updateShouldNotify(_InheritedStateContainer oldWidget) {
    return data != oldWidget.data;
  }
}
