import 'package:equatable/equatable.dart';

import 'app_color_schemes_holder_data_dto.dart';
import 'app_text_styles_holder_data_dto.dart';
import 'app_themes_holder.dart';

class AppThemesHolderDataDto extends Equatable {
  final AppColorSchemesHolderDataDto colorSchemesHolderDataDto;
  final AppTextStylesHolderDataDto textStylesHolderDataDto;

  const AppThemesHolderDataDto({
    required this.colorSchemesHolderDataDto,
    required this.textStylesHolderDataDto,
  });

  factory AppThemesHolderDataDto.fromJson(Map<String, dynamic> json) {
    return AppThemesHolderDataDto(
      colorSchemesHolderDataDto:
          AppColorSchemesHolderDataDto.fromJson(json['colorScheme']),
      textStylesHolderDataDto:
          AppTextStylesHolderDataDto.fromJson(json['textStyles']),
    );
  }

  @override
  List<Object?> get props {
    return [
      colorSchemesHolderDataDto,
      textStylesHolderDataDto,
    ];
  }

  AppThemesHolderData toAppThemesHolderData() {
    return AppThemesHolderData(
        colorSchemesHolderData:
            colorSchemesHolderDataDto.toAppColorSchemesHolderData(),
        textStylesHolderData:
            textStylesHolderDataDto.toAppTextStylesHolderData());
  }
}
