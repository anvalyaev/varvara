import 'package:equatable/equatable.dart';
import 'app_text_style.dart';
import 'app_text_style_override.dart';

class AppTextStylesHolderData extends Equatable {
  final AppTextStyle appTextStyle;

  final Map<String, AppTextStyleOverride> appTextStyleOverrides;

  const AppTextStylesHolderData(
      {required this.appTextStyle,
      Map<String, AppTextStyleOverride>? appTextStyleOverrides})
      : this.appTextStyleOverrides = appTextStyleOverrides ?? const {};

  factory AppTextStylesHolderData.empty() {
    return AppTextStylesHolderData(appTextStyle: AppTextStyle.empty());
  }

  @override
  List<Object?> get props {
    return [appTextStyle, appTextStyleOverrides];
  }
}
