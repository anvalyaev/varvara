import 'package:equatable/equatable.dart';

import 'app_text_style_dto.dart';
import 'app_text_style_override.dart';

class AppTextStyleOverrideDto extends Equatable {
  final TextStyleOverrideDto heading1;
  final TextStyleOverrideDto heading2;
  final TextStyleOverrideDto heading3;
  final TextStyleOverrideDto heading4;
  final TextStyleOverrideDto largeBold;
  final TextStyleOverrideDto largeSemibold;
  final TextStyleOverrideDto largeRegular;
  final TextStyleOverrideDto baseBold;
  final TextStyleOverrideDto baseSemibold;
  final TextStyleOverrideDto baseRegular;
  final TextStyleOverrideDto smallBold;
  final TextStyleOverrideDto smallSemibold;
  final TextStyleOverrideDto smallRegular;
  final TextStyleOverrideDto smallCaption;
  final TextStyleOverrideDto tapBar;
  final TextStyleOverrideDto button;
  final TextStyleOverrideDto date;

  const AppTextStyleOverrideDto(
      {required this.heading1,
      required this.heading2,
      required this.heading3,
      required this.heading4,
      required this.largeBold,
      required this.largeSemibold,
      required this.largeRegular,
      required this.baseBold,
      required this.baseSemibold,
      required this.baseRegular,
      required this.smallBold,
      required this.smallSemibold,
      required this.smallRegular,
      required this.smallCaption,
      required this.tapBar,
      required this.button,
      required this.date});

  factory AppTextStyleOverrideDto.fromJson(Map<String, dynamic> json) {
    return AppTextStyleOverrideDto(
      heading1: TextStyleOverrideDto.fromJson(json['heading1']),
      heading2: TextStyleOverrideDto.fromJson(json['heading2']),
      heading3: TextStyleOverrideDto.fromJson(json['heading3']),
      heading4: TextStyleOverrideDto.fromJson(json['heading4']),
      largeBold: TextStyleOverrideDto.fromJson(json['largeBold']),
      largeSemibold: TextStyleOverrideDto.fromJson(json['largeSemibold']),
      largeRegular: TextStyleOverrideDto.fromJson(json['largeRegular']),
      baseBold: TextStyleOverrideDto.fromJson(json['baseBold']),
      baseSemibold: TextStyleOverrideDto.fromJson(json['baseSemibold']),
      baseRegular: TextStyleOverrideDto.fromJson(json['baseRegular']),
      smallBold: TextStyleOverrideDto.fromJson(json['smallBold']),
      smallSemibold: TextStyleOverrideDto.fromJson(json['smallSemibold']),
      smallRegular: TextStyleOverrideDto.fromJson(json['smallRegular']),
      smallCaption: TextStyleOverrideDto.fromJson(json['smallRegular']),
      tapBar: TextStyleOverrideDto.fromJson(json['tapBar']),
      button: TextStyleOverrideDto.fromJson(json['button']),
      date: TextStyleOverrideDto.fromJson(json['date']),
    );
  }

  @override
  List<Object?> get props {
    return [
      heading1,
      heading2,
      heading3,
      heading4,
      largeBold,
      largeSemibold,
      largeRegular,
      baseBold,
      baseSemibold,
      baseRegular,
      smallBold,
      smallSemibold,
      smallRegular,
      smallCaption,
      tapBar,
      button,
      date,
    ];
  }

  AppTextStyleOverride toAppTextStyleOverride() {
    return AppTextStyleOverride(
      heading1: heading1.toEntity(),
      heading2: heading2.toEntity(),
      heading3: heading3.toEntity(),
      heading4: heading4.toEntity(),
      largeBold: largeBold.toEntity(),
      largeSemibold: largeSemibold.toEntity(),
      largeRegular: largeRegular.toEntity(),
      baseBold: baseBold.toEntity(),
      baseSemibold: baseSemibold.toEntity(),
      baseRegular: baseRegular.toEntity(),
      smallBold: smallBold.toEntity(),
      smallSemibold: smallSemibold.toEntity(),
      smallRegular: smallRegular.toEntity(),
      smallCaption: smallCaption.toEntity(),
      tapBar: tapBar.toEntity(),
      button: button.toEntity(),
      date: date.toEntity(),
    );
  }
}

class TextStyleOverrideDto extends Equatable {
  final String? fontFamily;
  final String? fontSize;
  final String? fontWeight;
  final String? fontStyle;
  final String? letterSpacing;

  const TextStyleOverrideDto(
      {required this.fontFamily,
      required this.fontSize,
      required this.fontWeight,
      required this.fontStyle,
      required this.letterSpacing});

  factory TextStyleOverrideDto.fromJson(Map<String, dynamic> json) =>
      TextStyleOverrideDto(
          fontFamily: json['fontFamily'],
          fontSize: json['fontSize'],
          fontWeight: json['fontWeight'],
          fontStyle: json['fontStyle'],
          letterSpacing: json['letterSpacing']);

  @override
  List<Object?> get props =>
      [fontFamily, fontSize, fontWeight, fontStyle, letterSpacing];

  TextStyleOverride toEntity() => TextStyleOverride(
      fontFamily: fontFamily,
      fontSize: parseDouble(fontSize),
      fontWeight: getFontWeight(fontWeight),
      fontStyle: getFontStyle(fontStyle),
      letterSpacing: parseDouble(letterSpacing));
}
