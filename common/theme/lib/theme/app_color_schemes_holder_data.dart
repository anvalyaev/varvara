import 'package:equatable/equatable.dart';

import 'app_color_scheme.dart';
import 'app_color_scheme_override.dart';

class AppColorSchemesHolderData extends Equatable {
  final AppColorScheme baseLightColorScheme;
  final AppColorScheme baseDarkColorScheme;

  final Map<String, AppColorSchemeOverride> lightColorSchemeOverrides;
  final Map<String, AppColorSchemeOverride> darkColorSchemeOverrides;

  const AppColorSchemesHolderData({
    required this.baseLightColorScheme,
    required this.baseDarkColorScheme,
    Map<String, AppColorSchemeOverride>? lightColorSchemeOverrides,
    Map<String, AppColorSchemeOverride>? darkColorSchemeOverrides,
  })  : lightColorSchemeOverrides = lightColorSchemeOverrides ?? const {},
        darkColorSchemeOverrides = darkColorSchemeOverrides ?? const {};

  factory AppColorSchemesHolderData.empty() {
    return AppColorSchemesHolderData(
      baseLightColorScheme: AppColorScheme.empty(),
      baseDarkColorScheme: AppColorScheme.empty(),
    );
  }

  @override
  List<Object?> get props {
    return [
      baseLightColorScheme,
      baseDarkColorScheme,
      lightColorSchemeOverrides,
      darkColorSchemeOverrides,
    ];
  }
}
