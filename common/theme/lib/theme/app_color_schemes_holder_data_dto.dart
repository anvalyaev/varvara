import 'package:equatable/equatable.dart';

import 'app_color_scheme_dto.dart';
import 'app_color_scheme_override_dto.dart';
import 'app_color_schemes_holder_data.dart';

class AppColorSchemesHolderDataDto extends Equatable {
  final AppColorSchemeDto baseLightColorSchemeDto;
  final AppColorSchemeDto baseDarkColorSchemeDto;
  final Map<String, AppColorSchemeOverrideDto> lightColorSchemeOverrideDtos;
  final Map<String, AppColorSchemeOverrideDto> darkColorSchemeOverrideDtos;

  const AppColorSchemesHolderDataDto({
    required this.baseLightColorSchemeDto,
    required this.baseDarkColorSchemeDto,
    this.lightColorSchemeOverrideDtos = const {},
    this.darkColorSchemeOverrideDtos = const {},
  });

  factory AppColorSchemesHolderDataDto.fromJson(Map<String, dynamic> json) {
    final baseLightColorSchemeDto = AppColorSchemeDto.fromJson(json['light']);
    final baseDarkColorSchemeDto = AppColorSchemeDto.fromJson(json['dark']);

    Map<String, Map> lightColorSchemeOverrideDtosMap = Map.castFrom(json['lightOverrides'] ?? {});
    final lightColorSchemeOverrideDtos = lightColorSchemeOverrideDtosMap.map((key, value) {
      final Map<String, dynamic> colorSchemeOverrideDtoJson = Map.castFrom(value);
      final colorSchemeOverrideDto = AppColorSchemeOverrideDto.fromJson(colorSchemeOverrideDtoJson);
      return MapEntry(key, colorSchemeOverrideDto);
    });

    Map<String, Map> darkColorSchemeOverrideDtosMap = Map.castFrom(json['darkOverrides'] ?? {});
    final darkColorSchemeOverrideDtos = darkColorSchemeOverrideDtosMap.map((key, value) {
      final Map<String, dynamic> colorSchemeOverrideDtoJson = Map.castFrom(value);
      final colorSchemeOverrideDto = AppColorSchemeOverrideDto.fromJson(colorSchemeOverrideDtoJson);
      return MapEntry(key, colorSchemeOverrideDto);
    });

    return AppColorSchemesHolderDataDto(
      baseLightColorSchemeDto: baseLightColorSchemeDto,
      baseDarkColorSchemeDto: baseDarkColorSchemeDto,
      lightColorSchemeOverrideDtos: lightColorSchemeOverrideDtos,
      darkColorSchemeOverrideDtos: darkColorSchemeOverrideDtos,
    );
  }

  @override
  List<Object?> get props {
    return [
      baseLightColorSchemeDto,
      baseDarkColorSchemeDto,
      lightColorSchemeOverrideDtos,
      darkColorSchemeOverrideDtos,
    ];
  }

  AppColorSchemesHolderData toAppColorSchemesHolderData() {
    final lightColorSchemeOverrides = lightColorSchemeOverrideDtos.map((key, value) {
      return MapEntry(key, value.toAppColorSchemeOverride());
    });

    final darkColorSchemeOverrides = darkColorSchemeOverrideDtos.map((key, value) {
      return MapEntry(key, value.toAppColorSchemeOverride());
    });

    return AppColorSchemesHolderData(
      baseLightColorScheme: baseLightColorSchemeDto.toAppColorScheme(),
      baseDarkColorScheme: baseDarkColorSchemeDto.toAppColorScheme(),
      lightColorSchemeOverrides: lightColorSchemeOverrides,
      darkColorSchemeOverrides: darkColorSchemeOverrides,
    );
  }
}
