import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:storage/storage.dart';
import 'rest_request.dart';

typedef GenerateStoragePathCallback = StoragePath Function(
    String mimeType, int size, int statusCode);

typedef ProgressCallback = void Function(Progress progress);

class BinaryDataInfo {
  final int size;
  final String mimeType;
  final StoragePath storagePath;
  final Progress progress;
  BinaryDataInfo(
      {required this.size,
      required this.mimeType,
      required this.storagePath,
      required this.progress});

  BinaryDataInfo copyWith({
    int? size,
    String? mimeType,
    StoragePath? storagePath,
    Progress? progress,
  }) {
    return BinaryDataInfo(
      size: size ?? this.size,
      mimeType: mimeType ?? this.mimeType,
      storagePath: storagePath ?? this.storagePath,
      progress: progress ?? this.progress,
    );
  }
}

abstract class BinaryDataNetworkRequest extends NetworkRequest<BinaryDataInfo> {
  final RestoreInfo? restoreInfo;
  final ProgressCallback? onProgressChanged;
  final GenerateStoragePathCallback generateStoragePath;
  BinaryDataInfo? _binaryDataInfo;
  BinaryDataNetworkRequest(
      {this.onProgressChanged,
      this.restoreInfo,
      required this.generateStoragePath});

  BinaryDataInfo get binaryDataInfo => _binaryDataInfo!;

  @override
  Map<String, String> get headers {
    if (restoreInfo != null) {
      int from = restoreInfo!.progress.receivedSize;
      int to = restoreInfo!.progress.size;
      return {"Range": "bytes=$from-$to"};
    }
    return {};
  }

  @override
  @nonVirtual
  BinaryDataInfo onAnswer(Response answer) {
    // Это заглушка, не используется в этом типе запросов
    throw UnimplementedError();
  }

  @nonVirtual
  void onReceivedAnswer(Response<dynamic> response) {
    var mimeType = response.headers['content-type']?.split(';').first ?? '';
    int size = int.parse(response.headers['content-length'] ?? '0');
    final StoragePath storagePath = restoreInfo == null
        ? generateStoragePath(mimeType, size, response.statusCode)
        : restoreInfo!.storagePath;
    if (restoreInfo == null) {
      var storage = Storage(storagePath);
      storage.deleteSync();
    }
    final progress = Progress(size: size, receivedSize: 0);

    _binaryDataInfo = BinaryDataInfo(
        size: size,
        mimeType: mimeType,
        storagePath: storagePath,
        progress: progress);
    if (onProgressChanged != null) {
      onProgressChanged!(_binaryDataInfo!.progress);
    }
  }

  @nonVirtual
  void onReceivedPartOfData(List<int> partData) {
    assert(_binaryDataInfo != null);
    var storage = Storage(_binaryDataInfo!.storagePath);
    storage.wrireSync(Uint8List.fromList(partData), append: true);
    final oldProgress = _binaryDataInfo!.progress;
    _binaryDataInfo = _binaryDataInfo!.copyWith(
        progress: oldProgress.copyWith(
            receivedSize: oldProgress.receivedSize + partData.length));
    if (onProgressChanged != null) {
      onProgressChanged!(_binaryDataInfo!.progress);
    }
  }
}

class Progress {
  final int size;
  final int receivedSize;
  Progress({
    required this.size,
    required this.receivedSize,
  });
  bool get isDone => size == receivedSize;
  double get progress => receivedSize / size;

  Progress copyWith({
    int? size,
    int? receivedSize,
  }) {
    return Progress(
      size: size ?? this.size,
      receivedSize: receivedSize ?? this.receivedSize,
    );
  }
}

class RestoreInfo {
  final StoragePath storagePath;
  final Progress progress;
  RestoreInfo({
    required this.storagePath,
    required this.progress,
  });
}
