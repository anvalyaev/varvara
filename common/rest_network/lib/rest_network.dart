library network;

import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:core/core.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:http/io_client.dart';
import 'package:quiver/collection.dart';
import 'package:rest_network/binary_data_network_request.dart';

import 'rest_request.dart';

export 'rest_request.dart';

enum RestNetworkStatus { authorized, refreshing, unauthorized }

abstract class IRestNetwork {
  void updateBaseUrl({required String baseUrl});
  void updateToken({String? token});
  Future<R> sendRequest<R>(NetworkRequest request);
}

class RestNetwork implements IRestNetwork {
  static const int sendTimeout = 10000;
  static const int receiveTimeout = 25000;
  bool _initialized = false;
  late final http.Client client;
  String? _token;
  String? _baseUrl;
  RestNetworkStatus _status = RestNetworkStatus.unauthorized;

  void Function() onNeedUpdateToken;

  @override
  void updateBaseUrl({required String baseUrl}) {
    _baseUrl = baseUrl;
  }

  @override
  void updateToken({String? token}) {
    if (token == null) {
      _status = RestNetworkStatus.unauthorized;
    } else {
      _status = RestNetworkStatus.authorized;
    }
    _token = token;
  }

  RestNetwork({required this.onNeedUpdateToken, required String baseUrl})
      : _baseUrl = baseUrl,
        super();

  Future<void> _init() async {
    if (_initialized) return;
    if (Platform.isAndroid) {
      final androidInfo = await DeviceInfoPlugin().androidInfo;
      if (androidInfo.version.sdkInt <= 24) {
        client = IOClient(_setCustomSertificate(cert: _ISRG_X1));
      } else {
        client = http.Client();
      }
    } else {
      client = http.Client();
    }
    _initialized = true;
  }

  HttpClient _setCustomSertificate({required String cert}) {
    SecurityContext context = SecurityContext.defaultContext;
    try {
      List<int> bytes = utf8.encode(cert);
      context.setTrustedCertificatesBytes(bytes);
    } on TlsException catch (e) {
      if (e.osError?.message != null &&
          e.osError!.message.contains('CERT_ALREADY_IN_HASH_TABLE')) {
      } else {
        rethrow;
      }
    } finally {}
    return HttpClient(context: context);
  }

  String _multimapToQueryString(Multimap<String, String> multimap) {
    String query = '';
    for (var queryKey in multimap.keys) {
      var queryValues = multimap[queryKey];
      for (var queryValue in queryValues) {
        if (query.isNotEmpty) {
          query += '&';
        }
        query += '$queryKey=$queryValue';
      }
    }
    return query;
  }

  @override
  Future<R> sendRequest<R>(NetworkRequest request) async {
    await _init();
    if (_status == RestNetworkStatus.unauthorized && request.authorized) {
      throw NetworkUnauthorizedError();
    }
    if (_status == RestNetworkStatus.refreshing) {
      await Future.delayed(const Duration(seconds: 1));
      return sendRequest(request);
    }
    String baseUrl = request.baseUrl ?? _baseUrl!;
    String path = request.path;
    Uri url = Uri.parse('$baseUrl$path');
    if (request.queryParameters.isNotEmpty) {
      String query = _multimapToQueryString(request.queryParameters);
      url = url.replace(query: query);
    }
    Map<String, String> headers = request.headers;
    if (request.authorized) {
      headers['Authorization'] = "Bearer $_token";
    }
    late http.Response httpResponse;
    late Response myResponse;
    logger.i(
        'REQUEST\ntype: ${request.typeRequest}\nhost: $baseUrl\nmethod:$path\nquery:${url.query}\nheaders $headers\nbody: ${request.data}');
    var httpRequest =
        http.Request(_methodByTypeRequest(request.typeRequest), url);
    httpRequest.headers.addAll(headers);
    var body = request.data;
    if (request.jsonEncode) {
      body = jsonEncode(body);
    }
    if (body is String) {
      httpRequest.body = body;
    } else if (body is List) {
      httpRequest.bodyBytes = body.cast<int>();
    } else if (body is Map) {
      httpRequest.bodyFields = body.cast<String, String>();
    } else {
      throw ArgumentError('Invalid request body "${request.data}".');
    }
    http.StreamedResponse streamedResponse;
    try {
      streamedResponse = await (client
              .send(httpRequest)
              .timeout(const Duration(milliseconds: sendTimeout)))
          .timeout(const Duration(milliseconds: sendTimeout))
          .onError((error, stackTrace) {
        if (error is TimeoutException) {
          throw NetworkTimeoutError();
        } else {
          throw NetworkUnknownError(error: error ?? 'unknown');
        }
      });
      if (request is BinaryDataNetworkRequest) {
        httpResponse = http.Response('', streamedResponse.statusCode,
            request: streamedResponse.request,
            headers: streamedResponse.headers,
            isRedirect: streamedResponse.isRedirect,
            persistentConnection: streamedResponse.persistentConnection,
            reasonPhrase: streamedResponse.reasonPhrase);
      } else {
        httpResponse = await (http.Response.fromStream((streamedResponse))
                .timeout(const Duration(milliseconds: receiveTimeout)))
            .onError((error, stackTrace) {
          if (error is TimeoutException) {
            throw NetworkTimeoutError();
          } else {
            throw NetworkUnknownError(error: error ?? 'unknown');
          }
        });
      }
    } on TimeoutException catch (_) {
      throw NetworkTimeoutError();
    } on SocketException catch (e) {
      throw NetworkUnknownError(error: e);
    } catch (e) {
      rethrow;
    }
    var headersLowerCase = httpResponse.headers
        .map((key, value) => MapEntry(key.toLowerCase(), value));
    if ((headersLowerCase['content-type']?.contains('application/json') ??
            false) &&
        !request.forceBinaryData) {
      if (httpResponse.body.isEmpty) {
        myResponse = Response<Map<String, dynamic>>(
            {}, httpResponse.statusCode, httpResponse.headers);
      } else {
        var jsonMap = jsonDecode(httpResponse.body);
        myResponse =
            Response(jsonMap, httpResponse.statusCode, httpResponse.headers);
      }
    } else if ((headersLowerCase['content-type']?.contains('text/plain') ??
            false) &&
        !request.forceBinaryData) {
      myResponse = Response<String>(
          httpResponse.body, httpResponse.statusCode, httpResponse.headers);
    } else {
      myResponse = Response<Uint8List>(httpResponse.bodyBytes,
          httpResponse.statusCode, httpResponse.headers);
    }
    if (myResponse.statusCode >= 300) {
      if (request.baseUrl == null) {
        if (myResponse.statusCode == 401) {
          if (request.authorized) {
            _status = RestNetworkStatus.refreshing;
            onNeedUpdateToken();
            return sendRequest(request);
          }
        }
      }
      logger.e(
          'ANSWER ERROR\ntype: ${request.typeRequest}\nhost: $baseUrl\nmethod:$url\nquery:${_multimapToQueryString(request.queryParameters)}\nheaders ${httpResponse.headers}\ncode: ${httpResponse.statusCode}\nbody: ${myResponse.data}');
      throw NetworkResponseError(
          myResponse.data, myResponse.statusCode, myResponse.headers);
    }
    logger.i(
        'ANSWER\ntype: ${request.typeRequest}\nhost: $baseUrl\nmethod:$url\nquery:${_multimapToQueryString(request.queryParameters)}\nheaders ${myResponse.headers}\nbody: ${myResponse.data}');
    if (request.authorized) {
      if (_token == null) {
        throw NetworkUnauthorizedError();
      }
    }
    if (request is BinaryDataNetworkRequest) {
      final completer = Completer<R>();
      request.onReceivedAnswer(myResponse);
      streamedResponse.stream.listen((partData) {
        request.onReceivedPartOfData(partData);
      }, onError: (error) {
        completer.completeError(request.binaryDataInfo);
      }, onDone: () {
        completer.complete(request.binaryDataInfo as R);
      }, cancelOnError: true);
      return completer.future;
    } else {
      return request.onAnswer(myResponse);
    }
  }

  String _methodByTypeRequest(TypeRequest typeRequest) {
    switch (typeRequest) {
      case TypeRequest.deleteRequest:
        return 'DELETE';
      case TypeRequest.getRequest:
        return 'GET';
      case TypeRequest.patchRequest:
        return 'PATCH';
      case TypeRequest.postRequest:
        return 'POST';
      case TypeRequest.putRequest:
        return 'PUT';
      default:
        return 'o_O';
    }
  }

  static const String _ISRG_X1 = """-----BEGIN CERTIFICATE-----
MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
-----END CERTIFICATE-----""";
}

class ApplicationSettings {}
