import 'package:quiver/collection.dart';

typedef JsonMap = Map<String, dynamic>;

abstract class NetworkRequest<R> {
  bool get authorized => true;
  bool get forceBinaryData => false;
  String? get baseUrl => null;
  Object get data;
  Map<String, String> get headers => {};
  bool get jsonEncode => true;
  Multimap<String, String> get queryParameters => Multimap<String, String>();
  TypeRequest get typeRequest;
  String get path;
  R onAnswer(Response answer);
}

class Response<T> {
  final T data;
  final int statusCode;
  final Map<String, String> headers;
  Response(this.data, this.statusCode, this.headers);
}

class NetworkError {}

class NetworkResponseError<T> extends NetworkError {
  final T data;
  final int statusCode;
  final Map<String, String> headers;
  NetworkResponseError(this.data, this.statusCode, this.headers);
}

class NetworkUnauthorizedError extends NetworkError {}

class NetworkTimeoutError extends NetworkError {}

class NetworkUnknownError extends NetworkError {
  Object? error;
  NetworkUnknownError({
    required this.error,
  });

  @override
  String toString() => 'UnknownError(error: $error)';
}

enum TypeRequest {
  postRequest,
  getRequest,
  putRequest,
  deleteRequest,
  patchRequest
}
