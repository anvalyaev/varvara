library storage;

import 'dart:io';
import 'dart:typed_data';

import 'package:equatable/equatable.dart';

export './impl/storage_stub.dart'
    if (dart.library.io) './impl/storage_vm.dart'
    if (dart.library.html) './impl/storage_js.dart';
export 'storage_image.dart';
export 'storage_image_provider.dart';
export 'fade_in_storage_image.dart';

abstract class IStorageEntity {}

abstract class IStorageDirectory implements IStorageEntity {
  final StoragePath dirPath;

  IStorageDirectory(this.dirPath);

  bool get hasChildren;

  Iterable<IStorageEntity> get children;

  Iterable<IStorage> get storageChildren;

  int get fullSizeSync;
}

abstract class IStorage implements IStorageEntity {
  final StoragePath storagePath;

  IStorage(this.storagePath);

  DateTime get changed;

  Future<void> copy(String newPath);

  void copySync(String newPath);

  Future<void> delete();

  void deleteSync();

  Future<bool> exists();

  bool existsSync();

  Future<int> length();

  int lengthSync();

  Future<Uint8List> read();

  Uint8List readSync();

  Future<void> wrire(Uint8List data, {bool append = false});

  void wrireSync(Uint8List data, {bool append = false});
}

class StoragePath extends Equatable {
  final String? basePath;
  final String? subPath;
  final String fileName;
  final String? hash;
  const StoragePath(
      {this.subPath, this.basePath, this.fileName = '', this.hash = ''});

  factory StoragePath.byFullPath(String fullPath) {
    var splittedPath = fullPath.split(Platform.pathSeparator);
    if (splittedPath.isEmpty) {
      throw Exception('Wrong file path. (Full path: $fullPath)');
    }
    String fileName = splittedPath.last;
    splittedPath.removeLast();
    String basePath =
        splittedPath.reduce((value, element) => '$value/$element');

    return StoragePath(basePath: basePath, fileName: fileName);
  }
  bool get isHaveBasePath => basePath != null;

  String get path => _preparePath();

  String get dirPath => _preparePath(true);

  @override
  List<Object?> get props => [path, hash];

  StoragePath copyWith(
      {String? basePath, String? subPath, String? hash, String? fileName}) {
    return StoragePath(
        basePath: basePath ?? this.basePath,
        subPath: subPath ?? this.subPath,
        fileName: fileName ?? this.fileName,
        hash: hash ?? this.hash);
  }

  String _preparePath([bool excludeFileName = false]) {
    final fileName = excludeFileName ? '' : this.fileName;
    if (!isHaveBasePath) {
      throw Exception(
          'Base path is not set, try fix this before using this path. (Subpath: $subPath), File Name: $fileName');
    }
    if (subPath != null) {
      return '$basePath${Platform.pathSeparator}$subPath${Platform.pathSeparator}$fileName';
    } else {
      return '$basePath${Platform.pathSeparator}$fileName';
    }
  }
}
