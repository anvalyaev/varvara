import 'dart:io';
import 'dart:typed_data';

import '../storage.dart' as i;

void redirect(Uri uri) {}

class StorageDirectory extends i.IStorageDirectory {
  final Directory _directory;

  StorageDirectory(i.StoragePath path)
      : _directory = Directory(path.dirPath),
        super(path) {
    if (!_directory.existsSync()) {
      try {
        _directory.createSync(recursive: true);
      } catch (e) {
        print('Directory creation failed: $_directory');
      }
    }
  }

  @override
  bool get hasChildren => _directory.listSync().isNotEmpty;

  @override
  Iterable<i.IStorageEntity> get children {
    final entities = _directory.listSync();
    return entities
        .where((entity) =>
    entity is File
        || entity is Directory)
        .map((entity) {
      if (entity is Directory) {
        final path = i.StoragePath(basePath: entity.path);
        return StorageDirectory(path);
      } else {
        final path = i.StoragePath.byFullPath(entity.path);
        return Storage(path);
      }
    });
  }

  @override
  Iterable<i.IStorage> get storageChildren {
    final entities = _directory.listSync();
    return entities
        .where((entity) =>
    entity is File)
        .map((entity) {
      final path = i.StoragePath.byFullPath(entity.path);
      return Storage(path);
    });
  }

  @override
  int get fullSizeSync {
    final entities = _directory.listSync();
    return entities.isEmpty ? 0 : _directory.listSync().map((entity) =>
    entity
        .statSync()
        .size).reduce((value, element) => value + element);
  }
}

class Storage extends i.IStorage {
  final File _file;

  Storage(i.StoragePath path, [createIfNotExist = true])
      : assert(path.fileName.isNotEmpty),
        _file = File(path.path),
        super(path) {
    if (!_file.existsSync() && createIfNotExist) {
      try {
        _file.createSync(recursive: true);
      } catch (e) {
        print('file creation failed: $_file');
      }
    }
  }

  DateTime get changed {
    return _file
        .statSync()
        .changed;
  }

  @override
  Future<void> copy(String newPath) {
    return _file.copy(newPath);
  }

  @override
  void copySync(String newPath) {
    _file.copySync(newPath);
  }

  @override
  Future<bool> exists() {
    return _file.exists();
  }

  @override
  bool existsSync() {
    return _file.existsSync();
  }

  @override
  Future<int> length() {
    return _file.length();
  }

  @override
  int lengthSync() {
    return _file.lengthSync();
  }

  @override
  Future<Uint8List> read() {
    return _file.readAsBytes();
  }

  @override
  Uint8List readSync() {
    return _file.readAsBytesSync();
  }

  @override
  Future<void> wrire(Uint8List data, {bool append = false}) {
    return _file.writeAsBytes(data,
        mode: append ? FileMode.append : FileMode.write);
  }

  @override
  void wrireSync(Uint8List data, {bool append = false}) {
    _file.writeAsBytesSync(data,
        mode: append ? FileMode.append : FileMode.write);
  }

  @override
  Future<void> delete() {
    return _file.delete();
  }

  @override
  void deleteSync() {
    return _file.deleteSync();
  }
}
