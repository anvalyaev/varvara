import 'dart:typed_data';
import '../storage.dart' as i;

void redirect(Uri uri) {}

class StorageDirectory extends i.IStorageDirectory {
  StorageDirectory(i.StoragePath path) : super(path);

  bool get hasChildren => throw UnimplementedError();

  Iterable<i.IStorageEntity> get children => throw UnimplementedError();

  Iterable<i.IStorage> get storageChildren => throw UnimplementedError();

  int get fullSizeSync => throw UnimplementedError();
}

class Storage extends i.IStorage {
  Storage(i.StoragePath path) : super(path);

  @override
  DateTime get changed => throw UnimplementedError();

  @override
  Future<void> copy(String newPath) {
    // TODO: implement copy
    throw UnimplementedError();
  }

  @override
  void copySync(String newPath) {
    // TODO: implement copySync
  }

  @override
  Future<bool> exists() {
    // TODO: implement exists
    throw UnimplementedError();
  }

  @override
  bool existsSync() {
    // TODO: implement existsSync
    throw UnimplementedError();
  }

  @override
  Future<int> length() {
    // TODO: implement length
    throw UnimplementedError();
  }

  @override
  int lengthSync() {
    // TODO: implement lengthSync
    throw UnimplementedError();
  }

  @override
  Future<Uint8List> read() {
    // TODO: implement read
    throw UnimplementedError();
  }

  @override
  Uint8List readSync() {
    // TODO: implement readSync
    throw UnimplementedError();
  }

  @override
  Future<void> delete() {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  void deleteSync() {
    // TODO: implement deleteSync
  }

  @override
  Future<void> wrire(Uint8List data, {bool append = false}) {
    // TODO: implement wrire
    throw UnimplementedError();
  }

  @override
  void wrireSync(Uint8List data, {bool append = false}) {
    // TODO: implement wrireSync
  }
}
