import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'storage.dart';
import 'dart:ui' as ui show Codec;

class StorageImageProvider extends ImageProvider<StorageImageProvider> {
  const StorageImageProvider(this.path, {this.scale = 1.0});

  final StoragePath path;

  /// The scale to place in the [ImageInfo] object of the image.
  final double scale;

  @override
  Future<StorageImageProvider> obtainKey(ImageConfiguration configuration) {
    return SynchronousFuture<StorageImageProvider>(this);
  }

  @override
  ImageStreamCompleter load(StorageImageProvider key, DecoderCallback decode) {
    return MultiFrameImageStreamCompleter(
      codec: _loadAsync(key, decode),
      scale: key.scale,
      debugLabel: key.path.path,
      informationCollector: () sync* {
        yield ErrorDescription('Path: ${path.path}');
      },
    );
  }

  Future<ui.Codec> _loadAsync(
      StorageImageProvider key, DecoderCallback decode) async {
    assert(key == this);

    var storage = Storage(path);

    final Uint8List bytes = await storage.read();

    if (bytes.lengthInBytes == 0) {
      // The file may become available later.
      PaintingBinding.instance.imageCache.evict(key);
      throw StateError(
          '${path.path} is empty and cannot be loaded as an image.');
    }

    return decode(bytes);
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is StorageImageProvider &&
        other.path.path == path.path &&
        other.scale == scale &&
        other.path.hash == path.hash;
  }

  @override
  int get hashCode => hashValues(path.path, scale);

  @override
  String toString() =>
      '${objectRuntimeType(this, 'StorageImageProvider')}("${path.path}", scale: $scale)';
}
