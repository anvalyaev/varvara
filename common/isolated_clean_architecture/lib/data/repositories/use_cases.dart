import 'package:flutter/foundation.dart';
import 'package:isolated_clean_architecture/data/repositories/repository_base.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';

abstract class IUseCaseRepository extends RepositoryBase {
  List<UseCaseBase> getUseCases();
  bool contains(UseCaseId useCaseId);
  void removeUseCase(UseCaseId useCaseId);
  void updateUseCase(UseCaseBase item);
  UseCaseBase? getUseCase(UseCaseId useCaseId);
  UseCaseState getUseCaseState(UseCaseId ucid);
  Map<UseCaseId, UseCaseState> getUseCaseStates({Set<UseCaseId>? useCaseIds});
  void setUseCaseState(UseCaseId ucid, UseCaseState state);

  @nonVirtual
  List<UseCaseBase> orderGroupUseCases(int orderGroupIndex) => (getUseCases())
      .where((element) => element.orderGroupIndex == orderGroupIndex)
      .toList();
}

class UseCaseRepository extends IUseCaseRepository {
  final Map<UseCaseId, UseCaseBase> _items;
  final List<UseCaseId> _order;
  final Map<UseCaseId, UseCaseState> _states;
  UseCaseRepository()
      : _items = {},
        _states = {},
        _order = [];

  @override
  List<UseCaseBase> getUseCases() {
    return _order.map<UseCaseBase>((e) => _items[e]!).toList();
  }

  @override
  UseCaseBase? getUseCase(UseCaseId useCaseId) => _items[useCaseId];

  @override
  Future<void> clear() async {
    _items.clear();
    _order.clear();
    _states.clear;
    notifyListeners();
  }

  @override
  bool contains(UseCaseId useCaseId) {
    return _items.containsKey(useCaseId);
  }

  @override
  void removeUseCase(UseCaseId useCaseId) {
    _items.remove(useCaseId);
    _order.remove(useCaseId);
    _states.remove(useCaseId);
    notifyListeners();
  }

  @override
  void updateUseCase(UseCaseBase item) {
    if (contains(item.id)) {
      _items[item.id] = item;
    } else {
      _items[item.id] = item;
      _order.add(item.id);
    }
    notifyListeners();
  }

  @override
  UseCaseState getUseCaseState(UseCaseId ucid) {
    return _states[ucid] ?? UseCaseState.unknown;
  }

  @override
  void setUseCaseState(UseCaseId ucid, UseCaseState state) {
    _states[ucid] = state;
    notifyListeners();
  }

  @override
  Map<UseCaseId, UseCaseState> getUseCaseStates({Set<UseCaseId>? useCaseIds}) {
    if (useCaseIds == null) {
      return Map<UseCaseId, UseCaseState>.from(_states);
    } else {
      return Map<UseCaseId, UseCaseState>.fromEntries(
          _states.entries.where((element) => useCaseIds.contains(element.key)));
    }
  }
}
