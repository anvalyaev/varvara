import 'package:flutter/foundation.dart';

abstract class RepositoryBase extends ChangeNotifier {
  Future<void> clear();
}
