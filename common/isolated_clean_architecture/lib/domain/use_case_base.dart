import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:isolator_channels/isolator_channels.dart';
import 'package:uuid/uuid.dart';

import '../accessor.dart';

typedef UseCaseOnResultCallback<Out extends UseCaseResult> = void Function(
    Out result);
typedef UseCaseOnErrorCallback = Future<void> Function(UseCaseError error);
typedef UseCaseOnCompleteCallback = void Function(UseCaseId useCaseId);

enum UseCaseState { unknown, processing, complete, error, pending, canceled }

enum ErrorStatus { resolved, unresolved }

class UseCaseId extends Equatable {
  final String _key;

  UseCaseId({String? key}) : _key = key ?? const Uuid().v4();

  @override
  List<Object> get props => [_key];

  @override
  String toString() => 'UseCaseId(_key: $_key)';
}

abstract class UseCaseResult with Message {
  UseCaseId ucid;
  UseCaseResult(this.ucid);

  @override
  String toString() => 'UseCaseResult(ucid: $ucid)';
}

abstract class UseCaseError with Message {
  UseCaseId ucid;
  UseCaseError(this.ucid);

  @override
  String toString() => 'UseCaseError(ucid: $ucid)';
}

class UseCaseComplete with Message {
  UseCaseId ucid;
  UseCaseComplete(this.ucid);

  @override
  String toString() => 'UseCaseComplete(ucid: $ucid)';
}

class UseCaseCancel with Message {
  UseCaseId ucid;
  UseCaseCancel(this.ucid);

  @override
  String toString() => 'UseCaseComplete(ucid: $ucid)';
}

abstract class UseCaseBase<In, Out extends UseCaseResult> with Message {
  final UseCaseId _id;
  final In? dataIn;
  final DateTime createdAt;

  UseCaseBase(
      {UseCaseId? id,
      required this.dataIn,
      UseCaseState? state,
      DateTime? createdAt})
      : _id = id ?? UseCaseId(key: const Uuid().v4()),
        createdAt = createdAt ?? DateTime.now();

  UseCaseId get id => _id;

  ///Действие выполеняемое в рабочем изоляте приложения.
  ///[onResult] вызывается когда формируется результат [Out] выполнения [doUseCase]
  ///и может быть вызван многократно.
  ///[onError] вызывается когда возникает логическая ошибка
  ///при выполнении [doUseCase]
  ///[onComplete] вызывается когда вся логика [doUseCase] завершена
  ///после вызова [onComplete] вызов [onResult] или [onError]
  ///не допускается и приведет к формированию исключения
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<Out> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete);

  ///Вызывается когда выполнение [UseCaseBase] было отменено.
  ///После вызова [cancelUseCase] все последующие вызовы
  ///[onResult], [onError], [onComplete] в [doUseCase] будут проигнорированны
  ///[cancelUseCase] должен реализовать логику высвобождения ресурсов и прерывания
  ///дальнейших действий [doUseCase].
  Future<void> cancelUseCase<A extends IAccessor>(A accessor);

  ///Вызывается один раз перед первым выплнением [doUseCase]
  Future init<A extends IAccessor>(A accessor);

  ///Вызывается каждый раз после вызова [onError] в [doUseCase]
  ///и реализует логику разрешения ошибки
  ///если ошибку удалось разрешить, то необходимо вернуть [ErrorStatus.resolved]
  ///это приведет к повторному выполнению [doUseCase]
  ///если ошибку не удалось разрешить, то необходимо вернуть [ErrorStatus.unresolved]
  ///это приведет к завершению жизненного цикла UseCase,
  ///в качестве результата выполнения UseCase вернется [UseCaseError]
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor);

  ///индекс очереди в которой будет выполняться [UseCaseBase]
  ///если [orderGroupIndex] у вызванных последовательно [UseCaseBase] совпадает,
  ///то [init] и [doUseCase] у следующего в очереди [UseCaseBase]
  ///не будет вызван до тех пор пока текущий [UseCaseBase] не вызовет [onEnd]
  int get orderGroupIndex => 0;

  @override
  String toString() {
    return 'UseCaseBase(_id: $_id, dataIn: $dataIn, createdAt: $createdAt)';
  }
}
