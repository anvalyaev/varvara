import 'package:collection/collection.dart';
import 'package:core/core.dart';
import 'package:isolated_clean_architecture/data/repositories/index.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:isolator_channels/isolator_channels.dart';

abstract class IAccessor {
  IUseCaseRepository get repositoryUseCase;
}

class MessageManager<A extends IAccessor> {
  IChannel channel;
  A accessor;
  MessageManager({
    required this.channel,
    required this.accessor,
  }) {
    channel.reciveStream<UseCaseBase>().listen((useCase) async {
      // logger.i('Recived UseCase: $useCase');
      if (accessor.repositoryUseCase.contains(useCase.id)) return;
      final useCaseStates = accessor.repositoryUseCase.getUseCaseStates();

      var processing = (accessor.repositoryUseCase.getUseCases())
          .where(
              (element) => element.orderGroupIndex == useCase.orderGroupIndex)
          .where((element) {
        final state = useCaseStates[element.id];
        return state == UseCaseState.processing ||
            state == UseCaseState.pending;
      });

      if (processing.isEmpty) {
        accessor.repositoryUseCase
            .setUseCaseState(useCase.id, UseCaseState.processing);
        accessor.repositoryUseCase.updateUseCase(useCase);
        _doUseCase(useCase);
      } else {
        accessor.repositoryUseCase
            .setUseCaseState(useCase.id, UseCaseState.pending);
        accessor.repositoryUseCase.updateUseCase(useCase);
      }
    });

    channel.reciveStream<UseCaseCancel>().listen((useCaseCancel) async {
      if (!accessor.repositoryUseCase.contains(useCaseCancel.ucid)) {
        return;
      }
      final useCase = accessor.repositoryUseCase.getUseCase(useCaseCancel.ucid);
      await useCase?.cancelUseCase(accessor);
      accessor.repositoryUseCase.removeUseCase(useCaseCancel.ucid);
    });
  }

  void restore() async {
    var useCases = accessor.repositoryUseCase.getUseCases();
    var states = accessor.repositoryUseCase.getUseCaseStates(
        useCaseIds: useCases.map<UseCaseId>((e) => e.id).toSet());

    if (useCases.isNotEmpty) {
      var firstInProgress = useCases.firstWhereOrNull(
          (element) => states[element.id] == UseCaseState.processing);
      if (firstInProgress != null) {
        firstInProgress.doUseCase<A>(
          accessor,
          _useCaseHandlerOnResult,
          _useCaseHandlerOnError,
          _useCaseHandlerOnComplete,
        );
      } else {
        var firstPending = useCases.firstWhereOrNull(
            (element) => states[element.id] == UseCaseState.pending);
        firstPending?.doUseCase<A>(
          accessor,
          _useCaseHandlerOnResult,
          _useCaseHandlerOnError,
          _useCaseHandlerOnComplete,
        );
      }
    }
  }

  void _doUseCase(UseCaseBase useCase) {
    logger.i('Do UseCase: $useCase');
    useCase.doUseCase<A>(
      accessor,
      _useCaseHandlerOnResult,
      _useCaseHandlerOnError,
      _useCaseHandlerOnComplete,
    );
  }

  void _runNextUseCaseIfAvailable(UseCaseBase useCase) {
    var useCases =
        accessor.repositoryUseCase.orderGroupUseCases(useCase.orderGroupIndex);
    var states = accessor.repositoryUseCase.getUseCaseStates(
        useCaseIds: useCases.map<UseCaseId>((e) => e.id).toSet());

    useCases = useCases
        .where((element) => states[element.id] == UseCaseState.pending)
        .toList();

    if (useCases.isNotEmpty) {
      var firstUseCase = useCases.first;
      accessor.repositoryUseCase
          .setUseCaseState(firstUseCase.id, UseCaseState.processing);
      _doUseCase(firstUseCase);
    }
  }

  void _useCaseHandlerOnComplete(UseCaseId ucid) async {
    if (!accessor.repositoryUseCase.contains(ucid)) {
      return;
    }
    final useCase = accessor.repositoryUseCase.getUseCase(ucid);
    final useCaseState = accessor.repositoryUseCase.getUseCaseState(ucid);
    if (useCaseState == UseCaseState.complete) {
      throw Exception('Re-completing a use case  $ucid');
    }
    channel.send(UseCaseComplete(ucid));

    accessor.repositoryUseCase
        .setUseCaseState(useCase!.id, UseCaseState.complete);
    accessor.repositoryUseCase.removeUseCase(useCase.id);
    _runNextUseCaseIfAvailable(useCase);
  }

  Future<void> _useCaseHandlerOnError(UseCaseError error) async {
    if (!accessor.repositoryUseCase.contains(error.ucid)) {
      return;
    }
    final useCase = accessor.repositoryUseCase.getUseCase(error.ucid);
    final useCaseState = accessor.repositoryUseCase.getUseCaseState(error.ucid);
    if (useCaseState == UseCaseState.complete) {
      throw Exception('The $error for the completed use case is obtained.');
    }

    accessor.repositoryUseCase.setUseCaseState(useCase!.id, UseCaseState.error);

    var errorStatus = await useCase.resolveError(accessor);

    if (errorStatus == ErrorStatus.resolved) {
      accessor.repositoryUseCase
          .setUseCaseState(useCase.id, UseCaseState.processing);
      accessor.repositoryUseCase.updateUseCase(useCase);
      _doUseCase(useCase);
    } else if (errorStatus == ErrorStatus.unresolved) {
      channel.send(error);
      accessor.repositoryUseCase.removeUseCase(useCase.id);
      _runNextUseCaseIfAvailable(useCase);
    }
  }

  void _useCaseHandlerOnResult<Out extends UseCaseResult>(Out result) {
    if (!accessor.repositoryUseCase.contains(result.ucid)) {
      return;
    }
    final useCaseState =
        accessor.repositoryUseCase.getUseCaseState(result.ucid);
    if (useCaseState == UseCaseState.complete) {
      throw Exception('The $result for the completed use case is obtained.');
    }
    channel.send(result);
  }
}
