import 'dart:async';

import 'package:core/core.dart';
import 'package:flutter/foundation.dart';
import 'package:go_router/go_router.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:get_it/get_it.dart';
import 'accessor_controller.dart';

abstract mixin class Presenter {
  MessageController get controller =>
      GetIt.instance.get<MessageController>(instanceName: microAppName);
  GoRouter get router => GetIt.instance.get<GoRouter>();
  Future<GoRouter> get routerAsync => GetIt.instance.getAsync<GoRouter>();

  final Set<UseCaseId> _myUseCases = {};

  bool wantKeepAlive = false;

  String get microAppName;

  @protected
  Future<Out?>
      execute<T extends UseCaseBase<In, Out>, In, Out extends UseCaseResult>(
          T useCase) async {
    _myUseCases.add(useCase.id);
    controller.executeUseCase(useCase);
    Out? useCaseResult;
    final useCaseStream = controller.allMessages.where((event) {
      logger.i(event);
      if (!_myUseCases.contains(useCase.id)) return false;
      if (event is UseCaseComplete) {
        return event.ucid == useCase.id;
      } else if (event is Out) {
        return event.ucid == useCase.id;
      } else if (event is UseCaseError) {
        return event.ucid == useCase.id;
      }
      return false;
    }).takeWhile((element) {
      if (element is UseCaseError) {
        throw element;
      } else if (element is UseCaseComplete) {
        return false;
      }
      return true;
    });

    try {
      await useCaseStream.forEach((element) {
        if (element is Out && useCaseResult == null) {
          useCaseResult = element;
        }
      });
    } catch (e) {
      rethrow;
    }
    return useCaseResult;
  }

  @protected
  Stream<Out>
      subscribe<T extends UseCaseBase<In, Out>, In, Out extends UseCaseResult>(
          T useCase) {
    _myUseCases.add(useCase.id);
    controller.executeUseCase(useCase);

    final useCaseStream = controller.allMessages.where((event) {
      if (!_myUseCases.contains(useCase.id)) return false;
      if (event is UseCaseComplete) {
        return event.ucid == useCase.id;
      } else if (event is Out) {
        return event.ucid == useCase.id;
      } else if (event is UseCaseError) {
        return event.ucid == useCase.id;
      }
      return false;
    }).takeWhile((element) {
      if (element is UseCaseError) {
        throw element;
      } else if (element is UseCaseComplete) {
        return false;
      }
      return true;
    });

    return useCaseStream
        .where((event) => event is Out)
        .map((event) => event as Out);
  }

  void unsubscribe(UseCaseId ucid) {
    _myUseCases.remove(ucid);
    controller.cancelUseCase(UseCaseCancel(ucid));
  }

  void unsubscribeAll() {
    for (var useCaseId in _myUseCases.toList()) {
      unsubscribe(useCaseId);
    }
  }
}
