import 'dart:async';
import 'dart:io';
import 'dart:isolate';

import 'package:flutter/services.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:isolator_channels/isolator_channels.dart';

typedef CreateMesageManager = void Function(IChannel channel);
typedef ErrorListener = Future<void> Function(
    {dynamic exception, StackTrace? stackTrace});

class MessageController {
  bool initialized = false;
  final _channelOwner = ChannelOwner();
  final List<Message> _buffer = [];
  final String name;

  MessageController(
      {required CreateMesageManager createMessageManager,
      required ErrorListener errorListener,
      required VoidCallback onInitiated,
      required this.name}) {
    init(createMessageManager, errorListener, name).whenComplete(onInitiated);
  }

  void executeUseCase(UseCaseBase useCase) {
    if (initialized) {
      _channelOwner.send(useCase, channelName: name);
    } else {
      _buffer.add(useCase);
    }
  }

  void cancelUseCase(UseCaseCancel useCaseCancel) {
    if (initialized) {
      _channelOwner.send(useCaseCancel, channelName: name);
    } else {
      _buffer.add(useCaseCancel);
    }
  }

  void dispose() {
    _buffer.clear();
  }

  Stream<UseCaseResult> get useCaseResults =>
      _channelOwner.reciveStream<UseCaseResult>(channelName: name);
  Stream<UseCaseError> get useCaseErrors =>
      _channelOwner.reciveStream<UseCaseError>(channelName: name);
  Stream<UseCaseComplete> get useCaseCompletes =>
      _channelOwner.reciveStream<UseCaseComplete>(channelName: name);
  Stream<Message> get allMessages =>
      _channelOwner.reciveStream<Message>(channelName: name);

  Future<void> init(CreateMesageManager createMessageManager,
      ErrorListener errorListener, String name) async {
    if (!initialized) {
      var isolate = await _channelOwner.createChannel(
          entryPoint: createMessageManager, channelName: name);
      if (isolate != null) {
        isolate.addErrorListener(RawReceivePort((pair) async {
          initialized = false;
          final List<dynamic> errorAndStacktrace = pair;
          await errorListener(
              exception: errorAndStacktrace.first,
              stackTrace: errorAndStacktrace.last is StackTrace
                  ? errorAndStacktrace.last
                  : null);
          if (Platform.isAndroid) {
            SystemNavigator.pop();
          } else {
            exit(0);
          }
        }).sendPort);
      }

      for (var message in _buffer) {
        _channelOwner.send(message, channelName: name);
      }
      _buffer.clear();
      initialized = true;
    }
  }
}
