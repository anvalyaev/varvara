import 'package:components/components.dart';
import 'package:flutter/material.dart';
import 'package:theme/theme.dart';

class CounterStyleByTheme extends CounterStyle {
  factory CounterStyleByTheme.of(BuildContext context) {
    return CounterStyleByTheme._(
        textStyle: context.baseBold,
        textColor: context.cfPrimary.cfOnPrimary.color,
        backgroundColor: context.cfPrimary.color);
  }

  factory CounterStyleByTheme.ofSecondary(BuildContext context) {
    return CounterStyleByTheme._(
        textStyle: context.smallCaption,
        textColor: context.cfPrimary.cfOnPrimary.color,
        backgroundColor: Colors.transparent);
  }

  @override
  CounterStyleByTheme copyWith(
          {TextStyle? textStyle, Color? textColor, Color? backgroundColor}) =>
      CounterStyleByTheme._(
          textStyle: textStyle ?? this.textStyle,
          textColor: textColor ?? this.textColor,
          backgroundColor: backgroundColor ?? this.backgroundColor);

  const CounterStyleByTheme._({
    required super.textStyle,
    required super.textColor,
    required super.backgroundColor,
  });
}
