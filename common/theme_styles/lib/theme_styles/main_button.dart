import 'package:components/components.dart';
import 'package:flutter/material.dart';
import 'package:theme/theme.dart';

@immutable
class MainButtonStyleByTheme extends MainButtonStyle {
  factory MainButtonStyleByTheme.of(BuildContext context) {
    return MainButtonStyleByTheme._(
        textStyle: context.button,
        backgroundColor: context.cfPrimary.color,
        disabledColor: context.bPrimary.color,
        disabledTextColor: context.bPrimary.nSecondary300.color,
        foregroundColor: context.cfPrimary.cfOnPrimary.color);
  }

  factory MainButtonStyleByTheme.primaryOf(BuildContext context) {
    return MainButtonStyleByTheme._(
        textStyle: context.button,
        backgroundColor: context.cfPrimary.color,
        disabledColor: context.bPrimary.color,
        disabledTextColor: context.bPrimary.nSecondary300.color,
        foregroundColor: context.cfPrimary.cfOnPrimary.color);
  }

  factory MainButtonStyleByTheme.secondaryOf(BuildContext context) {
    return MainButtonStyleByTheme._(
        textStyle: context.button,
        backgroundColor: context.bPrimary.color,
        disabledColor: context.bPrimary.color,
        disabledTextColor: context.bPrimary.nSecondary300.color,
        foregroundColor: context.cfPrimary.cfOnPrimary.color);
  }

  factory MainButtonStyleByTheme.acceptOf(BuildContext context) {
    return MainButtonStyleByTheme._(
        textStyle: context.button,
        backgroundColor: context.cfSuccess.color,
        disabledColor: context.bPrimary.color,
        disabledTextColor: context.bPrimary.nSecondary300.color,
        foregroundColor: context.cfSuccess.cfOnSuccess.color);
  }

  factory MainButtonStyleByTheme.errorOf(BuildContext context) {
    return MainButtonStyleByTheme._(
        textStyle: context.button,
        backgroundColor: context.cfWarning.color,
        disabledColor: context.bPrimary.color,
        disabledTextColor: context.bPrimary.nSecondary300.color,
        foregroundColor: context.cfWarning.cfOnWarning.color);
  }

  const MainButtonStyleByTheme._({
    required Color backgroundColor,
    required Color foregroundColor,
    required Color disabledColor,
    required Color disabledTextColor,
    required TextStyle textStyle,
  }) : super(
            backgroundColor: backgroundColor,
            foregroundColor: foregroundColor,
            disabledColor: disabledColor,
            disabledTextColor: disabledTextColor,
            textStyle: textStyle);

  @override
  MainButtonStyleByTheme copyWith(
      {Color? backgroundColor,
      Color? foregroundColor,
      TextStyle? textStyle,
      Color? disabledColor,
      Color? disabledTextColor}) {
    return MainButtonStyleByTheme._(
        backgroundColor: backgroundColor ?? this.backgroundColor,
        foregroundColor: foregroundColor ?? this.foregroundColor,
        disabledTextColor: disabledTextColor ?? this.disabledTextColor,
        disabledColor: disabledColor ?? this.disabledColor,
        textStyle: textStyle ?? this.textStyle);
  }
}
