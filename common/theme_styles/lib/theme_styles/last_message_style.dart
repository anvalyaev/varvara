import 'package:components/style.dart';
import 'package:flutter/material.dart';
import 'package:theme/theme.dart';
import 'package:theme/theme/app_theme.dart';

class LastMessageStyle extends ComponentStyle {
  final TextStyle authorNameTextStyle;
  final TextStyle messageTextStyle;

  LastMessageStyle(
    Color authorColor,
    BuildContext context,
  )   : authorNameTextStyle = context.baseRegular.copyWith(color: authorColor),
        messageTextStyle = context.baseRegular
            .copyWith(color: context.bSecondary100.nSecondary200.color);

  factory LastMessageStyle.byAuthorName(
    String authorFullName,
    BuildContext context,
  ) {
    final authorColor = AppTheme.of(context)
        .colorScheme
        .other
        .getColor(authorFullName.hashCode % 12);
    return LastMessageStyle(authorColor, context);
  }

  factory LastMessageStyle.defaultStyle(BuildContext context) =>
      LastMessageStyle(Colors.white, context);
}
