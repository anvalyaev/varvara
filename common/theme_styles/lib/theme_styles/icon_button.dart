import 'package:components/components.dart';
import 'package:flutter/material.dart';
import 'package:theme/theme.dart';

@immutable
class IconButtonStyleByTheme extends IconButtonStyle {
  factory IconButtonStyleByTheme.of(BuildContext context) {
    return IconButtonStyleByTheme._(
        foregroundColor: context.colorScheme.bSecondary100.nPrimary.color,
        backgroundColor: context.bPrimary.color,
        disabledColor: context.colorScheme.bSecondary100.nSecondary300.color,
        hoverColor: Color(0x0AFFFFFF));
  }

  const IconButtonStyleByTheme._({
    required Color foregroundColor,
    required Color backgroundColor,
    required Color disabledColor,
    required Color hoverColor,
  }) : super(
            foregroundColor: foregroundColor,
            disabledColor: disabledColor,
            backgroundColor: backgroundColor,
            hoverColor: hoverColor);

  IconButtonStyleByTheme copyWith({
    Color? backgroundColor,
    Color? foregroundColor,
    Color? disabledColor,
    Color? hoverColor,
  }) {
    return IconButtonStyleByTheme._(
        foregroundColor: foregroundColor ?? this.foregroundColor,
        backgroundColor: backgroundColor ?? this.backgroundColor,
        disabledColor: disabledColor ?? this.disabledColor,
        hoverColor: hoverColor ?? this.hoverColor);
  }
}
