import 'package:components/style.dart';
import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  static const double _height = 56.0;
  final MainButtonStyle style;
  final String? text;
  final bool busy;
  final VoidCallback? onPressed;
  final VoidCallback? onLongPressed;
  final double height;
  final bool enable;
  final IconData? icon;
  final Widget? loader;
  final EdgeInsets padding;
  final ShapeBorder? customBorder;

  final BorderRadius borderRadius;

  MainButton(
      {Key? key,
      required this.style,
      this.text,
      this.onPressed,
      this.onLongPressed,
      this.icon,
      this.busy = false,
      this.height = _height,
      this.enable = true,
      this.loader,
      this.padding = const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      BorderRadius? borderRadius,
      this.customBorder})
      : this.borderRadius = borderRadius ?? BorderRadius.circular(8),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: borderRadius,
      color: enable ? style.backgroundColor : style.disabledColor,
      child: Row(mainAxisSize: MainAxisSize.max, children: [
        Expanded(
            child: InkWell(
          customBorder: customBorder,
          enableFeedback: onPressed != null,
          borderRadius: borderRadius,
          hoverColor: style.hoverColor,
          highlightColor: style.highlightColor,
          splashColor: style.splashColor,
          onTap: enable
              ? busy
                  ? null
                  : onPressed
              : null,
          onLongPress: enable
              ? busy
                  ? null
                  : onLongPressed
              : null,
          child: ConstrainedBox(
            constraints: const BoxConstraints(
                minWidth: _height, maxHeight: _height, minHeight: _height),
            child: Padding(
              padding: padding,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  AnimatedOpacity(
                    duration: const Duration(milliseconds: 300),
                    opacity: busy ? 0.0 : 1.0,
                    curve: Curves.ease,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (icon != null)
                          Padding(
                            padding:
                                EdgeInsets.only(right: text != null ? 4 : 0),
                            child: Icon(icon,
                                color: enable
                                    ? style.foregroundColor
                                    : style.disabledTextColor),
                          ),
                        if (text != null)
                          Flexible(
                            fit: FlexFit.loose,
                            child: Text(
                              text!,
                              overflow: TextOverflow.ellipsis,
                              style: style.textStyle.copyWith(
                                  color: enable
                                      ? style.foregroundColor
                                      : style.disabledTextColor),
                              textAlign: TextAlign.center,
                            ),
                          ),
                      ],
                    ),
                  ),
                  AnimatedOpacity(
                    duration: const Duration(milliseconds: 300),
                    opacity: busy ? 1.0 : 0.0,
                    curve: Curves.ease,
                    child: SizedBox(
                        height: height * 0.58,
                        width: height * 0.58,
                        child: const FittedBox(
                            child: CircularProgressIndicator(
                          color: Colors.white,
                        ))),
                  )
                ],
              ),
            ),
          ),
        ))
      ]),
    );
  }
}

class MainButtonStyle extends ComponentStyle {
  final TextStyle textStyle;
  final Color foregroundColor;
  final Color backgroundColor;
  final Color disabledColor;
  final Color disabledTextColor;
  final Color splashColor;
  final Color hoverColor;
  final Color highlightColor;

  const MainButtonStyle({
    required this.textStyle,
    required this.foregroundColor,
    required this.backgroundColor,
    this.disabledColor = Colors.white38,
    required this.disabledTextColor,
    this.splashColor = const Color(0x40CCCCCC),
    this.hoverColor = const Color(0x0AFFFFFF),
    this.highlightColor = const Color(0x0ACCCCCC),
  });

  MainButtonStyle copyWith(
      {Color? backgroundColor,
      Color? foregroundColor,
      TextStyle? textStyle,
      Color? disabledColor,
      Color? disabledTextColor}) {
    return MainButtonStyle(
        backgroundColor: backgroundColor ?? this.backgroundColor,
        foregroundColor: foregroundColor ?? this.foregroundColor,
        disabledTextColor: disabledTextColor ?? this.disabledTextColor,
        disabledColor: disabledColor ?? this.disabledColor,
        textStyle: textStyle ?? this.textStyle);
  }

  factory MainButtonStyle.demo() {
    return const MainButtonStyle(
      textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.w700),
      disabledTextColor: Colors.white38,
      foregroundColor: Color(0xFFFFFFFF),
      backgroundColor: Color(0xFF009DE7),
    );
  }
}
