import 'package:components/style.dart';
import 'package:flutter/material.dart';

import '../common/counter.dart';

class IconButton extends StatelessWidget {
  final IconData icon;
  final IconButtonStyle style;
  final String? tooltip;
  final bool busy;
  final VoidCallback? onPressed;
  final GestureTapDownCallback? onTapDown;
  final VoidCallback? onTapCancel;
  final Counter? counter;
  final double iconSize;
  final Alignment iconAlignment;
  final double splashRadius;
  final EdgeInsetsGeometry? margin;
  final ShapeBorder shape;
  final Clip clipBehavior;
  final Decoration? decoration;
  final Size? bottonSize;
  final VoidCallback? onLongPress;
  final GestureLongPressEndCallback? onLongPressEnd;
  final List<Shadow>? shadows;

  const IconButton(
      {Key? key,
      required this.icon,
      required this.style,
      this.tooltip,
      this.onPressed,
      this.busy = false,
      this.counter,
      this.onTapDown,
      this.iconSize = 24,
      this.splashRadius = 24,
      this.iconAlignment = Alignment.center,
      this.margin,
      this.shape = const CircleBorder(),
      this.clipBehavior = Clip.none,
      this.onTapCancel,
      this.decoration,
      this.bottonSize,
      this.onLongPress,
      this.onLongPressEnd,
      this.shadows})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));
    Color? foregroundColor;
    if (onPressed != null || onTapDown != null) {
      foregroundColor = style.foregroundColor;
    } else {
      foregroundColor = style.disabledColor;
    }
    Widget child = Container(
      margin: margin,
      height: bottonSize?.height ?? iconSize,
      width: bottonSize?.width ?? iconSize,
      decoration: decoration,
      child: IconTheme.merge(
        data: IconThemeData(
          size: iconSize,
          color: foregroundColor,
        ),
        child: Stack(
          alignment: iconAlignment,
          children: [
            AnimatedOpacity(
                duration: const Duration(milliseconds: 300),
                opacity: busy ? 0.0 : 1.0,
                curve: Curves.ease,
                child: Icon(icon, shadows: shadows)),
            AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              opacity: busy ? 1.0 : 0.0,
              curve: Curves.ease,
              child: SizedBox(
                height: iconSize / 1.5,
                width: iconSize / 1.5,
                child: const FittedBox(
                    child: CircularProgressIndicator(
                  color: Colors.white,
                )),
              ),
            )
          ],
        ),
      ),
    );
    return GestureDetector(
      behavior: HitTestBehavior.deferToChild,
      onLongPress: onLongPress,
      onLongPressEnd: onLongPressEnd,
      child: InkResponse(
        onTap: busy ? null : onPressed,
        onTapDown: onTapDown,
        onTapCancel: onTapCancel,
        enableFeedback: onPressed != null || onTapDown != null,
        hoverColor: style.hoverColor,
        highlightColor: style.highlightColor,
        splashColor: style.splashColor,
        radius: splashRadius,
        child: counter != null
            ? Stack(clipBehavior: Clip.none, children: [
                Center(child: child),
                Positioned(
                    right: 0,
                    top: 28,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        counter!,
                      ],
                    ))
              ])
            : child,
      ),
    );
  }
}

class IconButtonStyle extends ComponentStyle {
  final Color backgroundColor;
  final Color foregroundColor;
  final Color disabledColor;
  final Color splashColor;
  final Color hoverColor;
  final Color highlightColor;
  final Color? fillColor;

  const IconButtonStyle(
      {this.fillColor = Colors.transparent,
      required this.foregroundColor,
      required this.disabledColor,
      this.backgroundColor = Colors.transparent,
      this.splashColor = const Color(0x40CCCCCC),
      this.hoverColor = const Color(0x0AFFFFFF),
      this.highlightColor = const Color(0x0ACCCCCC)});

  factory IconButtonStyle.demo(int num) {
    switch (num) {
      case 0:
        return const IconButtonStyle(
            foregroundColor: Color(0xFFFFFFFF), disabledColor: Colors.white38);
      case 1:
        return const IconButtonStyle(
            foregroundColor: Color(0xFF674EA7),
            disabledColor: Color(0xFF5B5B5B));
      case 2:
        return const IconButtonStyle(
            foregroundColor: Color(0xFFF44336),
            disabledColor: Color(0xFFFFFFFF));
      default:
        return const IconButtonStyle(
            foregroundColor: Color(0xFFFFFFFF), disabledColor: Colors.white38);
    }
  }
}
