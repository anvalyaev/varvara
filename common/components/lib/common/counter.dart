import 'package:components/style.dart';
import 'package:flutter/material.dart';

enum CounterBaseType { round, ellipse }

class Counter extends StatelessWidget {
  final int count;
  final int maxCount;
  final CounterStyle style;
  final double size;
  final EdgeInsets? padding;
  final Color paddingColor;
  final CounterBaseType type;

  const Counter(
      {Key? key,
      required this.count,
      required this.maxCount,
      required this.style,
      required this.size,
      this.padding,
      this.paddingColor = Colors.transparent,
      this.type = CounterBaseType.round})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (count < 1) {
      return SizedBox(
        height: size,
        width: size,
      );
    }
    Widget child = LayoutBuilder(builder: (context, constraint) {
      late int countSize;
      late String countText;
      if (count > maxCount) {
        countSize =
            _getCount(maxCount) + (type == CounterBaseType.ellipse ? 2 : 0);
        countText = '+$maxCount';
      } else {
        countSize =
            _getCount(count) + (type == CounterBaseType.ellipse ? 2 : 0);
        countText = '$count';
      }
      double width = size + (countSize * size * 0.25);

      if (constraint.maxWidth < width) {
        if (width > size && !(count > maxCount)) countText = '+$countText';
        double currentCountSize = (constraint.maxWidth - size) / (size * 0.25);
        int indent =
            countText.length - (countSize - currentCountSize).round() - 1;
        countText = countText.substring(
            0,
            indent > 1
                ? indent
                : countSize < 1
                    ? 1
                    : 2);
      }
      return ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: width, maxHeight: size, minWidth: size, minHeight: size),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: style.backgroundColor,
                  borderRadius: BorderRadius.circular(size * 0.5),
                ),
                alignment: Alignment.center,
                child: Text(
                  countText,
                  style: style.textStyle.copyWith(
                      fontSize: type == CounterBaseType.ellipse
                          ? style.textStyle.fontSize
                          : size * 0.5,
                      color: style.textColor),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  softWrap: true,
                  maxLines: 1,
                ),
              ),
            ),
          ],
        ),
      );
    });
    if (padding != null) {
      child = Container(
        padding: const EdgeInsets.all(1),
        decoration: BoxDecoration(
          color: paddingColor,
          borderRadius: BorderRadius.circular(size * 0.7),
        ),
        child: child,
      );
    }
    return child;
  }

  int _getCount(int counter) {
    int countSize = -1;
    while (counter != 0) {
      countSize += 1;
      counter = (counter / 10).truncate();
    }
    return countSize;
  }
}

class CounterStyle extends ComponentStyle {
  final TextStyle textStyle;
  final Color backgroundColor;
  final Color textColor;

  CounterStyle copyWith(
          {TextStyle? textStyle, Color? textColor, Color? backgroundColor}) =>
      CounterStyle(
          textStyle: textStyle ?? this.textStyle,
          textColor: textColor ?? this.textColor,
          backgroundColor: backgroundColor ?? this.backgroundColor);

  const CounterStyle(
      {required this.textStyle,
      required this.backgroundColor,
      required this.textColor});

  factory CounterStyle.demo() {
    return const CounterStyle(
      textStyle: TextStyle(),
      textColor: Color(0xFFFFFFFF),
      backgroundColor: Color(0xFF009DE7),
    );
  }
}
