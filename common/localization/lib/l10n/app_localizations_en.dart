import 'package:intl/intl.dart' as intl;

import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String minSizeMessage(num count) {
    String _temp0 = intl.Intl.pluralLogic(
      count,
      locale: localeName,
      other: 'Min $count symbols',
      one: 'Min $count symbol',
    );
    return '$_temp0';
  }

  @override
  String maxSizeMessage(num count) {
    String _temp0 = intl.Intl.pluralLogic(
      count,
      locale: localeName,
      other: 'Max $count symbols',
      one: 'Max $count symbol',
    );
    return '$_temp0';
  }

  @override
  String get exit_from_app => 'Closing the application';

  @override
  String get exit_from_app_question => 'Are you sure you want to close the application?';

  @override
  String get ok => 'Ok';

  @override
  String get cancel => 'Cancel';

  @override
  String membersCount(num count) {
    String _temp0 = intl.Intl.pluralLogic(
      count,
      locale: localeName,
      other: '$count members',
      one: '$count member',
    );
    return '$_temp0';
  }
}
