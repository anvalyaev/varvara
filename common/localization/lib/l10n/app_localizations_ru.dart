import 'package:intl/intl.dart' as intl;

import 'app_localizations.dart';

/// The translations for Russian (`ru`).
class AppLocalizationsRu extends AppLocalizations {
  AppLocalizationsRu([String locale = 'ru']) : super(locale);

  @override
  String minSizeMessage(num count) {
    String _temp0 = intl.Intl.pluralLogic(
      count,
      locale: localeName,
      other: 'Минимум $count символов',
      few: 'Минимум $count символа',
      one: 'Минимум $count символ',
    );
    return '$_temp0';
  }

  @override
  String maxSizeMessage(num count) {
    String _temp0 = intl.Intl.pluralLogic(
      count,
      locale: localeName,
      other: 'Максимум $count символов',
      few: 'Максимум $count символа',
      one: 'Максимум $count символ',
    );
    return '$_temp0';
  }

  @override
  String get exit_from_app => 'Выйти из приложения';

  @override
  String get exit_from_app_question => 'Вы уверенны, чо хотите выйти из приложения?';

  @override
  String get ok => 'Ок';

  @override
  String get cancel => 'Отмена';

  @override
  String membersCount(num count) {
    String _temp0 = intl.Intl.pluralLogic(
      count,
      locale: localeName,
      other: '$count участников',
      few: '$count участника',
      one: '$count участник',
    );
    return '$_temp0';
  }
}
