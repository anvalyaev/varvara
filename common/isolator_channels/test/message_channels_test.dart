import 'package:flutter_test/flutter_test.dart';

import 'package:isolator_channels/isolator_channels.dart';

class Counter with Message {
  int data;
  Counter(this.data);
}

void testEntryPoint(IChannel slave) async {
  slave.reciveStream<Counter>().listen((event) {
    print('slave recive: ${event.data}');
  });
  for (int i = 1; i <= 100; ++i) {
    print('slave send: ${i}');

    slave.send(Counter(i));
    if (i == 15) {
      Counter? p;
      var j = 10 / 0;
      print('${p!.data}, $j');
    }
    await Future.delayed(Duration(milliseconds: 500));
  }
}

void main() {
  test('adds one to input values', () async {
    const String testChannel = "test";
    var master = ChannelOwner();
    await master.createChannel(
        channelName: testChannel, entryPoint: testEntryPoint);
    master.reciveStream<Counter>(channelName: testChannel).listen((event) {
      print('master recive: ${event.data}');
    });
    for (int i = 1; i <= 100; ++i) {
      print('master send: ${i}');
      master.send(Counter(i), channelName: testChannel);
      await Future.delayed(Duration(seconds: 1));
    }
    await master
        .reciveStream<Counter>(channelName: testChannel)
        .firstWhere((element) => element.data == 100);
    print("end");
  });
}
