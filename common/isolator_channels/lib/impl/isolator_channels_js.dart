import 'package:isolator_channels/isolator_channels.dart' as i;
import 'dart:async';

class InitialData implements i.InitialData {
  Sink<i.Message?> sendSink;
  Stream<i.Message?> reciveStream;
  String channelName;
  InitialData(
      {required this.channelName,
      required this.sendSink,
      required this.reciveStream});
}

class ChannelOwner implements i.IChannelOwner {
  void send(i.Message message, {required String channelName}) {
    if (!_masterSendSink.containsKey(channelName))
      throw Exception("unknown channel");
    _masterSendSink[channelName]!.add(message);
  }

  Stream<T> reciveStream<T extends i.Message>({required String channelName}) {
    if (!_masterReciveStream.containsKey(channelName))
      throw Exception("unknown channel");
    return _masterReciveStream[channelName]!
        .where((message) => message != null)
        .where((message) => message is i.Message)
        .where((message) => message is T)
        .map<T>((event) => event as T);
  }

  Future<dynamic> createChannel(
      {required String channelName, required i.EntryPoint entryPoint}) async {
    var masterController = StreamController<i.Message?>.broadcast();
    var slaveController = StreamController<i.Message?>.broadcast();

    _slaveControllers[channelName] = slaveController;
    _masterControllers[channelName] = masterController;

    var data = InitialData(
        channelName: channelName,
        sendSink: _slaveSendSink[channelName]!,
        reciveStream: _slaveReciveStream[channelName]!);
    var channel = Channel._();
    channel.init(data);
    entryPoint(channel);
    return null;
  }

  Map<String, StreamController<i.Message?>> _slaveControllers = {};
  Map<String, StreamController<i.Message?>> _masterControllers = {};

  Map<String, Sink<i.Message?>> get _slaveSendSink => _slaveControllers
      .map<String, Sink<i.Message?>>((key, value) => MapEntry(key, value.sink));
  Map<String, Sink<i.Message?>> get _masterSendSink => _masterControllers
      .map<String, Sink<i.Message?>>((key, value) => MapEntry(key, value.sink));
  Map<String, Stream<i.Message?>> get _slaveReciveStream =>
      _masterControllers.map<String, Stream<i.Message?>>(
          (key, value) => MapEntry(key, value.stream));
  Map<String, Stream<i.Message?>> get _masterReciveStream =>
      _slaveControllers.map<String, Stream<i.Message?>>(
          (key, value) => MapEntry(key, value.stream));
}

class Channel implements i.IChannel {
  String? channelName;
  Channel._();
  void init(i.InitialData data) {
    var initialData = data as InitialData;
    channelName = initialData.channelName;
    _slaveReciveStream = initialData.reciveStream;
    _masterSendSink = initialData.sendSink;
  }

  void send(i.Message message) {
    message.channelName = channelName!;
    _masterSendSink!.add(message);
  }

  Stream<T> reciveStream<T extends i.Message>() => _slaveReciveStream!
      .where((message) => message != null)
      .where((message) => message is i.Message)
      .where((message) => message is T)
      .map<T>((event) => event as T);

  Stream<i.Message?>? _slaveReciveStream;
  Sink<i.Message?>? _masterSendSink;
}
