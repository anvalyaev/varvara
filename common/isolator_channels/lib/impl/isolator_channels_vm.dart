import 'package:isolator_channels/isolator_channels.dart' as i;

import 'dart:async';
import 'dart:isolate';

class InitialData implements i.InitialData {
  SendPort sendPort;
  String channelName;
  i.EntryPoint entryPoint;
  InitialData(
      {required this.channelName,
      required this.sendPort,
      required this.entryPoint});
}

class ChannelOwner implements i.IChannelOwner {
  ChannelOwner() {}

  @override
  void send(i.Message message, {required String channelName}) {
    if (!_slaveSendPorts.containsKey(channelName))
      throw Exception("unknown channel");
    message.channelName = channelName;
    _slaveSendPorts[channelName]!.send(message);
  }

  Stream<T> reciveStream<T extends i.Message>({required String channelName}) {
    if (!_slaveSendPorts.containsKey(channelName))
      throw Exception("unknown channel");

    return _masterRecivePorts[channelName]!
        .where((message) => message != null)
        .where((message) => message is i.Message)
        .where((message) => message is T)
        .map<T>((event) => event as T);
  }

  static void _spawn(InitialData data) {
    var slave = Channel._();
    slave.init(data);
    data.entryPoint(slave);
  }

  Future<Isolate> createChannel(
      {required String channelName, required i.EntryPoint entryPoint}) async {
    ReceivePort receivePort = new ReceivePort();

    var isolate = await Isolate.spawn<InitialData>(
      _spawn,
      InitialData(
          channelName: channelName,
          sendPort: receivePort.sendPort,
          entryPoint: entryPoint),
    );
    var bloadCastReceivePort = receivePort.asBroadcastStream();

    isolate.errors.listen((event) {
      print('error: ${event.toString()}');
      // createSlave(channelName);
      // isolate.resume(Capability());
    });

    var sendPort =
        await bloadCastReceivePort.firstWhere((element) => element is SendPort);

    _slaveSendPorts[channelName] = sendPort;
    _masterRecivePorts[channelName] = bloadCastReceivePort;
    return isolate;
  }

  Map<String, SendPort> _slaveSendPorts = {};
  Map<String, Stream> _masterRecivePorts = {};
}

class Channel implements i.IChannel {
  String? channelName;
  Channel._();
  void init(i.InitialData data) {
    if (channelName != null) {
      throw Exception('The object cannot manually initialized');
    }
    var initialData = data as InitialData;
    channelName = initialData.channelName;
    _masterSendPort = initialData.sendPort;
    _slaveReciveStream = _slaveRecivePort.asBroadcastStream();
    _masterSendPort!.send(_slaveRecivePort.sendPort);
  }

  void send(i.Message message) {
    message.channelName = channelName!;
    _masterSendPort!.send(message);
  }

  Stream<T> reciveStream<T extends i.Message>() => _slaveReciveStream
      .where((message) => message != null)
      .where((message) => message is i.Message)
      .where((message) => message is T)
      .map<T>((event) => event as T);

  SendPort? _masterSendPort;
  ReceivePort _slaveRecivePort = ReceivePort();
  late Stream _slaveReciveStream;
}
