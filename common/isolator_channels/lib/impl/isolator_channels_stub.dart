import 'package:isolator_channels/isolator_channels.dart' as i;
import 'dart:async';

// class InitialData implements i.InitialData {
//   Sink<i.Message?> sendSink;
//   Stream<i.Message?> reciveStream;
//   String channelName;
//   InitialData(
//       {required this.channelName,
//       required this.sendSink,
//       required this.reciveStream});
// }

class ChannelOwner implements i.IChannelOwner {
  void send(i.Message message, {required String channelName}) {
    throw UnimplementedError();
  }

  Stream<T> reciveStream<T extends i.Message>({required String channelName}) {
    throw UnimplementedError();
  }

  Future<dynamic> createChannel(
      {required String channelName, required i.EntryPoint entryPoint}) async {
    throw UnimplementedError();
  }
}

abstract class Channel implements i.IChannel {
  void init(i.InitialData data) {
    throw UnimplementedError();
  }

  void send(i.Message message) {
    throw UnimplementedError();
  }

  Stream<T> reciveStream<T extends i.Message>() {
    throw UnimplementedError();
  }

  void main();
}
