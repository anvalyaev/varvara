library isolator_channels;

export 'package:isolator_channels/impl/isolator_channels_stub.dart'
    if (dart.library.io) 'package:isolator_channels/impl/isolator_channels_vm.dart'
    if (dart.library.html) 'package:isolator_channels/impl/isolator_channels_js.dart';

mixin Message {
  late String channelName;
}

abstract class InitialData {}

typedef EntryPoint = void Function(IChannel message);

abstract class IChannelOwner {
  void send(Message message, {required String channelName});

  Stream<T> reciveStream<T extends Message>({required String channelName});

  Future<dynamic> createChannel(
      {required String channelName, required EntryPoint entryPoint});
}

abstract class IChannel {
  void init(InitialData data);
  void send(Message message);

  Stream<T> reciveStream<T extends Message>();
}
