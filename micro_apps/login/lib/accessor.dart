import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/data/repositories/use_cases.dart';
import 'package:login/data/repositories/index.dart';

abstract class ILoginAccessor extends IAccessor {
  IUsersRepository get repositoryUsers;
}

class LoginAccessor extends ILoginAccessor {
  final UseCaseRepository _useCaseRepository = UseCaseRepository();
  UsersRepositoryIsar? _usersRepository;

  @override
  IUseCaseRepository get repositoryUseCase => _useCaseRepository;

  @override
  IUsersRepository get repositoryUsers => _usersRepository!;

  void initRepositoryUsers(UsersRepositoryIsar usersRepositoryIsar) {
    if (_usersRepository != null) {
      throw Exception('Tryed reinitialize _usersRepository');
    }
    _usersRepository ??= usersRepositoryIsar;
  }
}
