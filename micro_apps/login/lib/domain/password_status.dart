import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';
import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:login/accessor.dart';

class PasswordStatusOut extends UseCaseResult with EquatableMixin {
  final bool havePassword;

  PasswordStatusOut(super.ucid, {required this.havePassword});

  @override
  String toString() => 'AuthorizationEntity(success: $havePassword)';

  @override
  List<Object?> get props => [havePassword];
}

class AuthorizationStatusError extends UseCaseError {
  AuthorizationStatusError(super.ucid);
}

class PasswordStatus extends UseCaseBase<void, PasswordStatusOut> {
  PasswordStatusOut? passwordStatusEntity;
  PasswordStatus() : super(dataIn: null);
  VoidCallback? repositoryListener;

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    accessor as LoginAccessor;
    if (repositoryListener != null) {
      accessor.repositoryUsers.removeListener(repositoryListener!);
    }

    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<PasswordStatusOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as LoginAccessor;

    onResult(_getPasswordStatus(accessor));

    repositoryListener = () {
      onResult(_getPasswordStatus(accessor));
    };
    accessor.repositoryUsers.addListener(repositoryListener!);
  }

  PasswordStatusOut _getPasswordStatus(accessor) {
    final bool havePassword =
        accessor.repositoryUsers.getMyUser()?.passwordHash != null;
    return PasswordStatusOut(this.id, havePassword: havePassword);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }

  @override
  int get orderGroupIndex => 0;
}
