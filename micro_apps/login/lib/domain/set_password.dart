import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:login/accessor.dart';
import 'package:uuid/uuid.dart';

import '../data/repositories/collections/index.dart';

class SetPasswordOut extends UseCaseResult {
  final bool success;

  SetPasswordOut(super.ucid, {required this.success});

  @override
  String toString() => 'SetPasswordOut(success: $success)';
}

class SetPasswordIn {
  final String newPassword;
  final String newPasswordConfirmation;
  final String? oldPassword;

  SetPasswordIn(
      {required this.newPassword,
      required this.newPasswordConfirmation,
      required this.oldPassword});

  @override
  String toString() =>
      'SetPasswordIn(newPassword: $newPassword, oldPassword: $oldPassword)';
}

class SetPassword extends UseCaseBase<SetPasswordIn, SetPasswordOut> {
  SetPassword(SetPasswordIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<SetPasswordOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as LoginAccessor;
    if (dataIn?.newPassword != dataIn?.newPasswordConfirmation) {
      onResult(SetPasswordOut(id, success: false));
      onComplete(id);
      return;
    }

    User myUser =
        accessor.repositoryUsers.getMyUser() ?? User(id: const Uuid().v4());

    final String newPasswordHash =
        base64.encode(sha256.convert(utf8.encode(dataIn!.newPassword)).bytes);

    String? oldPasswordHash;
    if (dataIn!.oldPassword != null) {
      oldPasswordHash = base64
          .encode(sha256.convert(utf8.encode(dataIn!.oldPassword!)).bytes);
    }

    if (oldPasswordHash != myUser.passwordHash) {
      onResult(SetPasswordOut(id, success: false));
      onComplete(id);
      return;
    } else {
      myUser = myUser.copyWith(passwordHash: newPasswordHash);
      accessor.repositoryUsers.setMyUser(myUser);
      onResult(SetPasswordOut(id, success: true));
      onComplete(id);
      return;
    }
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
