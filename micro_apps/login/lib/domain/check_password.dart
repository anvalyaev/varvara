import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:login/accessor.dart';

class CheckPasswordOut extends UseCaseResult {
  final bool success;
  final String? passwordHash;

  CheckPasswordOut(super.ucid, {required this.success, this.passwordHash});

  @override
  String toString() =>
      'CheckPasswordOut(success: $success, passwordHash: $passwordHash)';
}

class CheckPasswordIn {
  final String password;

  CheckPasswordIn({required this.password});

  @override
  String toString() => 'CheckPasswordIn(success: $password)';
}

class CheckPassword extends UseCaseBase<CheckPasswordIn, CheckPasswordOut> {
  CheckPassword(CheckPasswordIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<CheckPasswordOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as LoginAccessor;

    final passwordHashMy = accessor.repositoryUsers.getMyUser()?.passwordHash;
    if (passwordHashMy == null) {
      onResult(CheckPasswordOut(id, success: false));
      onComplete(id);
      return;
    }

    String passwordHashIn =
        base64.encode(sha256.convert(utf8.encode(dataIn!.password)).bytes);

    onResult(CheckPasswordOut(id,
        success: passwordHashMy == passwordHashIn,
        passwordHash: passwordHashIn));
    onComplete(id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
