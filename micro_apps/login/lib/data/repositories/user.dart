import 'package:isar_repository/isar_repository.dart';
import 'package:isolated_clean_architecture/data/repositories/repository_base.dart';
import 'package:isar/isar.dart';

import 'collections/index.dart';

abstract mixin class IUsersRepository implements RepositoryBase {
  User? getMyUser();
  void setMyUser(User myUser);
}

class UsersRepositoryIsar extends IsarRepositoryBase with IUsersRepository {
  UsersRepositoryIsar({required super.dir})
      : super(name: 'UserRepositoryIsar', schemas: [UserSchema]);

  @override
  User? getMyUser() {
    List<User> myUsers = isar.users.where().myEqualTo(true).findAll();
    return myUsers.isEmpty ? null : myUsers.first;
  }

  @override
  void setMyUser(User myUser) {
    if (!myUser.my) {
      throw Exception('Not my user setted');
    }
    isar.write((isar) {
      isar.users.where().myEqualTo(true).deleteAll();
      isar.users.put(myUser);
    });

    notifyListeners();
  }
}
