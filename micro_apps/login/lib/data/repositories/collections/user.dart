import 'package:isar/isar.dart';

part 'user.g.dart';

@collection
class User {
  final String id;
  final bool my;
  final String? firstName;
  final String? lastName;
  final String? middleName;
  final String? passwordHash;
  User({
    required this.id,
    this.my = true,
    this.firstName,
    this.lastName,
    this.middleName,
    this.passwordHash,
  });

  User copyWith({
    String? firstName,
    String? lastName,
    String? middleName,
    String? passwordHash,
  }) {
    return User(
      id: id,
      my: my,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      middleName: middleName ?? this.middleName,
      passwordHash: passwordHash ?? this.passwordHash,
    );
  }
}
