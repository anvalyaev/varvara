import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login/presentation/bloc/login_cubit.dart';
import 'package:login/presentation/bloc/login_state.dart';
import 'package:pinput/pinput.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final LoginCubit cubit = LoginCubit();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Authorization'),
      ),
      body: Center(
          child: BlocBuilder<LoginCubit, LoginState>(
        bloc: cubit,
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Pinput(
                useNativeKeyboard: true,
                onCompleted: cubit.onLogin,
                forceErrorState: state.errors.isNotEmpty,
                errorText: 'Incorrect password, please try again',
              ),
            ],
          );
        },
      )),
    );
  }

  @override
  void dispose() {
    super.dispose();
    cubit.close();
  }
}
