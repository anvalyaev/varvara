import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login/presentation/bloc/index.dart';
import 'package:pinput/pinput.dart';

class SetPasswordScreen extends StatefulWidget {
  const SetPasswordScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _SetPasswordState createState() => _SetPasswordState();
}

class _SetPasswordState extends State<SetPasswordScreen> {
  final SetPasswordCubit cubit = SetPasswordCubit();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Enter your PIN, please...'),
      ),
      body: Center(
          child: BlocBuilder<SetPasswordCubit, SetPasswordState>(
        bloc: cubit,
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text('PIN'),
              Padding(
                key: const ValueKey('PIN'),
                padding: const EdgeInsets.all(8.0),
                child: Pinput(
                  useNativeKeyboard: true,
                  autofocus: true,
                  focusNode: cubit.focusNodePassword,
                  onCompleted: cubit.onSetPassword,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Text('Confirmation'),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Pinput(
                  key: const ValueKey('Confirmation'),
                  useNativeKeyboard: true,
                  autofocus: false,
                  focusNode: cubit.focusNodePasswordConfirmation,
                  onCompleted: cubit.onSetPasswordConfirmation,
                  forceErrorState: state.errors.isNotEmpty,
                  errorText: 'PINs is not identical',
                ),
              ),
            ],
          );
        },
      )),
    );
  }

  @override
  void dispose() {
    super.dispose();
    cubit.close();
  }
}
