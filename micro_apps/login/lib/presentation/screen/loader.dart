import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login/presentation/bloc/index.dart';

class LoaderScreen extends StatefulWidget {
  const LoaderScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _LoaderScreenState createState() => _LoaderScreenState();
}

class _LoaderScreenState extends State<LoaderScreen> {
  final LoaderCubit cubit = LoaderCubit();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: BlocBuilder<LoaderCubit, LoaderState>(
        bloc: cubit,
        builder: (context, state) {
          return const Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[CircularProgressIndicator()],
          );
        },
      )),
    );
  }

  @override
  void initState() {
    cubit.onInit();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    cubit.close();
  }
}
