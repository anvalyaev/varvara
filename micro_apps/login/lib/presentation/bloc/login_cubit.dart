import 'package:core/core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:login/domain/check_password.dart';
import 'package:login/presentation/bloc/login_state.dart';

class LoginCubit extends Cubit<LoginState> with Presenter {
  LoginCubit() : super(LoginState());

  Future<void> onLogin(String password) async {
    final res = await execute<CheckPassword, CheckPasswordIn, CheckPasswordOut>(
        CheckPassword(CheckPasswordIn(password: password)));
    print(res);
    if (!(res?.success ?? false)) {
      emit(LoginState(errors: {LoginError.incorrectPassword}));
    } else {
      emit(LoginState());
      MicroAppsEventBus().emit(Login());
    }
  }

  @override
  Future<void> close() async {
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/login';
}
