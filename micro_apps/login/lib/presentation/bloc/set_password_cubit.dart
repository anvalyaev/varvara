import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:login/domain/set_password.dart';

import 'set_password_state.dart';

class SetPasswordCubit extends Cubit<SetPasswordState> with Presenter {
  SetPasswordCubit() : super(SetPasswordState());

  final FocusNode focusNodePassword = FocusNode();
  final FocusNode focusNodePasswordConfirmation = FocusNode();
  final FocusScopeNode focusScopeNode = FocusScopeNode();
  String? _password;
  String? _passwordConfirmation;

  Future<void> onSetPassword(String password) async {
    emit(SetPasswordState());
    _password = password;
    // focusNodePassword.nextFocus();
    focusNodePassword.unfocus();
    focusNodePasswordConfirmation.requestFocus();
    print(
        'focusNodePasswordConfirmation.hasFocus: ${focusNodePasswordConfirmation.hasFocus}, ${focusNodePassword.hasFocus}, ${focusScopeNode.hasFocus}');
    print(
        'focusNodePasswordConfirmation.hasFocus: ${focusNodePasswordConfirmation.canRequestFocus}, ${focusNodePassword.canRequestFocus}, ${focusScopeNode.canRequestFocus}');
  }

  Future<void> onSetPasswordConfirmation(String password) async {
    if (_password == null) {
      focusNodePassword.requestFocus();
      return;
    }
    _passwordConfirmation = password;
    final res = await execute<SetPassword, SetPasswordIn, SetPasswordOut>(
        SetPassword(SetPasswordIn(
            newPassword: _password!,
            newPasswordConfirmation: _passwordConfirmation!,
            oldPassword: null)));
    if (res?.success ?? false) {
      GetIt.instance.get<GoRouter>().replace('/login');
    } else {
      emit(SetPasswordState(errors: {SetPasswordError.pinIsNotIdentical}));
    }
  }

  @override
  Future<void> close() async {
    focusNodePassword.dispose();
    focusNodePasswordConfirmation.dispose();
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/login';
}
