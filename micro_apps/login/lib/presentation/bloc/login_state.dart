enum LoginError { incorrectPassword }

class LoginState {
  final Set<LoginError> errors;
  LoginState({this.errors = const {}});
}
