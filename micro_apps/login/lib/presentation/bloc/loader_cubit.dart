import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:login/domain/password_status.dart';
import 'package:login/presentation/bloc/index.dart';

class LoaderCubit extends Cubit<LoaderState> with Presenter {
  LoaderCubit() : super(LoaderState());

  void onInit() async {
    final res =
        subscribe<PasswordStatus, void, PasswordStatusOut>(PasswordStatus());

    res.listen((event) {
      if (event.havePassword) {
        GetIt.instance.get<GoRouter>().replace('/login');
      } else {
        GetIt.instance.get<GoRouter>().replace('/set_password');
      }
    });
  }

  @override
  Future<void> close() async {
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/login';
}
