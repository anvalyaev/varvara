library login;

import 'dart:async';
import 'dart:ui';

import 'package:core/core.dart' as core;
import 'package:core/core.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:isolated_clean_architecture/isolated_clean_architecture.dart';
import 'package:login/accessor.dart';
import 'package:login/domain/initialize.dart';
import 'package:login/presentation/screen/index.dart' as screens;

class Login with core.MicroApp, Presenter {
  @override
  Future<void> init() async {
    final Completer completer = Completer();
    final controller = MessageController(
        name: microAppName,
        createMessageManager: (channel) {
          MessageManager<LoginAccessor>(
              channel: channel, accessor: LoginAccessor());
        },
        errorListener: ({exception, stackTrace}) async {},
        onInitiated: () async {
          try {
            await execute(Initialize(RootIsolateToken.instance!));
          } catch (e) {
            completer.completeError(e);
            return;
          }
          completer.complete();
        });

    GetIt.instance.registerSingleton<MessageController>(controller,
        instanceName: microAppName, signalsReady: true);

    core.MicroAppsEventBus().on<core.NeedRefreshToken>((event) async {
      logger.i('offline auth');
    });

    core.MicroAppsEventBus().on<core.Logout>((event) async {
      core.MicroAppsEventBus()
          .emit(core.TokenChanged(baseUrl: '', token: null));
      router.replace('/login');
    });

    return completer.future;
  }

  @override
  String get microAppName => '/login';

  @override
  Widget? microAppWidget() => const screens.LoaderScreen();

  @override
  @override
  List<RouteBase> get routes => [
        GoRoute(
            path: '/',
            builder: (BuildContext context, GoRouterState state) {
              return const screens.LoaderScreen();
            },
            routes: [
              GoRoute(
                path: 'login',
                builder: (BuildContext context, GoRouterState state) {
                  return const screens.LoginScreen();
                },
              ),
              GoRoute(
                path: 'set_password',
                builder: (BuildContext context, GoRouterState state) {
                  return const screens.SetPasswordScreen();
                },
              )
            ])
      ];
}
