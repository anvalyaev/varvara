library login;

import 'dart:async';
import 'dart:ui';

import 'package:core/core.dart' as core;
import 'package:flutter/src/widgets/framework.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:isolated_clean_architecture/isolated_clean_architecture.dart';
import 'package:keycloak_auth/accessor.dart';
import 'package:keycloak_auth/domain/index.dart';
import 'package:keycloak_auth/presentation/screen/index.dart' as screens;

class KeycloakAuth with core.MicroApp, Presenter {
  final Uri authUri;
  KeycloakAuth({
    required this.authUri,
  });
  @override
  Future<void> init() async {
    final Completer completer = Completer();
    final controller = MessageController(
        name: microAppName,
        createMessageManager: (channel) {
          MessageManager<KeycloakAuthAccessor>(
              channel: channel, accessor: KeycloakAuthAccessor());
        },
        errorListener: ({exception, stackTrace}) async {},
        onInitiated: () async {
          try {
            final applicationSettings =
                GetIt.instance.get<core.IApplicationSettings>();
            subscribe<Initialize, InitializeIn, InitializeOut>(Initialize(
                    InitializeIn(
                        applicationSettings:
                            GetIt.instance.get<core.IApplicationSettings>(),
                        rootIsolateToken: RootIsolateToken.instance!,
                        baseUrl: Uri.tryParse(applicationSettings.baseUrl)!)))
                .listen((event) {
              if (event.needRefreshToken) {
                core.MicroAppsEventBus().emit(core.NeedRefreshToken());
              }
            });
          } catch (e) {
            completer.completeError(e);
            return;
          }
          completer.complete();
        });

    GetIt.instance.registerSingleton<MessageController>(controller,
        instanceName: microAppName, signalsReady: true);

    core.MicroAppsEventBus().on<core.NeedRefreshToken>((event) async {
      try {
        final res = await execute<TryRefreshToken, TryRefreshTokenIn,
            TryRefreshTokenOut>(TryRefreshToken(TryRefreshTokenIn()));
        final applicationSettings =
            GetIt.instance.get<core.IApplicationSettings>();
        core.MicroAppsEventBus().emit(core.TokenChanged(
            baseUrl: applicationSettings.baseUrl, token: res!.token));
      } on TryRefreshTokenError {
        core.MicroAppsEventBus()
            .emit(core.TokenChanged(baseUrl: '', token: null));
        router.replace('/authorization');
      }
    });

    core.MicroAppsEventBus().on<core.Logout>((event) async {
      await execute(Logout(LogoutIn()));
      core.MicroAppsEventBus()
          .emit(core.TokenChanged(baseUrl: '', token: null));
      router.replace('/authorization');
    });

    core.MicroAppsEventBus().on<core.TokenChanged>((event) async {
      await execute<SetToken, SetTokenIn, SetTokenOut>(SetToken(SetTokenIn(
          baseUrl: Uri.tryParse(event.baseUrl)!, token: event.token)));
    });

    return completer.future;
  }

  @override
  String get microAppName => '/keycloac_auth';

  @override
  Widget? microAppWidget() => const screens.KeycloakAuthScreen();

  @override
  @override
  List<RouteBase> get routes => [
        GoRoute(
            path: '/',
            builder: (BuildContext context, GoRouterState state) {
              return const screens.LoaderScreen();
            },
            routes: [
              GoRoute(
                path: 'authorization',
                builder: (BuildContext context, GoRouterState state) {
                  return const screens.KeycloakAuthScreen();
                },
              ),
            ])
      ];
}
