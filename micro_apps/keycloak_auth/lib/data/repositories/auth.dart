import 'package:isar_repository/isar_repository.dart';
import 'package:isolated_clean_architecture/data/repositories/repository_base.dart';

import 'collections/index.dart';

abstract mixin class IAuthRepository implements RepositoryBase {
  static const String authId = 'auth';
  Auth? getAuth();
  void setAuth(Auth auth);
}

class AuthRepositoryIsar extends IsarRepositoryBase with IAuthRepository {
  AuthRepositoryIsar({required super.dir})
      : super(name: 'AuthRepositoryIsar', schemas: [AuthSchema]);

  @override
  Auth? getAuth() {
    Auth? auth = isar.auths.get(IAuthRepository.authId);
    return auth;
  }

  @override
  void setAuth(Auth auth) {
    isar.write((isar) {
      isar.auths.put(auth);
    });
    notifyListeners();
  }
}
