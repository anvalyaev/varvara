import 'package:isar/isar.dart';

part 'auth.g.dart';

@collection
class Auth {
  final String id;
  final String? token;
  final String? refreshToken;

  Auth({
    required this.id,
    required this.token,
    required this.refreshToken,
  });

  Auth copyWith({
    String? token,
    String? refreshToken,
  }) {
    return Auth(
      id: id,
      token: token ?? this.token,
      refreshToken: refreshToken ?? this.refreshToken,
    );
  }
}
