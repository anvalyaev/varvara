import 'package:keycloak_auth/data/network_requests/dto/index.dart';
import 'package:rest_network/rest_request.dart';

class RefreshToken extends NetworkRequest<TokenDto> {
  final String refreshToken;
  RefreshToken({
    required this.refreshToken,
  });

  @override
  Map<String, String> get data => {
        'grant_type': 'refresh_token',
        'client_id': 'admin-cli',
        'refresh_token': refreshToken,
      };

  @override
  TokenDto onAnswer(Response answer) {
    return TokenDto.fromJson(answer.data);
  }

  @override
  String get path => '/token';

  @override
  TypeRequest get typeRequest => TypeRequest.postRequest;

  @override
  bool get authorized => false;

  @override
  bool get jsonEncode => false;
}
