import 'package:keycloak_auth/data/network_requests/dto/index.dart';
import 'package:rest_network/rest_request.dart';

class GetToken extends NetworkRequest<TokenDto> {
  final String username;
  final String password;
  GetToken({
    required this.username,
    required this.password,
  });

  @override
  Map<String, String> get data => {
        'grant_type': 'password',
        'client_id': 'admin-cli',
        'username': username,
        'password': password
      };

  @override
  TokenDto onAnswer(Response answer) {
    return TokenDto.fromJson(answer.data);
  }

  @override
  String get path => '/token';

  @override
  TypeRequest get typeRequest => TypeRequest.postRequest;

  @override
  bool get authorized => false;

  @override
  bool get jsonEncode => false;
}
