// ignore_for_file: non_constant_identifier_names

import 'package:json_annotation/json_annotation.dart';

part 'token_dto.g.dart';

@JsonSerializable()
class TokenDto {
  /// The generated code assumes these values exist in JSON.
  final String access_token, refresh_token;

  TokenDto({required this.access_token, required this.refresh_token});

  /// Connect the generated [_$PersonFromJson] function to the `fromJson`
  /// factory.
  factory TokenDto.fromJson(Map<String, dynamic> json) =>
      _$TokenDtoFromJson(json);

  /// Connect the generated [_$PersonToJson] function to the `toJson` method.
  Map<String, dynamic> toJson() => _$TokenDtoToJson(this);
}
