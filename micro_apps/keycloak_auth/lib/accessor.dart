import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/data/repositories/use_cases.dart';
import 'package:keycloak_auth/data/repositories/index.dart';
import 'package:rest_network/rest_network.dart';

abstract class IKeycloakAuthAccessor extends IAccessor {
  IAuthRepository get repositoryAuth;
  IRestNetwork get serviceRestNetwork;
}

class KeycloakAuthAccessor extends IKeycloakAuthAccessor {
  final UseCaseRepository _useCaseRepository = UseCaseRepository();
  AuthRepositoryIsar? _authRepository;
  RestNetwork? _serviceRestNetwork;

  @override
  IUseCaseRepository get repositoryUseCase => _useCaseRepository;

  @override
  IAuthRepository get repositoryAuth => _authRepository!;

  @override
  IRestNetwork get serviceRestNetwork => _serviceRestNetwork!;

  void initRepositoryAuth(AuthRepositoryIsar authRepositoryIsar) {
    if (_authRepository != null) {
      throw Exception('Tryed reinitialize _authRepository');
    }
    _authRepository ??= authRepositoryIsar;
  }

  void initServiceRestNetwork(RestNetwork serviceRestNetwork) {
    if (_serviceRestNetwork != null) {
      throw Exception('Tryed reinitialize _serviceRestNetwork');
    }
    _serviceRestNetwork ??= serviceRestNetwork;
  }
}
