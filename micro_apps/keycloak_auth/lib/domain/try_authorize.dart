import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:keycloak_auth/accessor.dart';
import 'package:keycloak_auth/data/network_requests/dto/index.dart';
import 'package:keycloak_auth/data/network_requests/get_token.dart';
import 'package:keycloak_auth/data/repositories/collections/index.dart';
import 'package:keycloak_auth/data/repositories/index.dart';
import 'package:rest_network/rest_network.dart';

class TryAuthorizeOut extends UseCaseResult {
  final bool success;
  final String token;

  TryAuthorizeOut(super.ucid, {required this.success, required this.token});

  @override
  String toString() => 'TryAuthorizeOut(success: $success)';
}

class TryAuthorizeIn {
  final String username;
  final String password;

  TryAuthorizeIn({required this.username, required this.password});

  @override
  String toString() =>
      'TryAuthorizeIn(username: $username, password: $password)';
}

class AuthorizeError extends UseCaseError {
  AuthorizeError(super.ucid);
}

class NoConnectionError extends UseCaseError {
  NoConnectionError(super.ucid);
}

class TryAuthorize extends UseCaseBase<TryAuthorizeIn, TryAuthorizeOut> {
  TryAuthorize(TryAuthorizeIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<TryAuthorizeOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as IKeycloakAuthAccessor;

    try {
      final tokenDto = await accessor.serviceRestNetwork.sendRequest<TokenDto>(
          GetToken(username: dataIn!.username, password: dataIn!.password));

      accessor.repositoryAuth.setAuth(Auth(
          id: IAuthRepository.authId,
          token: tokenDto.access_token,
          refreshToken: tokenDto.refresh_token));
      onResult(
          TryAuthorizeOut(id, success: true, token: tokenDto.access_token));
    } on NetworkResponseError {
      await onError(AuthorizeError(id));
    } catch (e) {
      await onError(NoConnectionError(id));
    }
    onComplete(id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
