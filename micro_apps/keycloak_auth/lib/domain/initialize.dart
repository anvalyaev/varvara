import 'dart:io';

import 'package:core/core.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:keycloak_auth/accessor.dart';
import 'package:keycloak_auth/data/repositories/auth.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rest_network/rest_network.dart';

import 'order_group_indexes.dart';

class InitializeOut extends UseCaseResult {
  final bool success;
  final bool needRefreshToken;

  InitializeOut(super.ucid,
      {required this.success, this.needRefreshToken = false});

  @override
  String toString() => 'InitializeOut(success: $success)';
}

class InitializeIn {
  final RootIsolateToken rootIsolateToken;
  final Uri baseUrl;
  final IApplicationSettings applicationSettings;

  InitializeIn(
      {required this.rootIsolateToken,
      required this.baseUrl,
      required this.applicationSettings});

  @override
  String toString() =>
      'InitializeIn(rootIsolateToken: $rootIsolateToken, baseUrl: $baseUrl)';
}

class InitializeError extends UseCaseError {
  InitializeError(super.ucid);
}

class Initialize extends UseCaseBase<InitializeIn, InitializeOut> {
  Initialize(InitializeIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<InitializeOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as KeycloakAuthAccessor;
    BackgroundIsolateBinaryMessenger.ensureInitialized(
        dataIn!.rootIsolateToken);
    try {
      GetIt.instance
          .registerSingleton<IApplicationSettings>(dataIn!.applicationSettings);
      Directory dir = await getApplicationSupportDirectory();
      accessor.initRepositoryAuth(AuthRepositoryIsar(dir: dir));

      accessor.initServiceRestNetwork(RestNetwork(
          onNeedUpdateToken: () {
            onResult(
                InitializeOut(this.id, success: true, needRefreshToken: true));
          },
          baseUrl: dataIn!.baseUrl.toString()));
    } catch (e) {
      onError(InitializeError(this.id));
    }
    onResult(InitializeOut(this.id, success: true));
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }

  @override
  int get orderGroupIndex => OrderGroupIndexes.init;
}
