import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:keycloak_auth/accessor.dart';
import 'package:keycloak_auth/data/repositories/collections/index.dart';
import 'package:keycloak_auth/data/repositories/index.dart';

class LogoutOut extends UseCaseResult {
  LogoutOut(
    super.ucid,
  );

  @override
  String toString() => 'LogoutOut()';
}

class LogoutIn {
  LogoutIn();
}

class LogoutError extends UseCaseError {
  LogoutError(
    super.ucid,
  );
}

class Logout extends UseCaseBase<LogoutIn, LogoutOut> {
  Logout(LogoutIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<LogoutOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as KeycloakAuthAccessor;

    final auth = accessor.repositoryAuth.getAuth();
    if (auth?.refreshToken == null) {
      await onError(LogoutError(id));
      onComplete(id);
      return;
    }

    accessor.repositoryAuth.setAuth(
        Auth(id: IAuthRepository.authId, token: null, refreshToken: null));
    onResult(LogoutOut(
      id,
    ));
    onComplete(id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
