import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:keycloak_auth/accessor.dart';
import 'package:keycloak_auth/data/network_requests/dto/index.dart';
import 'package:keycloak_auth/data/network_requests/refresh_token.dart';
import 'package:keycloak_auth/data/repositories/collections/index.dart';
import 'package:keycloak_auth/data/repositories/index.dart';

class TryRefreshTokenOut extends UseCaseResult {
  final String token;

  TryRefreshTokenOut(super.ucid, {required this.token});

  @override
  String toString() => 'TryRefreshTokenOut(token: $token)';
}

class TryRefreshTokenIn {
  TryRefreshTokenIn();
}

class TryRefreshTokenError extends UseCaseError {
  TryRefreshTokenError(
    super.ucid,
  );
}

class TryRefreshToken
    extends UseCaseBase<TryRefreshTokenIn, TryRefreshTokenOut> {
  TryRefreshToken(TryRefreshTokenIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<TryRefreshTokenOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as KeycloakAuthAccessor;

    final auth = accessor.repositoryAuth.getAuth();
    if (auth?.refreshToken == null) {
      await onError(TryRefreshTokenError(id));
      onComplete(id);
      return;
    }

    final tokenDto = await accessor.serviceRestNetwork
        .sendRequest<TokenDto>(RefreshToken(refreshToken: auth!.refreshToken!));
    accessor.repositoryAuth.setAuth(Auth(
        id: IAuthRepository.authId,
        token: tokenDto.access_token,
        refreshToken: tokenDto.refresh_token));
    onResult(TryRefreshTokenOut(id, token: tokenDto.access_token));
    onComplete(id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
