import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:keycloak_auth/accessor.dart';

class GetAuthStatusOut extends UseCaseResult {
  final bool authorized;

  GetAuthStatusOut(super.ucid, {required this.authorized});

  @override
  String toString() => 'GetAuthStatusOut(authorized: $authorized)';
}

class GetAuthStatusIn {
  GetAuthStatusIn();
}

class GetAuthStatusError extends UseCaseError {
  GetAuthStatusError(super.ucid);
}

class GetAuthStatus extends UseCaseBase<GetAuthStatusIn, GetAuthStatusOut> {
  GetAuthStatus(GetAuthStatusIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<GetAuthStatusOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as IKeycloakAuthAccessor;
    try {
      final auth = accessor.repositoryAuth.getAuth();
      if (auth == null) {
        onResult(GetAuthStatusOut(this.id, authorized: false));
      } else if (auth.token == null) {
        onResult(GetAuthStatusOut(this.id, authorized: false));
      } else {
        onResult(GetAuthStatusOut(this.id, authorized: true));
      }
    } catch (e) {
      onError(GetAuthStatusError(this.id));
    }
    onComplete(this.id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
