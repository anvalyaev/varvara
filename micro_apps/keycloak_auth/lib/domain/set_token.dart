import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:keycloak_auth/accessor.dart';

import 'order_group_indexes.dart';

class SetTokenOut extends UseCaseResult {
  final bool success;
  final bool needRefreshToken;

  SetTokenOut(super.ucid,
      {required this.success, this.needRefreshToken = false});

  @override
  String toString() => 'SetTokenOut(success: $success)';
}

class SetTokenIn {
  final String? token;
  final Uri baseUrl;

  SetTokenIn({required this.baseUrl, required this.token});

  @override
  String toString() => 'SetTokenIn(token: $token, baseUrl: $baseUrl)';
}

class SetTokenError extends UseCaseError {
  SetTokenError(super.ucid);
}

class SetToken extends UseCaseBase<SetTokenIn, SetTokenOut> {
  SetToken(SetTokenIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<SetTokenOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as IKeycloakAuthAccessor;
    try {
      accessor.serviceRestNetwork
          .updateBaseUrl(baseUrl: dataIn!.baseUrl.toString());
      accessor.serviceRestNetwork.updateToken(token: dataIn!.token);
    } catch (e) {
      onError(SetTokenError(this.id));
    }
    onResult(SetTokenOut(this.id, success: true));
    onComplete(this.id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }

  @override
  int get orderGroupIndex => OrderGroupIndexes.init;
}
