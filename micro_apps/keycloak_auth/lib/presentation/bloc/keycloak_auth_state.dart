enum KeycloakAuthError { noError, wrongUsernameOrPassword, noAnswer }

class KeycloakAuthState {
  KeycloakAuthError error;
  bool busy;
  KeycloakAuthState({
    required this.error,
    required this.busy,
  });

  KeycloakAuthState copyWith({
    KeycloakAuthError? error,
    bool? busy,
  }) {
    return KeycloakAuthState(
      error: error ?? this.error,
      busy: busy ?? this.busy,
    );
  }
}
