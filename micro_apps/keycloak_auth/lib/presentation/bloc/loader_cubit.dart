import 'package:core/core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:keycloak_auth/domain/index.dart';
import 'package:keycloak_auth/presentation/bloc/index.dart';

class LoaderCubit extends Cubit<LoaderState> with Presenter {
  LoaderCubit() : super(LoaderState());

  void onInit() async {
    execute<GetAuthStatus, GetAuthStatusIn, GetAuthStatusOut>(
            GetAuthStatus(GetAuthStatusIn()))
        .then((res) async {
      if (res?.authorized ?? false) {
        MicroAppsEventBus().emit(Login());
      } else {
        // await Future.delayed(const Duration(seconds: 2));
        router.replace('/authorization');
      }
    });
  }

  @override
  Future<void> close() async {
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/keycloac_auth';
}
