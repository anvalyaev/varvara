import 'package:core/core.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:keycloak_auth/domain/try_authorize.dart';
import 'package:keycloak_auth/presentation/bloc/keycloak_auth_state.dart';

class KeycloakAuthCubit extends Cubit<KeycloakAuthState> with Presenter {
  KeycloakAuthCubit()
      : super(KeycloakAuthState(error: KeycloakAuthError.noError, busy: false));
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Future<void> onInit() async {}

  void onTryAuth() async {
    try {
      final username = usernameController.text;
      final passsword = passwordController.text;
      emit(state.copyWith(busy: true));
      final res = await execute<TryAuthorize, TryAuthorizeIn, TryAuthorizeOut>(
          TryAuthorize(
              TryAuthorizeIn(username: username, password: passsword)));
      final settings = GetIt.instance.get<IApplicationSettings>();
      MicroAppsEventBus()
          .emit(TokenChanged(baseUrl: settings.baseUrl, token: res!.token));
      MicroAppsEventBus().emit(Login());
    } on AuthorizeError {
      emit(KeycloakAuthState(
          error: KeycloakAuthError.wrongUsernameOrPassword, busy: false));
      MicroAppsEventBus().emit(TokenChanged(baseUrl: '', token: null));
    } catch (e) {
      emit(KeycloakAuthState(error: KeycloakAuthError.noAnswer, busy: false));
      MicroAppsEventBus().emit(TokenChanged(baseUrl: '', token: null));
    }
    emit(state.copyWith(busy: false));
  }

  @override
  Future<void> close() async {
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/keycloac_auth';
}
