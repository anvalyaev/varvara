import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keycloak_auth/presentation/bloc/keycloak_auth_cubit.dart';
import 'package:keycloak_auth/presentation/bloc/keycloak_auth_state.dart';

class KeycloakAuthScreen extends StatefulWidget {
  const KeycloakAuthScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _KeycloakAuthScreenState createState() => _KeycloakAuthScreenState();
}

class _KeycloakAuthScreenState extends State<KeycloakAuthScreen> {
  late final KeycloakAuthCubit cubit = KeycloakAuthCubit();

  @override
  void initState() {
    cubit.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Authorization'),
      ),
      body: Center(
          child: BlocBuilder<KeycloakAuthCubit, KeycloakAuthState>(
        bloc: cubit,
        builder: (context, state) {
          return Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                child: TextField(
                  controller: cubit.usernameController,
                  autofocus: true,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Username',
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                child: TextField(
                  controller: cubit.passwordController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Password',
                  ),
                ),
              ),
              if (state.error == KeycloakAuthError.wrongUsernameOrPassword)
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text('Wrong username or password. Please try again'),
                ),
              if (state.error == KeycloakAuthError.noAnswer)
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                      'No response from the server. Please check your connection and try again later.'),
                )
            ],
          );
        },
      )),
      floatingActionButton: BlocBuilder<KeycloakAuthCubit, KeycloakAuthState>(
          bloc: cubit,
          builder: (context, state) {
            return FloatingActionButton(
              onPressed: state.busy ? null : cubit.onTryAuth,
              child: state.busy
                  ? const SizedBox(
                      height: 24,
                      width: 24,
                      child: FittedBox(
                          child: CircularProgressIndicator(
                        color: Colors.white,
                      )))
                  : const Icon(Icons.login),
            );
          }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    cubit.close();
  }
}
