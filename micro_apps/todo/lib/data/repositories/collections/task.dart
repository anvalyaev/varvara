import 'package:isar/isar.dart';

part 'task.g.dart';

@collection
class Task {
  final String id;
  final bool done;
  final String title;
  final String? subtitle;
  final DateTime created;

  const Task(
      {required this.id,
      required this.done,
      required this.title,
      this.subtitle,
      required this.created});

  Task copyWith({
    bool? done,
    String? title,
    String? subtitle,
  }) {
    return Task(
      id: id,
      done: done ?? this.done,
      title: title ?? this.title,
      subtitle: subtitle ?? this.subtitle,
      created: created,
    );
  }
}
