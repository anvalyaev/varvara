import 'package:isar_repository/isar_repository.dart';
import 'package:isolated_clean_architecture/data/repositories/repository_base.dart';
import 'package:isar/isar.dart';

import 'collections/index.dart';

abstract mixin class ITasksRepository implements RepositoryBase {
  Task? getTask(String taskId);
  List<Task> getTasks({int offset = 0, int limit = 10});
  void removeTask(String taskId);
  void updateTask(Task task);
}

class TasksRepositoryIsar extends IsarRepositoryBase with ITasksRepository {
  TasksRepositoryIsar({required super.dir})
      : super(name: 'TasksRepositoryIsar', schemas: [TaskSchema]);

  @override
  List<Task> getTasks({int offset = 0, int limit = 10}) {
    return isar.tasks
        .where()
        .sortByCreatedDesc()
        .thenByTitleDesc()
        .findAll(limit: limit, offset: offset);
  }

  @override
  Task? getTask(String taskId) {
    final tasks = isar.tasks.where().idEqualTo(taskId).findAll();
    if (tasks.isEmpty) {
      return null;
    } else {
      return tasks.first;
    }
  }

  @override
  void removeTask(String taskId) {
    isar.write((isar) {
      isar.tasks.where().idEqualTo(taskId).deleteAll();
    });
    notifyListeners();
  }

  @override
  void updateTask(Task task) {
    isar.write((isar) {
      isar.tasks.put(task);
    });
    notifyListeners();
  }
}
