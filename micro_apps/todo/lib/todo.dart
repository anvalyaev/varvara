library login;

import 'dart:async';
import 'dart:ui';

import 'package:core/core.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:isolated_clean_architecture/isolated_clean_architecture.dart';
import 'package:todo/accessor.dart';
import 'package:todo/domain/index.dart';
import 'package:todo/presentation/screen/index.dart' as screens;
import 'package:uuid/uuid.dart';

class Todo with MicroApp, Presenter {
  @override
  Future<void> init() async {
    final Completer completer = Completer();

    final controller = MessageController(
        name: microAppName,
        createMessageManager: (channel) {
          MessageManager<TodoAccessor>(
              channel: channel, accessor: TodoAccessor());
        },
        errorListener: ({exception, stackTrace}) async {},
        onInitiated: () async {
          try {
            await execute(Initialize(RootIsolateToken.instance!));
          } catch (e) {
            completer.completeError(e);
            return;
          }
          completer.complete();
        });

    GetIt.instance.registerSingleton<MessageController>(controller,
        instanceName: microAppName, signalsReady: true);

    MicroAppsEventBus().on<Login>((event) {
      GetIt.instance.get<GoRouter>().replace('/todo_list');
    });

    return completer.future;
  }

  @override
  String get microAppName => '/todo';

  @override
  Widget? microAppWidget() => const screens.TodoList();

  @override
  @override
  List<RouteBase> get routes => [
        GoRoute(
            path: '/todo_list',
            builder: (BuildContext context, GoRouterState state) {
              return screens.TodoList(
                key: ValueKey(const Uuid().v4()),
              );
            },
            routes: [
              GoRoute(
                path: 'new_task',
                builder: (BuildContext context, GoRouterState state) {
                  return const screens.Task(
                    mode: screens.TaskMode.newTask,
                    title: 'New Task',
                  );
                },
              ),
              GoRoute(
                path: 'edit_task/:taskId',
                builder: (BuildContext context, GoRouterState state) {
                  String title = state.uri.queryParameters['title']!;
                  String subtitle = state.uri.queryParameters['subtitle']!;
                  String taskId = state.pathParameters['taskId']!;
                  return screens.Task(
                    mode: screens.TaskMode.editTask,
                    title: 'Edit Task',
                    taskId: taskId,
                    taskTitle: title,
                    taskSubtitle: subtitle,
                  );
                },
              ),
              GoRoute(
                path: 'view_task/:taskId',
                builder: (BuildContext context, GoRouterState state) {
                  String title = state.uri.queryParameters['title']!;
                  String subtitle = state.uri.queryParameters['subtitle']!;
                  String taskId = state.pathParameters['taskId']!;
                  return screens.Task(
                    mode: screens.TaskMode.viewTask,
                    title: 'Task',
                    taskId: taskId,
                    taskTitle: title,
                    taskSubtitle: subtitle,
                  );
                },
              ),
            ])
      ];
}
