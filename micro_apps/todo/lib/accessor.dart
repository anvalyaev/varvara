import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/data/repositories/use_cases.dart';
import 'package:todo/data/repositories/index.dart';

abstract class ITodoAccessor extends IAccessor {
  ITasksRepository get repositoryTasks;
}

class TodoAccessor extends ITodoAccessor {
  final UseCaseRepository _useCaseRepository = UseCaseRepository();
  ITasksRepository? _tasksRepository;

  @override
  IUseCaseRepository get repositoryUseCase => _useCaseRepository;

  @override
  ITasksRepository get repositoryTasks => _tasksRepository!;

  void initRepositoryTasks(TasksRepositoryIsar tasksRepositoryIsar) {
    if (_tasksRepository != null) {
      throw Exception('Tryed reinitialize _tasksRepository');
    }
    _tasksRepository ??= tasksRepositoryIsar;
  }
}
