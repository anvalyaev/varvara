import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localization/localization.dart';
import 'package:todo/presentation/bloc/index.dart';

class TodoList extends StatefulWidget {
  const TodoList({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  final TodoListCubit cubit = TodoListCubit();

  @override
  void initState() {
    cubit.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('To Do'),
        actions: [
          IconButton(onPressed: cubit.onNewTask, icon: const Icon(Icons.task)),
          IconButton(
            onPressed: cubit.onLogout,
            icon: const Icon(Icons.logout),
            tooltip: AppLocalizations.of(context)!.exit_from_app,
          ),
          // BlocBuilder<TodoListCubit, TodoListState>(
          //     bloc: cubit,
          //     builder: (context, state) {
          //       return IconButton(
          //           onPressed: state.busy ? null : cubit.onCount,
          //           icon: state.busy
          //               ? const SizedBox(
          //                   height: 24,
          //                   width: 24,
          //                   child: CircularProgressIndicator(
          //                     color: Colors.white,
          //                   ),
          //                 )
          //               : const Icon(Icons.cable));
          //     })
        ],
      ),
      body: Center(
          child: BlocBuilder<TodoListCubit, TodoListState>(
        bloc: cubit,
        builder: (context, state) {
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                    controller: cubit.scrollController,
                    itemCount: state.tasks.length,
                    itemBuilder: ((context, index) {
                      final item = state.tasks.values.elementAt(index);
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          title: Text(item.title),
                          subtitle: item.subtitle != null
                              ? Text(item.subtitle!)
                              : null,
                          leading: IconButton(
                            onPressed: () => cubit.onTaskEdit(item.id),
                            icon: const Icon(Icons.edit),
                          ),
                          trailing: Checkbox.adaptive(
                              value: item.done,
                              onChanged: ((value) {
                                cubit.onDoneChanged(item.id, value ?? false);
                              })),
                          onTap: () => cubit.onTaskView(item.id),
                        ),
                      );
                    })),
              ),
            ],
          );
        },
      )),
    );
  }

  @override
  void dispose() {
    cubit.close();
    super.dispose();
  }
}
