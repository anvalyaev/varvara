import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo/presentation/bloc/index.dart';

enum TaskMode { newTask, editTask, viewTask }

class Task extends StatefulWidget {
  final String title;
  final TaskMode mode;
  final String? taskId;
  final String? taskTitle;
  final String? taskSubtitle;

  const Task(
      {Key? key,
      required this.title,
      required this.mode,
      this.taskId,
      this.taskTitle,
      this.taskSubtitle})
      : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _TaskState createState() => _TaskState();
}

class _TaskState extends State<Task> {
  late final TaskCubit cubit = TaskCubit(
      mode: widget.mode,
      taskId: widget.taskId,
      taskSubtitle: widget.taskSubtitle,
      taskTitle: widget.taskTitle);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskCubit, TaskState>(
        bloc: cubit,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.title),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                  child: TextField(
                    readOnly: widget.mode == TaskMode.viewTask,
                    controller: cubit.controllerTitle,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                  child: TextField(
                    readOnly: widget.mode == TaskMode.viewTask,
                    controller: cubit.controllerSubtitle,
                  ),
                ),
              ],
            ),
            floatingActionButton: widget.mode != TaskMode.viewTask
                ? FloatingActionButton(
                    onPressed: cubit.onSubmit, child: const Icon(Icons.check))
                : null,
          );
        });
  }

  @override
  void dispose() {
    super.dispose();
    cubit.close();
  }
}
