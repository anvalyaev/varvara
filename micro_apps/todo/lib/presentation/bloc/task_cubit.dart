import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:todo/domain/index.dart';
import 'package:todo/presentation/bloc/index.dart';
import 'package:todo/presentation/screen/task.dart';

class TaskCubit extends Cubit<TaskState> with Presenter {
  final TextEditingController controllerTitle;
  final TextEditingController controllerSubtitle;
  final TaskMode mode;
  final String? taskId;
  TaskCubit(
      {required this.mode,
      required this.taskId,
      String? taskTitle,
      String? taskSubtitle})
      : controllerTitle = TextEditingController(text: taskTitle),
        controllerSubtitle = TextEditingController(text: taskSubtitle),
        super(TaskState(busy: false));

  void onInit() async {}

  void onSubmit() async {
    router.go('/todo_list');
    switch (mode) {
      case TaskMode.viewTask:
        throw Exception('Unsupported operation for TaskMode.viewTask');
      case TaskMode.editTask:
        emit(TaskState(busy: true));
        await execute<UpdateTask, UpdateTaskIn, UpdateTaskOut>(UpdateTask(
            UpdateTaskIn(
                taskId: taskId!,
                title: controllerTitle.text,
                subTitle: controllerSubtitle.text)));
        emit(TaskState(busy: false));
        break;
      case TaskMode.newTask:
        emit(TaskState(busy: true));
        await execute<AddTask, AddTaskIn, AddTaskOut>(AddTask(AddTaskIn(
            title: controllerTitle.text, subTitle: controllerSubtitle.text)));
        emit(TaskState(busy: false));
        break;
    }
  }

  @override
  Future<void> close() async {
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/todo';
}
