import 'package:todo/domain/entities/task.dart';

class TodoListState {
  final Map<String, TaskEntity> tasks;
  final bool busy;
  const TodoListState({this.tasks = const {}, this.busy = true});

  TodoListState copyWith({Map<String, TaskEntity>? tasks, bool? busy}) {
    return TodoListState(tasks: tasks ?? this.tasks, busy: busy ?? this.busy);
  }
}
