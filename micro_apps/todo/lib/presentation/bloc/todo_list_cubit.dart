import 'dart:async';

import 'package:core/core.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:isolated_clean_architecture/presentation/presenter.dart';
import 'package:todo/domain/entities/task.dart';
import 'package:todo/domain/index.dart';
import 'package:todo/presentation/bloc/index.dart';

class TodoListCubit extends Cubit<TodoListState> with Presenter {
  ScrollController scrollController = ScrollController();
  TodoListCubit() : super(const TodoListState());

  void onInit() async {
    final res = await execute<LoadTasks, LoadTasksIn, LoadTasksOut>(
        LoadTasks(LoadTasksIn()));
    emit(state.copyWith(busy: false, tasks: res?.tasks));

    subscribe<TasksChanged, TasksChangedIn, TasksChangedOut>(
            TasksChanged(TasksChangedIn()))
        .listen((event) async {
      // Future.delayed(const Duration(seconds: 3)).whenComplete(() async {
      emit(state.copyWith(busy: true));
      final res = await execute<LoadTasks, LoadTasksIn, LoadTasksOut>(
          LoadTasks(LoadTasksIn(limit: state.tasks.length)));
      emit(state.copyWith(busy: false, tasks: res?.tasks));
      // });
    });

    scrollController.addListener(() {
      if (scrollController.position.extentAfter < 500) {
        onLoadNext();
      }
    });
  }

  void onCount() async {
    emit(state.copyWith(busy: true));
    await execute<Count, CountIn, CountOut>(Count(CountIn()));
    emit(state.copyWith(busy: false));
  }

  void onLogout() {
    MicroAppsEventBus().emit(Logout());
  }

  Future<void> onLoadNext() async {
    print('onLoadNext');
    emit(state.copyWith(busy: true));
    final res = await execute<LoadTasks, LoadTasksIn, LoadTasksOut>(
        LoadTasks(LoadTasksIn(offset: state.tasks.length)));
    emit(state.copyWith(
        busy: false, tasks: state.tasks..addAll(res?.tasks ?? {})));
  }

  void onDoneChanged(
    String taskId,
    bool done,
  ) async {
    emit(state.copyWith(busy: true));
    final res = await execute<UpdateTask, UpdateTaskIn, UpdateTaskOut>(
        UpdateTask(UpdateTaskIn(taskId: taskId, done: done)));
    if (res != null) {
      final newTasks = Map<String, TaskEntity>.from(state.tasks);
      newTasks[taskId] = res.updatedTask;
      emit(state.copyWith(busy: false, tasks: newTasks));
    } else {
      emit(state.copyWith(busy: false));
    }
  }

  void onTaskView(String taskId) {
    final task = state.tasks[taskId];
    if (task == null) return;
    router.go(Uri(
            path: '/todo_list/view_task/$taskId',
            queryParameters: {'title': task.title, 'subtitle': task.subtitle})
        .toString());
  }

  void onTaskEdit(String taskId) {
    final task = state.tasks[taskId];
    if (task == null) return;
    router.go(Uri(
            path: '/todo_list/edit_task/$taskId',
            queryParameters: {'title': task.title, 'subtitle': task.subtitle})
        .toString());
  }

  void onNewTask() {
    router.go(Uri(
      path: '/todo_list/new_task/',
    ).toString());
  }

  @override
  Future<void> close() async {
    super.close();
    unsubscribeAll();
  }

  @override
  String get microAppName => '/todo';
}
