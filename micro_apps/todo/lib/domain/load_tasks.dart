import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';
import 'package:todo/domain/entities/task.dart';

class LoadTasksIn {
  final int offset;
  final int limit;
  LoadTasksIn({this.offset = 0, this.limit = 10});

  @override
  String toString() => 'LoadTasksIn(offset: $offset)';
}

class LoadTasksOut extends UseCaseResult {
  final Map<String, TaskEntity> tasks;

  LoadTasksOut(super.ucid, {required this.tasks});

  @override
  String toString() => 'LoadTasksOut(newTask: $tasks)';
}

class LoadTasksError extends UseCaseError {
  LoadTasksError(super.ucid);
}

class LoadTasks extends UseCaseBase<LoadTasksIn, LoadTasksOut> {
  LoadTasks(LoadTasksIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<LoadTasksOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;

    final tasks = accessor.repositoryTasks
        .getTasks(offset: dataIn!.offset, limit: dataIn!.limit)
        .map((e) => TaskEntity.fromTask(e))
        .toList();
    onResult(LoadTasksOut(this.id,
        tasks: tasks.asMap().map<String, TaskEntity>(
            (key, value) => MapEntry(value.id, value))));
    onComplete(this.id);
    return;
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
