import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';
import 'package:todo/data/repositories/collections/index.dart';
import 'package:todo/domain/entities/task.dart';
import 'package:uuid/uuid.dart';

class AddTaskIn {
  final String title;
  final String? subTitle;

  AddTaskIn({required this.title, this.subTitle});

  @override
  String toString() => 'AddTasIn(title: $title, subTitle: $subTitle)';
}

class AddTaskOut extends UseCaseResult {
  final TaskEntity newTask;

  AddTaskOut(super.ucid, {required this.newTask});

  @override
  String toString() => 'AddTaskOut(newTask: $newTask)';
}

class AddTaskError extends UseCaseError {
  AddTaskError(super.ucid);
}

class AddTask extends UseCaseBase<AddTaskIn, AddTaskOut> {
  AddTask(AddTaskIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<AddTaskOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;

    Task newTask = Task(
        id: const Uuid().v4(),
        done: false,
        title: dataIn!.title,
        subtitle: dataIn!.subTitle,
        created: DateTime.now());

    accessor.repositoryTasks.updateTask(newTask);

    onResult(AddTaskOut(id, newTask: TaskEntity.fromTask(newTask)));
    onComplete(id);
    return;
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
