import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';
import 'package:todo/domain/entities/task.dart';

class UpdateTaskIn {
  final String taskId;
  final bool? done;
  final String? title;
  final String? subTitle;

  UpdateTaskIn({required this.taskId, this.title, this.subTitle, this.done});

  @override
  String toString() =>
      'UpdateTaskIn(done: $done, title: $title, subTitle: $subTitle)';
}

class UpdateTaskOut extends UseCaseResult {
  final TaskEntity updatedTask;

  UpdateTaskOut(super.ucid, {required this.updatedTask});

  @override
  String toString() => 'UpdateTaskOut(updatedTask: $updatedTask)';
}

class UpdateTaskError extends UseCaseError {
  UpdateTaskError(super.ucid);
}

class UpdateTask extends UseCaseBase<UpdateTaskIn, UpdateTaskOut> {
  UpdateTask(UpdateTaskIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<UpdateTaskOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;
    var task = accessor.repositoryTasks.getTask(dataIn!.taskId);
    if (task == null) {
      onError(UpdateTaskError(this.id));
      onComplete(this.id);
      return;
    } else {
      task = task.copyWith(
          done: dataIn!.done, title: dataIn!.title, subtitle: dataIn!.subTitle);
      accessor.repositoryTasks.updateTask(task);
      onResult(UpdateTaskOut(this.id, updatedTask: TaskEntity.fromTask(task)));
      onComplete(this.id);
      return;
    }
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
