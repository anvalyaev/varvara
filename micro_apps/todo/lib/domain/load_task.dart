import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';
import 'package:todo/domain/entities/task.dart';

class LoadTaskIn {
  final String taskId;

  LoadTaskIn({required this.taskId});

  @override
  String toString() => 'LoadTaskIn(taskId: $taskId)';
}

class LoadTaskOut extends UseCaseResult {
  final TaskEntity task;

  LoadTaskOut(super.ucid, {required this.task});

  @override
  String toString() => 'LoadTaskOut(newTask: $task)';
}

class LoadTaskError extends UseCaseError {
  LoadTaskError(super.ucid);
}

class LoadTask extends UseCaseBase<LoadTaskIn, LoadTaskOut> {
  LoadTask(LoadTaskIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<LoadTaskOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;

    final task = accessor.repositoryTasks.getTask(dataIn!.taskId);
    if (task == null) {
      onError(LoadTaskError(this.id));
      onComplete(this.id);
      return;
    }
    onResult(LoadTaskOut(this.id, task: TaskEntity.fromTask(task)));
    onComplete(this.id);
    return;
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
