import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';

class RemoveTaskIn {
  final String taskId;

  RemoveTaskIn({required this.taskId});

  @override
  String toString() => 'RemoveTaskIn(taskId: $taskId)';
}

class RemoveTaskOut extends UseCaseResult {
  final bool success;

  RemoveTaskOut(super.ucid, {required this.success});

  @override
  String toString() => 'RemoveTaskOut(success: $success)';
}

class RemoveTaskError extends UseCaseError {
  RemoveTaskError(super.ucid);
}

class RemoveTask extends UseCaseBase<RemoveTaskIn, RemoveTaskOut> {
  RemoveTask(RemoveTaskIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<RemoveTaskOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;
    accessor.repositoryTasks.removeTask(dataIn!.taskId);
    onResult(RemoveTaskOut(this.id, success: true));
    onComplete(this.id);
    return;
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
