import 'package:intl/intl.dart';

import '../../data/repositories/collections/index.dart';

class TaskEntity {
  final String id;
  final bool done;
  final String title;
  final String? subtitle;
  final String created;

  TaskEntity(
      {required this.id,
      required this.done,
      required this.title,
      required this.created,
      this.subtitle});

  factory TaskEntity.fromTask(Task task) {
    String formattedDate = DateFormat('dd.MM.yyyy').format(task.created);
    return TaskEntity(
        id: task.id,
        done: task.done,
        title: task.title,
        subtitle: task.subtitle,
        created: formattedDate);
  }

  @override
  String toString() {
    return 'TaskEntity(id: $id, done: $done, title: $title, subtitle: $subtitle, created: $created)';
  }
}
