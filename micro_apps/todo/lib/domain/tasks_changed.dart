import 'package:flutter/services.dart';
import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';

class TasksChangedIn {
  TasksChangedIn();
}

class TasksChangedOut extends UseCaseResult {
  TasksChangedOut(super.ucid);
}

class TasksChangedError extends UseCaseError {
  TasksChangedError(super.ucid);
}

class TasksChanged extends UseCaseBase<TasksChangedIn, TasksChangedOut> {
  VoidCallback? _listener;
  TasksChanged(TasksChangedIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    accessor as ITodoAccessor;
    if (_listener != null) {
      accessor.repositoryTasks.removeListener(_listener!);
    }

    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<TasksChangedOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;

    _listener = () {
      onResult(TasksChangedOut(this.id));
    };

    accessor.repositoryTasks.addListener(_listener!);

    return;
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }

  @override
  int get orderGroupIndex => 1;
}
