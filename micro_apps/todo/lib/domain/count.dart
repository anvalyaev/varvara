import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';

class CountOut extends UseCaseResult {
  final int result;

  CountOut(super.ucid, {required this.result});

  @override
  String toString() => 'CountOut(success: $result)';
}

class CountIn {
  CountIn();
}

class Count extends UseCaseBase<CountIn, CountOut> {
  Count(CountIn dataIn) : super(dataIn: dataIn);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<CountOut> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as ITodoAccessor;

    int result = 0;
    for (; result < 100000000; result++) {
      result = result;
    }
    onResult(CountOut(id, result: result));
    onComplete(id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }

  @override
  int get orderGroupIndex => 2;
}
