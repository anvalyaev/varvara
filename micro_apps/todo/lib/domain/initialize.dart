import 'dart:io';

import 'package:flutter/services.dart';
import 'package:isolated_clean_architecture/accessor.dart';
import 'package:isolated_clean_architecture/domain/use_case_base.dart';
import 'package:todo/accessor.dart';
import 'package:todo/data/repositories/tasks.dart';
import 'package:path_provider/path_provider.dart';

class InitializeResult extends UseCaseResult {
  final bool success;

  InitializeResult(super.ucid, {required this.success});

  @override
  String toString() => 'InitializeResult(success: $success)';
}

class InitializeError extends UseCaseError {
  InitializeError(super.ucid);
}

class Initialize extends UseCaseBase<RootIsolateToken, InitializeResult> {
  Initialize(RootIsolateToken rootIsolateToken)
      : super(dataIn: rootIsolateToken);

  @override
  Future<void> cancelUseCase<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<void> doUseCase<A extends IAccessor>(
      A accessor,
      UseCaseOnResultCallback<InitializeResult> onResult,
      UseCaseOnErrorCallback onError,
      UseCaseOnCompleteCallback onComplete) async {
    accessor as TodoAccessor;
    BackgroundIsolateBinaryMessenger.ensureInitialized(dataIn!);
    try {
      Directory dir = await getApplicationSupportDirectory();
      accessor.initRepositoryTasks(TasksRepositoryIsar(dir: dir));
    } catch (e) {
      onError(InitializeError(this.id));
    }
    onResult(InitializeResult(this.id, success: true));
    onComplete(this.id);
  }

  @override
  Future init<A extends IAccessor>(A accessor) async {
    return;
  }

  @override
  Future<ErrorStatus> resolveError<A extends IAccessor>(A accessor) async {
    return ErrorStatus.unresolved;
  }
}
