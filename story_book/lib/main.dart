import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:story_book/common/index.dart';
import 'package:theme/theme.dart';
import 'package:theme/theme_loader.dart';
import 'package:widgetbook/widgetbook.dart';

late final AppThemesHolderData themesHolderDataModel;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final themeContent = await rootBundle.loadString('assets/theme/theme.json');
  final themesHolderDataJson = jsonDecode(themeContent);
  themesHolderDataModel = AppThemesHolderDataDto.fromJson(themesHolderDataJson)
      .toAppThemesHolderData();
  runApp(AppThemesHolder(
      data: themesHolderDataModel, child: const WidgetbookApp()));
}

class WidgetbookApp extends StatefulWidget {
  const WidgetbookApp({super.key});

  @override
  State<WidgetbookApp> createState() => _WidgetbookAppState();
}

class _WidgetbookAppState extends State<WidgetbookApp> {
  @override
  Widget build(BuildContext context) {
    return AnimatedAppTheme.wrapViewWithEffectiveTheme(
      child: buildWidgetBook(),
      viewKey: widget.key,
      context: context,
    );
  }

  Widgetbook buildWidgetBook() {
    return Widgetbook.material(
      addons: [
        DeviceFrameAddon(devices: Devices.ios.all),
        InspectorAddon(),
        GridAddon(100),
        AlignmentAddon(),
        ZoomAddon(),
      ],
      directories: [
        WidgetbookFolder(
          name: 'Common components',
          children: [
            WidgetbookComponent(
              name: 'Counter',
              useCases: [
                WidgetbookUseCase(
                  name: 'Default Style',
                  builder: (context) => buildCounter(context),
                ),
              ],
            ),
            WidgetbookComponent(
              name: 'Main Button',
              useCases: [
                WidgetbookUseCase(
                  name: 'Default Style',
                  builder: (context) => buildMainButton(context),
                ),
              ],
            ),
            WidgetbookComponent(
              name: 'Icon Button',
              useCases: [
                WidgetbookUseCase(
                  name: 'Default Style',
                  builder: (context) => buildIconButton(context),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
