import 'package:components/common/counter.dart';
import 'package:components/components.dart' as components;
import 'package:flutter/material.dart';
import 'package:story_book/common/counter.dart';
import 'package:theme_styles/theme_styles.dart';
import 'package:widgetbook/widgetbook.dart';

List<IconData> _icons = [
  Icons.login,
  Icons.logout,
  Icons.wallet,
  Icons.add,
  Icons.calculate,
];

Widget buildIconButton(BuildContext context) {
  return components.IconButton(
    style: IconButtonStyleByTheme.of(context),
    icon: _icons[context.knobs.int
        .slider(label: 'Icons', min: 0, max: _icons.length - 1)],
    busy: context.knobs.boolean(label: 'Busy'),
    counter: buildCounter(context) as Counter,
  );
}
