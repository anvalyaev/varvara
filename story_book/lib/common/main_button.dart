import 'package:components/components.dart';
import 'package:flutter/material.dart';
import 'package:theme_styles/theme_styles.dart';
import 'package:widgetbook/widgetbook.dart';

List<IconData> _icons = [
  Icons.login,
  Icons.logout,
  Icons.wallet,
  Icons.add,
  Icons.calculate,
];

Widget buildMainButton(BuildContext context) {
  return MainButton(
    style: MainButtonStyleByTheme.of(context),
    text: context.knobs.string(label: 'Text', initialValue: 'Main Button'),
    icon: _icons[context.knobs.int
        .slider(label: 'Icons', min: 0, max: _icons.length - 1)],
    busy: context.knobs.boolean(label: 'Busy'),
    enable: context.knobs.boolean(label: 'Enable', initialValue: true),
    onPressed: () {},
  );
}
