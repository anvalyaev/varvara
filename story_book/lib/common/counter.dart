import 'package:components/components.dart';
import 'package:flutter/material.dart';
import 'package:theme_styles/theme_styles.dart';
import 'package:widgetbook/widgetbook.dart';

Widget buildCounter(BuildContext context) {
  return Counter(
      count:
          context.knobs.int.slider(label: 'Count', max: 200, initialValue: 10),
      maxCount: context.knobs.int
          .slider(label: 'Max Count', max: 200, initialValue: 100),
      style: CounterStyleByTheme.of(context),
      size: context.knobs.double
          .slider(label: 'Size', initialValue: 24, max: 100));
}
